//
//  CustomPickerView.swift
//  FHE
//
//  Created by Macintosh on 22/10/21.
//  Copyright © 2021 sylar. All rights reserved.
//

import UIKit
/*
 AlertView.show(message: AlertConst.Msg.internectDisconnectError, cancelButtonText: ButtonCaption.retry) { (button) in
     callGetPaymentCards(block)
 }
 */
protocol CustomPickerViewDelegate {
    func cancelTapped()
    func okTapped()
}

class CustomPickerView: UIView {
    
    @IBOutlet  weak var alertLabel: UILabel!
    var delegate:CustomPickerViewDelegate?
    
    let heightArray = ["3","4"]
    
 
    @IBOutlet  weak var customPicker: UIPickerView!
    @IBOutlet  weak var bottomView_CancelBtn: UIButton!
    @IBOutlet  weak var bottomView_OkeyBtn: UIButton!
    
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        if self.subviews.count == 0 {
            setup()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    private func setup() {
        if let view = Bundle.main.loadNibNamed("CustomPickerView", owner: self, options: nil)?.first as? UIView {
            view.frame = bounds
            view.autoresizingMask = UIView.AutoresizingMask(rawValue: UIView.AutoresizingMask.RawValue(UInt8(UIView.AutoresizingMask.flexibleWidth.rawValue) | UInt8(UIView.AutoresizingMask.flexibleHeight.rawValue)))
            
            view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            self.backgroundColor = UIColor.clear
            addSubview(view)
            
        }
    }
    
    //===========================================================
    //MARK: - Action Methods
    //===========================================================
    @IBAction func okeyBtnTapped(_ sender: UIButton) {
        self.delegate?.okTapped()
      
    }
    @IBAction func cancelBtnTapped(_ sender: UIButton) {
        self.delegate?.cancelTapped()
      
    }
    
}
extension CustomPickerView:UIPickerViewDelegate,UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
   
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 2
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return heightArray[component]
    }
    
}
