//
//  ReminderModel.swift
//  FHE
//
//  Created by Macintosh on 29/10/21.
//  Copyright © 2021 sylar. All rights reserved.
//

import UIKit
import SwiftyJSON

class ReminderModel: NSObject {
    

    
    
    var remainderBody = ""
    var _id = ""
    var createdAt = ""
    var userId = ""
    var updatedAt = ""
    var interval = ""
    var atThisTime = ""
    
    
    
    class func getAllJSONList(dataArray:[JSON]) -> Array<ReminderModel>{
        var completeDataArray = Array<ReminderModel>()
        for elements in dataArray{
            let dataDetails = ReminderModel.parseJSONData(details: elements)
            completeDataArray.append(dataDetails)
        }
        return completeDataArray
    }
    
    class func parseJSONData(details:JSON) -> ReminderModel{
        let dataDetails = ReminderModel()
        
        
        dataDetails.remainderBody = "\(details["remainderBody"].string ?? "")"
        dataDetails._id = "\(details["_id"].string ?? "")"
        
        dataDetails.createdAt = details["createdAt"].string ?? ""
        dataDetails.userId = details["userId"].string ?? ""
        
        dataDetails.interval = details["interval"].string ?? ""
        dataDetails.updatedAt = details["updatedAt"].string ?? ""
        dataDetails.atThisTime = details["atThisTime"].string ?? ""
        return dataDetails
    }
    

}
