//
//  HeartRateModel.swift
//  FHE
//
//  Created by Macintosh on 28/10/21.
//  Copyright © 2021 sylar. All rights reserved.
//

import UIKit
import SwiftyJSON
class HeartRateModel: NSObject {
    
    
    var beatsPerMinute = ""
    
    var _id = ""
    
    var createdAt = ""
    
    var userId = ""
    
    var dateString = ""
    var updatedAt = ""
    
    
    
    class func getAllJSONList(dataArray:[JSON]) -> Array<HeartRateModel>{
        var completeDataArray = Array<HeartRateModel>()
        for elements in dataArray{
            let dataDetails = HeartRateModel.parseJSONData(details: elements)
            completeDataArray.append(dataDetails)
        }
        return completeDataArray
    }
    
    class func parseJSONData(details:JSON) -> HeartRateModel{
        let dataDetails = HeartRateModel()
        
        
        dataDetails.beatsPerMinute = "\(details["beatsPerMinute"].int ?? 0)"
        dataDetails._id = "\(details["_id"].string ?? "")"
        
        dataDetails.createdAt = details["createdAt"].string ?? ""
        dataDetails.userId = details["userId"].string ?? ""
        
        dataDetails.dateString = details["dateString"].string ?? ""
        dataDetails.updatedAt = details["updatedAt"].string ?? ""
        return dataDetails
    }
    
    
}
