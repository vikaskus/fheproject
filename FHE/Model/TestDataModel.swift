// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let loginDataModel = try? newJSONDecoder().decode(LoginDataModel.self, from: jsonData)

import Foundation
import SwiftyJSON
// MARK: - LoginDataModel
class TestDataModel: NSObject {
    
    
       var name = ""
       var realname = ""
       var team = ""
       var firstappearance = ""
       var createdby = ""
       var publisher = ""
       var imageurl = ""
       var bio = ""
       
       
       
       class func getAllTestDataList(dataArray:[JSON]) -> Array<TestDataModel>{
           var completeDataArray = Array<TestDataModel>()
           for elements in dataArray{
               let dataDetails = TestDataModel.parseTestData(details: elements)
            completeDataArray.append(dataDetails)
           }
           return completeDataArray
       }
       
       class func parseTestData(details:JSON) -> TestDataModel{
           let dataDetails = TestDataModel()
        dataDetails.name = details["name"].string ?? ""
         
        dataDetails.realname = details["realname"].string ?? ""
        dataDetails.team = "\(details["team"].string ?? "")"
        dataDetails.firstappearance = "\(details["firstappearance"].string ?? "")"
        dataDetails.createdby = details["createdby"].string ?? ""
          
          
        dataDetails.publisher = details["publisher"].string ?? ""
           
        dataDetails.imageurl = details["imageurl"].string ?? ""
        dataDetails.bio = details["bio"].string ?? ""
           return dataDetails
       }

   
}
