//
//  BlogDataModel.swift
//  FHE
//
//  Created by Macintosh on 30/10/21.
//  Copyright © 2021 sylar. All rights reserved.
//

import UIKit
import SwiftyJSON

class BlogDataModel: NSObject {
    
   
    
    
    var userName = ""
    var _id = ""
    var createdAt = ""
    var dateString = ""
    var updatedAt = ""
    var userType = ""
    var postData = ""
    
    
    
    class func getAllJSONList(dataArray:[JSON]) -> Array<BlogDataModel>{
        var completeDataArray = Array<BlogDataModel>()
        for elements in dataArray{
            let dataDetails = BlogDataModel.parseJSONData(details: elements)
            completeDataArray.append(dataDetails)
        }
        return completeDataArray
    }
    
    class func parseJSONData(details:JSON) -> BlogDataModel{
        let dataDetails = BlogDataModel()
        
        
        dataDetails.userName = "\(details["userName"].string ?? "")"
        dataDetails._id = "\(details["_id"].string ?? "")"
        
        dataDetails.createdAt = details["createdAt"].string ?? ""
        dataDetails.dateString = details["dateString"].string ?? ""
        
        dataDetails.userType = details["userType"].string ?? ""
        dataDetails.updatedAt = details["updatedAt"].string ?? ""
        dataDetails.postData = details["postData"].string ?? ""
        return dataDetails
    }
    
    
    

}
