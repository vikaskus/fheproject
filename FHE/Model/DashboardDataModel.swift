//
//  DashboardDataModel.swift
//  FHE
//
//  Created by Macintosh on 28/10/21.
//  Copyright © 2021 sylar. All rights reserved.
//

import UIKit
import SwiftyJSON
class DashboardDataModel: NSObject {
    var heartData:HeartRateModel?
    var oxygenData:OxygenModel?
    var bpData:BPDataModel?
    var stepsData:StepsDataModel?
    
    
    class func parseJSONData(details:JSON) -> DashboardDataModel{
        let dataDetails = DashboardDataModel()
        
        
        dataDetails.heartData = HeartRateModel.parseJSONData(details: details["HeartData"])
        dataDetails.oxygenData = OxygenModel.parseJSONData(details: details["oxygenPercentIs"])
        dataDetails.bpData = BPDataModel.parseJSONData(details: details["BPdata"])
        dataDetails.stepsData = StepsDataModel.parseJSONData(details: details["StepsData"])
        return dataDetails
    }
    
    
}
