//
//  ExerciseModel.swift
//  FHE
//
//  Created by Macintosh on 29/10/21.
//  Copyright © 2021 sylar. All rights reserved.
//

import UIKit
import SwiftyJSON

class ExerciseModel: NSObject {

    var totalCaloriesBurned = ""
    var _id = ""
    var createdAt = ""
    var exerciseType = ""
    var updatedAt = ""
    var interval = ""
    var exerciseName = ""
    
    
    
    class func getAllJSONList(dataArray:[JSON]) -> Array<ExerciseModel>{
        var completeDataArray = Array<ExerciseModel>()
        for elements in dataArray{
            let dataDetails = ExerciseModel.parseJSONData(details: elements)
            completeDataArray.append(dataDetails)
        }
        return completeDataArray
    }
    
    class func parseJSONData(details:JSON) -> ExerciseModel{
        let dataDetails = ExerciseModel()
        
        
        dataDetails.totalCaloriesBurned = "\(details["totalCaloriesBurned"].int ?? 0)"
        dataDetails._id = "\(details["_id"].string ?? "")"
        
        dataDetails.createdAt = details["createdAt"].string ?? ""
        dataDetails.exerciseType = details["exerciseType"].string ?? ""
        
        dataDetails.interval = details["interval"].string ?? ""
        dataDetails.updatedAt = details["updatedAt"].string ?? ""
        dataDetails.exerciseName = details["exerciseName"].string ?? ""
        return dataDetails
    }
    
    
    

}
