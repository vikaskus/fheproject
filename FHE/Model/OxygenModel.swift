//
//  OxygenModel.swift
//  FHE
//
//  Created by Macintosh on 28/10/21.
//  Copyright © 2021 sylar. All rights reserved.
//

import UIKit
import SwiftyJSON

class OxygenModel: NSObject {
    

     var oxygenPercentage = ""
     var _id = ""
     var createdAt = ""
     var userId = ""
     var dateString = ""
     var updatedAt = ""
     
     
     
     class func getAllJSONList(dataArray:[JSON]) -> Array<OxygenModel>{
         var completeDataArray = Array<OxygenModel>()
         for elements in dataArray{
             let dataDetails = OxygenModel.parseJSONData(details: elements)
          completeDataArray.append(dataDetails)
         }
         return completeDataArray
     }
     
     class func parseJSONData(details:JSON) -> OxygenModel{
         let dataDetails = OxygenModel()

       
      dataDetails.oxygenPercentage = "\(details["oxygenPercentage"].int ?? 0)"
      dataDetails._id = "\(details["_id"].string ?? "")"

      dataDetails.createdAt = details["createdAt"].string ?? ""
      dataDetails.userId = details["userId"].string ?? ""
     
      dataDetails.dateString = details["dateString"].string ?? ""
      dataDetails.updatedAt = details["updatedAt"].string ?? ""
         return dataDetails
     }


}
