// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let loginDataModel = try? newJSONDecoder().decode(LoginDataModel.self, from: jsonData)

import Foundation
import SwiftyJSON
// MARK: - LoginDataModel
class LoginDataModel: NSObject {
    
    
       var mobileotp = ""
       var createdAt = ""
       var countryCode = ""
       var phoneNo = ""
       var _id = ""
       var deviceToken = ""
       var updatedAt = ""
       
       
       
       class func getAllLoginListArray(hospitalArray:[JSON]) -> Array<LoginDataModel>{
           var hospitalDataArray = Array<LoginDataModel>()
           for elements in hospitalArray{
               let dataDetails = LoginDataModel.parseLoginData(details: elements)
               hospitalDataArray.append(dataDetails)
           }
           return hospitalDataArray
       }
       
       class func parseLoginData(details:JSON) -> LoginDataModel{
           let hospitalDetails = LoginDataModel()
           hospitalDetails.createdAt = details["createdAt"].string ?? ""
         
           hospitalDetails.mobileotp = details["mobileotp"].string ?? ""
           hospitalDetails.countryCode = "\(details["countryCode"].string ?? "")"
           hospitalDetails._id = "\(details["_id"].string ?? "")"
           hospitalDetails.deviceToken = details["deviceToken"].string ?? ""
          
          
           hospitalDetails.phoneNo = details["phoneNo"].string ?? ""
           
           hospitalDetails.updatedAt = details["updatedAt"].string ?? ""
           return hospitalDetails
       }

   
}
