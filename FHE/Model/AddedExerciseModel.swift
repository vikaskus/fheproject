//
//  AddedExerciseModel.swift
//  FHE
//
//  Created by Macintosh on 29/10/21.
//  Copyright © 2021 sylar. All rights reserved.
//

import UIKit
import SwiftyJSON

class AddedExerciseModel: NSObject {
    
    var totalCaloriesBurned = ""
    var _id = ""
    var createdAt = ""
    var exerciseType = ""
    var updatedAt = ""
    var interval = ""
    var exerciseName = ""
    var duration = ""
    var userId = ""
    var numberOfRepetitionPerSet = ""
    var dateString = ""
    var numberOfSets = ""
    
    
    
    class func getAllJSONList(dataArray:[JSON]) -> Array<AddedExerciseModel>{
        var completeDataArray = Array<AddedExerciseModel>()
        for elements in dataArray{
            let dataDetails = AddedExerciseModel.parseJSONData(details: elements)
            completeDataArray.append(dataDetails)
        }
        return completeDataArray
    }
    
    class func parseJSONData(details:JSON) -> AddedExerciseModel{
        let dataDetails = AddedExerciseModel()
        
        
        dataDetails.totalCaloriesBurned = "\(details["totalCaloriesBurned"].int ?? 0)"
        dataDetails.duration = "\(details["duration"].int ?? 0)"
        dataDetails._id = "\(details["_id"].string ?? "")"
        
        dataDetails.createdAt = details["createdAt"].string ?? ""
        dataDetails.exerciseType = details["exerciseType"].string ?? ""
        
        dataDetails.interval = details["interval"].string ?? ""
        dataDetails.updatedAt = details["updatedAt"].string ?? ""
        dataDetails.exerciseName = details["exerciseName"].string ?? ""
        
        
        dataDetails.userId = details["userId"].string ?? ""
        
        dataDetails.numberOfRepetitionPerSet = details["numberOfRepetitionPerSet"].string ?? ""
        dataDetails.dateString = details["dateString"].string ?? ""
        dataDetails.numberOfSets = details["numberOfSets"].string ?? ""
        
        return dataDetails
    }

}
