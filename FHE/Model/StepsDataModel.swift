//
//  StepsDataModel.swift
//  FHE
//
//  Created by Macintosh on 28/10/21.
//  Copyright © 2021 sylar. All rights reserved.
//

import UIKit
import SwiftyJSON

class StepsDataModel: NSObject {
    
   
    
    
       var currentStatus = ""
       var distance = ""
       var _id = ""
       var stepCount = ""
       var createdAt = ""
       var caloriesBurned = ""
       var userId = ""
       var duration = ""
    var dateString = ""
    var updatedAt = ""
       
       
       
       class func getAllJSONList(dataArray:[JSON]) -> Array<StepsDataModel>{
           var completeDataArray = Array<StepsDataModel>()
           for elements in dataArray{
               let dataDetails = StepsDataModel.parseJSONData(details: elements)
            completeDataArray.append(dataDetails)
           }
           return completeDataArray
       }
       
       class func parseJSONData(details:JSON) -> StepsDataModel{
           let dataDetails = StepsDataModel()
        dataDetails.currentStatus = details["currentStatus"].string ?? ""
         
        dataDetails.distance = "\(details["distance"].int ?? 0)"
        dataDetails._id = "\(details["_id"].string ?? "")"
        dataDetails.stepCount = "\(details["stepCount"].int ?? 0)"
        dataDetails.createdAt = details["createdAt"].string ?? ""
          
          
        dataDetails.caloriesBurned = "\(details["caloriesBurned"].int ?? 0)"
           
        dataDetails.userId = details["userId"].string ?? ""
        dataDetails.duration = "\(details["duration"].int ?? 0)"
        dataDetails.dateString = details["dateString"].string ?? ""
        dataDetails.updatedAt = details["updatedAt"].string ?? ""
           return dataDetails
       }


}
