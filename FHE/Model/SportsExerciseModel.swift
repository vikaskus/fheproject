//
//  SportsExerciseModel.swift
//  FHE
//
//  Created by Macintosh on 02/11/21.
//  Copyright © 2021 sylar. All rights reserved.
//

import UIKit
import SwiftyJSON
class SportsExerciseModel: NSObject {
    
    var startTime = ""
    var _id = ""
    var createdAt = ""
    var kcal = ""
    var updatedAt = ""
    var step = ""
    var sportName = ""
    var vaildTime = ""
    var userId = ""
    var endTime = ""
    var dateString = ""
    var date = ""
    
    
    class func getAllJSONList(dataArray:[JSON]) -> Array<SportsExerciseModel>{
        var completeDataArray = Array<SportsExerciseModel>()
        for elements in dataArray{
            let dataDetails = SportsExerciseModel.parseJSONData(details: elements)
            completeDataArray.append(dataDetails)
        }
        return completeDataArray
    }
    
    class func parseJSONData(details:JSON) -> SportsExerciseModel{
        let dataDetails = SportsExerciseModel()
        
        
        dataDetails.startTime = "\(details["startTime"].string ?? "")"
        dataDetails.kcal = "\(details["kcal"].string ?? "")"
        dataDetails._id = "\(details["_id"].string ?? "")"
        
        dataDetails.createdAt = details["createdAt"].string ?? ""
        dataDetails.step = details["step"].string ?? ""
        
        dataDetails.sportName = details["sportName"].string ?? ""
        dataDetails.updatedAt = details["updatedAt"].string ?? ""
        dataDetails.vaildTime = details["vaildTime"].string ?? ""
        
        
        dataDetails.userId = details["userId"].string ?? ""
        
        dataDetails.endTime = details["endTime"].string ?? ""
        dataDetails.dateString = details["dateString"].string ?? ""
        dataDetails.date = details["date"].string ?? ""
        
        return dataDetails
    }


}
