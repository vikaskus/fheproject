//
//  BPDataModel.swift
//  FHE
//
//  Created by Macintosh on 28/10/21.
//  Copyright © 2021 sylar. All rights reserved.
//

import UIKit
import SwiftyJSON

class BPDataModel: NSObject {

        var systolicPressure = ""
        var diastolicPressure = ""
        var _id = ""
     
        var createdAt = ""
       
        var userId = ""
        var detailAboutCurrentActivity = ""
     var dateString = ""
     var updatedAt = ""
        
        
        
        class func getAllJSONList(dataArray:[JSON]) -> Array<BPDataModel>{
            var completeDataArray = Array<BPDataModel>()
            for elements in dataArray{
                let dataDetails = BPDataModel.parseJSONData(details: elements)
             completeDataArray.append(dataDetails)
            }
            return completeDataArray
        }
        
        class func parseJSONData(details:JSON) -> BPDataModel{
            let dataDetails = BPDataModel()
    
          
         dataDetails.systolicPressure = "\(details["systolicPressure"].int ?? 0)"
         dataDetails._id = "\(details["_id"].string ?? "")"
         dataDetails.diastolicPressure = "\(details["diastolicPressure"].int ?? 0)"
         dataDetails.createdAt = details["createdAt"].string ?? ""
           
         dataDetails.userId = details["userId"].string ?? ""
         dataDetails.detailAboutCurrentActivity = "\(details["detailAboutCurrentActivity"].string ?? "")"
         dataDetails.dateString = details["dateString"].string ?? ""
         dataDetails.updatedAt = details["updatedAt"].string ?? ""
            return dataDetails
        }



}
