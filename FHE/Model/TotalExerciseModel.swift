//
//  TotalExerciseModel.swift
//  FHE
//
//  Created by Macintosh on 29/10/21.
//  Copyright © 2021 sylar. All rights reserved.
//

import UIKit
import SwiftyJSON

class TotalExerciseModel: NSObject {
    
    
    
    var totalExerciseDone = ""
    var _id = ""
    var totalExerciseDuration = ""
    var totalCaloriesBurned = ""
  
    
    
    
    class func getAllJSONList(dataArray:[JSON]) -> Array<TotalExerciseModel>{
        var completeDataArray = Array<TotalExerciseModel>()
        for elements in dataArray{
            let dataDetails = TotalExerciseModel.parseJSONData(details: elements)
            completeDataArray.append(dataDetails)
        }
        return completeDataArray
    }
    
    class func parseJSONData(details:JSON) -> TotalExerciseModel{
        let dataDetails = TotalExerciseModel()
        
        
        dataDetails.totalCaloriesBurned = "\(details["totalCaloriesBurned"].int ?? 0)"
        dataDetails._id = "\(details["_id"].string ?? "")"
        dataDetails.totalExerciseDone = "\(details["totalExerciseDone"].int ?? 0)"
        dataDetails.totalExerciseDuration = "\(details["totalExerciseDuration"].int ?? 0)"
        return dataDetails
    }
    
    

}
