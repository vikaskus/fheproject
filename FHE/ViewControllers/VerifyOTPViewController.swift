//
//  VerifyOTPViewController.swift
//  FHE
//
//  Created by Macintosh on 21/10/21.
//  Copyright © 2021 sylar. All rights reserved.
//

import UIKit
import SVPinView

class VerifyOTPViewController: BaseViewControllerClass {
    
    static var viewControllerId = "VerifyOTPViewController"
    static var storyBoard = "Main"
    var otpEntered = ""
    var phoneNumber = ""

    @IBOutlet weak var pinView: SVPinView!
       
       override func viewDidLoad() {
           super.viewDidLoad()

           configurePinView()
           
       }
       
       override func viewWillLayoutSubviews() {
           super.viewWillLayoutSubviews()
           
           // Setup background gradient
   //        let valenciaColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
   //        let discoColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
   //        setGradientBackground(view: self.view, colorTop: valenciaColor, colorBottom: discoColor)
       }
       
       func configurePinView() {
           
           pinView.pinLength = 4
           pinView.secureCharacter = "\u{25CF}"
           pinView.interSpace = 10
           pinView.textColor = UIColor.white
           pinView.borderLineColor = UIColor.white
        pinView.activeBorderLineColor = Colors.themeGreen
           pinView.borderLineThickness = 1
           pinView.shouldSecureText = false
           pinView.allowsWhitespaces = false
           pinView.style = .box
           pinView.fieldBackgroundColor = UIColor.clear
           pinView.activeFieldBackgroundColor = UIColor.black.withAlphaComponent(0.5)
           pinView.fieldCornerRadius = 15
           pinView.activeFieldCornerRadius = 15
           pinView.placeholder = ""
           pinView.deleteButtonAction = .deleteCurrentAndMoveToPrevious
           pinView.keyboardAppearance = .default
           pinView.tintColor = .white
           pinView.becomeFirstResponderAtIndex = 0
           pinView.shouldDismissKeyboardOnEmptyFirstField = false
           
           pinView.font = UIFont.systemFont(ofSize: 17)
           pinView.keyboardType = .phonePad
           pinView.pinInputAccessoryView = { () -> UIView in
               let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
               doneToolbar.barStyle = UIBarStyle.default
               let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
               let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(dismissKeyboard))
               
               var items = [UIBarButtonItem]()
               items.append(flexSpace)
               items.append(done)
               
               doneToolbar.items = items
               doneToolbar.sizeToFit()
               return doneToolbar
           }()
           
           pinView.didFinishCallback = didFinishEnteringPin(pin:)
           pinView.didChangeCallback = { pin in
               print("The entered pin is \(pin)")
           }
       }
       
       @objc func dismissKeyboard() {
           self.view.endEditing(false)
       }
       
       @IBAction func printPin() {
           let pin = pinView.getPin()
           guard !pin.isEmpty else {
               showAlert(title: "Error", message: "Pin entry incomplete")
               return
           }
          // showAlert(title: "Success", message: "The Pin entered is \(pin)")
        otpEntered = pin
       }
       
       @IBAction func toggleStyle() {
           var nextStyle = pinView.style.rawValue + 1
           if nextStyle == 3 { nextStyle = 0 }
           let style = SVPinViewStyle(rawValue: nextStyle)!
           switch style {
           case .none:
               pinView.fieldBackgroundColor = UIColor.white.withAlphaComponent(0.3)
               pinView.activeFieldBackgroundColor = UIColor.white.withAlphaComponent(0.5)
               pinView.fieldCornerRadius = 15
               pinView.activeFieldCornerRadius = 15
               pinView.style = style
           case .box:
               pinView.activeBorderLineThickness = 4
               pinView.fieldBackgroundColor = UIColor.clear
               pinView.activeFieldBackgroundColor = UIColor.clear
               pinView.fieldCornerRadius = 0
               pinView.activeFieldCornerRadius = 0
               pinView.style = style
           case .underline:
               pinView.activeBorderLineThickness = 4
               pinView.fieldBackgroundColor = UIColor.clear
               pinView.activeFieldBackgroundColor = UIColor.clear
               pinView.fieldCornerRadius = 0
               pinView.activeFieldCornerRadius = 0
               pinView.style = style
           @unknown default: break
           }
       }
       
       func didFinishEnteringPin(pin:String) {
         //  showAlert(title: "Success", message: "The Pin entered is \(pin)")
        otpEntered = pin
       }
       
       func showAlert(title:String, message:String) {
           let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
           alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
           self.present(alert, animated: true, completion: nil)
       }
    
    
    @IBAction func btnVerifyOTPAction(_ sender: Any) {
        if otpEntered != ""{
            let params = ["phoneNo": phoneNumber,
                          "mobileotp": otpEntered,]
            callVerifyOTPApi(params: params)
        }
        
    }
    
}
extension VerifyOTPViewController{
    
    func callVerifyOTPApi(params:[String:String]){
        
        /*
        if !AppHelper.isInterNetConnectionAvailable(){
            showAlertWithMessage(ConstantStrings.ALERT, ConstantStrings.pleaseCheckYourInternetConnection)
            return
        }
        */
      //  ERProgressHud.sharedInstance.showBlurView(withTitle: "Loading...")
        WebServiceHandler.performPOSTRequest(withURL: verifyOtp, andParameters: params) {(result,error) in
            if result != nil{
                print(result!)
                
                let statusCode = result!["statusCode"]?.int
                if statusCode == 200
                {
                    let resultData = result!["accessToken"]?.string
                    UserDetails.sharedInstance.accessToken = resultData!
                    UserDefaultOperations.setStringObject("accessToken", resultData!)
                    let controller = UserDataViewController.instantiateFromStoryBoard()
                    self.push(controller)
                    if let data = result?["data"]?.dictionary{
                        print(data)
                        /*
                        self.loginData = LoginDataModel.parseLoginData(details: result!["data"]!)
                        let controller = VerifyOTPViewController.instantiateFromStoryBoard()
                        self.push(controller)
                        self.showAlertWithMessage(ConstantStrings.ALERT, "OTP is :\(self.loginData!.mobileotp)")
                        print(self.loginData)
                        */
                    }
                }
                else{
                    if let message = result!["message"]?.string{
                       // AppHelper.showAlertView(message: errorMsg)
                        self.showAlertWithMessage(ConstantStrings.ALERT, message)
                    }
                }
              //  ERProgressHud.sharedInstance.hide()
            }else{
                self.showAlertWithMessage("ALERT",  "Something Went Wrong")
              //  ERProgressHud.sharedInstance.hide()
            }
        }
    }
}
