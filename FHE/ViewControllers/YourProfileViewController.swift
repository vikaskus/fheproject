//
//  YourProfileViewController.swift
//  FHE
//
//  Created by Macintosh on 29/10/21.
//  Copyright © 2021 sylar. All rights reserved.
//

import UIKit

class YourProfileViewController: BaseViewControllerClass {
    
    @IBOutlet weak var lblNumberTitle: UILabel!
    @IBOutlet weak var lblNameTitle: UILabel!
    static var viewControllerId = "YourProfileViewController"
    static var storyBoard = "Main"
    
    let listArray = ["About me","My Plans","My Goals","My Daily Report","Reminders","Settings","Contact Us","Logout"]
    
    @IBOutlet weak var itemList: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        setInitials()
    }
    
    
    func setInitials(){
        
       // CRPSmartBandSDK.sharedInstance.delegate = self
        itemList.delegate = self
        itemList.dataSource = self
        
        itemList.register(UINib(nibName: "ProfileTabTableViewCell", bundle: nil), forCellReuseIdentifier: "ProfileTabTableViewCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setProfileData()
    }
    
    
    func setProfileData(){
        lblNumberTitle.text = UserDetails.sharedInstance.phone
        lblNameTitle.text = UserDetails.sharedInstance.firstName + " " + UserDetails.sharedInstance.lastName
    }
    
    
    

  
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
extension YourProfileViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listArray.count
      
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = itemList.dequeueReusableCell(withIdentifier: "ProfileTabTableViewCell") as? ProfileTabTableViewCell
        cell?.lblTitle.text = listArray[indexPath.row]
        cell?.selectionStyle = .none
        return cell!
        
       
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 70
     
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            let controller = ViewProfileViewController.instantiateFromStoryBoard()
            self.push(controller)
        }else if indexPath.row == 4{
            let controller = ReminderListViewController.instantiateFromStoryBoard()
            self.push(controller)
        }else if indexPath.row == 5{
            let controller = WatchSettingsViewController.instantiateFromStoryBoard()
            self.push(controller)
        }
    }
}
