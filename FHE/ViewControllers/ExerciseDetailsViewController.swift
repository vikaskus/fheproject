//
//  ExerciseDetailsViewController.swift
//  FHE
//
//  Created by Macintosh on 29/10/21.
//  Copyright © 2021 sylar. All rights reserved.
//

import UIKit

class ExerciseDetailsViewController: BaseViewControllerClass,ExerciseDurationTableViewCellDelegate {
   
    var duration = 0
    var totalCaloriesBurned = 0
    var perCalorie = 0
    var exerciseData:ExerciseModel?
    
    static var viewControllerId = "ExerciseDetailsViewController"
    static var storyBoard = "Main"

    @IBOutlet weak var lblExerciseName: UILabel!
    @IBOutlet weak var itemList: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setInitials()
        
        

        // Do any additional setup after loading the view.
    }
    
    
    func setInitials(){
        
       // CRPSmartBandSDK.sharedInstance.delegate = self
        itemList.delegate = self
        itemList.dataSource = self
        
        itemList.register(UINib(nibName: "ExerciseDurationTableViewCell", bundle: nil), forCellReuseIdentifier: "ExerciseDurationTableViewCell")
        itemList.register(UINib(nibName: "CaloriesBurnedTableViewCell", bundle: nil), forCellReuseIdentifier: "CaloriesBurnedTableViewCell")
        
        if exerciseData != nil{
            perCalorie = Int(exerciseData!.totalCaloriesBurned)!
            lblExerciseName.text = exerciseData!.exerciseName
        }
        
        
    
    }
    
    func durationEntered(value: String) {
        if let num = Int(value){
            duration = num
        }
        totalCaloriesBurned = duration * perCalorie
        let indexPathRow:Int = 1
        let indexPosition = IndexPath(row: indexPathRow, section: 0)
        itemList.reloadRows(at: [indexPosition], with: .none)
       // itemList.reloadData()
    }

    @IBAction func btnAddExerciseAction(_ sender: Any) {
        let params = ["totalCaloriesBurned": "\(totalCaloriesBurned)",
                      "exerciseType": exerciseData!.exerciseType,
                      "numberOfSets": "",
                      "numberOfRepetitionPerSet": "",
                      "exerciseName": lblExerciseName.text!,
                      "duration": "\(duration)"]
        addExerciseApi(params: params)
    }
    

    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
extension ExerciseDetailsViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
      
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = itemList.dequeueReusableCell(withIdentifier: "ExerciseDurationTableViewCell") as? ExerciseDurationTableViewCell
            cell?.delegate = self
            cell?.selectionStyle = .none
            return cell!
        }else{
            let cell = itemList.dequeueReusableCell(withIdentifier: "CaloriesBurnedTableViewCell") as? CaloriesBurnedTableViewCell
            cell?.lblCaloriesBurnt.text = "\(totalCaloriesBurned)"
            cell?.selectionStyle = .none
            return cell!
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 140
     
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
     
       
    }
    
}
extension ExerciseDetailsViewController{
    
    func addExerciseApi(params:[String:String]){
        
        /*
        if !AppHelper.isInterNetConnectionAvailable(){
            showAlertWithMessage(ConstantStrings.ALERT, ConstantStrings.pleaseCheckYourInternetConnection)
            return
        }
        */
      //  ERProgressHud.sharedInstance.showBlurView(withTitle: "Loading...")
        WebServiceHandler.performPOSTRequest(withURL: addExerciseByUser, andParameters: params) {(result,error) in
            if result != nil{
                print(result!)
                
                let statusCode = result!["statusCode"]?.int
                if statusCode == 200
                {
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let controller = storyboard.instantiateViewController(withIdentifier: "TabbarController")
                     APPDELEGATE.window?.rootViewController = controller
                }
                else{
                    if let message = result!["message"]?.string{
                       // AppHelper.showAlertView(message: errorMsg)
                        self.showAlertWithMessage(ConstantStrings.ALERT, message)
                    }
                }
              //  ERProgressHud.sharedInstance.hide()
            }else{
                self.showAlertWithMessage("ALERT",  "Something Went Wrong")
              //  ERProgressHud.sharedInstance.hide()
            }
        }
    }
}

