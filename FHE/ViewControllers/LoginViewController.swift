//
//  LoginViewController.swift
//  FHE
//
//  Created by Macintosh on 21/10/21.
//  Copyright © 2021 sylar. All rights reserved.
//

import UIKit
import Alamofire_SwiftyJSON
import SwiftyJSON

class LoginViewController: BaseViewControllerClass {
    static var viewControllerId = "LoginViewController"
    
    static var storyBoard = "Main"
    var loginData:LoginDataModel?
  
    

    @IBOutlet weak var txtFieldPhone: UITextField!
    @IBOutlet weak var btnCode: UIButton!
    let titleArray = ["TAILOR MADE","TRACK YOUR","PERSONALIZED"]
    let subTitleArray = ["Workouts","Habits","Meal plans"]
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var sliderCollectionView: UICollectionView!
    @IBOutlet weak var pageView: UIPageControl!
    
    var imgArr = [  UIImage(named:"image1"),
                    UIImage(named:"image2") ,
                    UIImage(named:"image3") ,
                   ]
    
    var timer = Timer()
    var counter = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtFieldPhone.attributedPlaceholder = NSAttributedString(string: "Enter Mobile Number",
                                                                  attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        self.navigationController?.isNavigationBarHidden = true
        pageView.numberOfPages = imgArr.count
        pageView.currentPage = 0
        DispatchQueue.main.async {
            self.timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.changeImage), userInfo: nil, repeats: true)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func checkLoginValidation() -> Bool {
        
        self.view.endEditing(true)
       
        if self.txtFieldPhone.text!.isEmpty {
            
            self.txtFieldPhone.becomeFirstResponder()
            self.showAlertWithMessage(ConstantStrings.ALERT, ConstantStrings.MOBILE_NO_FIELD_IS_REQUIRED)
            return false
        }
        
        if (self.txtFieldPhone.text?.isValid(regex: .phone))!{
            
        }else{
            self.showAlertWithMessage(ConstantStrings.ALERT, ConstantStrings.PLEASE_ENTER_VALID_MOBILE)
            return false
        }
        

        
        return true
    }
    
   @objc func changeImage() {
    
    if counter < imgArr.count {
        let index = IndexPath.init(item: counter, section: 0)
        self.sliderCollectionView.scrollToItem(at: index, at: .centeredHorizontally, animated: true)
        self.lblTitle.text = titleArray[counter]
        self.lblSubTitle.text = subTitleArray[counter]
        pageView.currentPage = counter
        counter += 1
    } else {
        counter = 0
        let index = IndexPath.init(item: counter, section: 0)
        self.sliderCollectionView.scrollToItem(at: index, at: .centeredHorizontally, animated: false)
        pageView.currentPage = counter
        self.lblTitle.text = titleArray[counter]
        self.lblSubTitle.text = subTitleArray[counter]
        counter = 1
    }
        
    }
    
    @IBAction func btnContinueAction(_ sender: Any) {
        if checkLoginValidation(){
            let params = ["phoneNo": txtFieldPhone.text!]
            callLoginApi(params: params)
            
        }
        
    }
    

}

extension LoginViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imgArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        if let vc = cell.viewWithTag(111) as? UIImageView {
            vc.image = imgArr[indexPath.row]
        }
        return cell
    }
}

extension LoginViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = sliderCollectionView.frame.size
        return CGSize(width: size.width, height: size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }

}
extension LoginViewController{
    
    func callLoginApi(params:[String:String]){
        
        /*
        if !AppHelper.isInterNetConnectionAvailable(){
            showAlertWithMessage(ConstantStrings.ALERT, ConstantStrings.pleaseCheckYourInternetConnection)
            return
        }
        */
      //  ERProgressHud.sharedInstance.showBlurView(withTitle: "Loading...")
        WebServiceHandler.performPOSTRequest(withURL: loginUrl, andParameters: params) {(result,error) in
            if result != nil{
                print(result!)
                
                let statusCode = result!["statusCode"]?.int
                if statusCode == 200
                {
                   
                    if let data = result?["data"]?.dictionary{
                        print(data)
                        self.loginData = LoginDataModel.parseLoginData(details: result!["data"]!)
                        let controller = VerifyOTPViewController.instantiateFromStoryBoard()
                        controller.phoneNumber = self.txtFieldPhone.text!
                        self.push(controller)
                        self.showAlertWithMessage(ConstantStrings.ALERT, "OTP is :\(self.loginData!.mobileotp)")
                        print(self.loginData)
                        
                    }
                }
                else{
                    if let message = result!["message"]?.string{
                       // AppHelper.showAlertView(message: errorMsg)
                        self.showAlertWithMessage(ConstantStrings.ALERT, message)
                    }
                }
              //  ERProgressHud.sharedInstance.hide()
            }else{
                self.showAlertWithMessage("ALERT",  "Something Went Wrong")
              //  ERProgressHud.sharedInstance.hide()
            }
        }
    }
}
