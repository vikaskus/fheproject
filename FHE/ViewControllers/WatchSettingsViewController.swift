//
//  WatchSettingsViewController.swift
//  FHE
//
//  Created by Macintosh on 08/11/21.
//  Copyright © 2021 sylar. All rights reserved.
//

import UIKit

class WatchSettingsViewController: BaseViewControllerClass {

    @IBOutlet weak var itemList: UITableView!
    let listArray = ["Watch Faces","Notification"]
    
    static var viewControllerId = "WatchSettingsViewController"
    static var storyBoard = "Main"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setInitials()
    }
    

    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setInitials(){
        
       // CRPSmartBandSDK.sharedInstance.delegate = self
        itemList.delegate = self
        itemList.dataSource = self
        
        itemList.register(UINib(nibName: "ProfileTabTableViewCell", bundle: nil), forCellReuseIdentifier: "ProfileTabTableViewCell")
    }
    

}
extension WatchSettingsViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listArray.count
      
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = itemList.dequeueReusableCell(withIdentifier: "ProfileTabTableViewCell") as? ProfileTabTableViewCell
        cell?.lblTitle.text = listArray[indexPath.row]
        cell?.selectionStyle = .none
        return cell!
        
       
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 70
     
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            let controller = WatchFacesViewController.instantiateFromStoryBoard()
            self.push(controller)
          
        }else if indexPath.row == 1{
            let controller = WatchNotificationsViewController.instantiateFromStoryBoard()
            self.push(controller)
        }
    }
}
