//
//  WatchNotificationsViewController.swift
//  FHE
//
//  Created by Macintosh on 08/11/21.
//  Copyright © 2021 sylar. All rights reserved.
//

import UIKit
import CRPSmartBand
import CoreBluetooth

class NotificationData{
    let name:String
    var isSelected:Bool
    var type:NotificationType
    
    init(name:String,isSelected:Bool,type:NotificationType) {
        self.name = name
        self.isSelected = isSelected
        self.type = type
    }
}
class WatchNotificationsViewController: BaseViewControllerClass {
    
    @IBOutlet weak var itemList: UITableView!
    
    var selectedIndexsArray = [Int]()
    let manager = CRPSmartBandSDK.sharedInstance
    var dataArray = UserDetails.sharedInstance.notificationData
    
    static var viewControllerId = "WatchNotificationsViewController"
    static var storyBoard = "Main"
    var swis = [NotificationType]()
  //  let dataArray = ["Phone","Message","Facebook","Twitter","WhatsApp","Skype","Instagram","KakaoTalk","Line","WeChat"]
    
    var managerBLE: CBCentralManager?

    override func viewDidLoad() {
        super.viewDidLoad()
        setInitialsWatchData()
        setInitials()
        

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        dataArray = UserDetails.sharedInstance.notificationData
      //  setNotificationList()
        itemList.reloadData()
    }
    
    func setInitials(){
       // CRPSmartBandSDK.sharedInstance.delegate = self
        itemList.delegate = self
        itemList.dataSource = self
        itemList.allowsMultipleSelectionDuringEditing = true
        
        itemList.register(UINib(nibName: "WatchNotificationTableViewCell", bundle: nil), forCellReuseIdentifier: "WatchNotificationTableViewCell")
    }
    
   
    func setNotificationList(){
        dataArray = [
            NotificationData(name: "Phone", isSelected: false, type: NotificationType.phone),
            NotificationData(name: "Message", isSelected: false, type: NotificationType.messages),
            NotificationData(name: "Facebook", isSelected: false, type: NotificationType.facebook),
            NotificationData(name: "Twitter", isSelected: false, type: NotificationType.twitter),
            NotificationData(name: "WhatsApp", isSelected: false, type: NotificationType.whatsApp),
            NotificationData(name: "Skype", isSelected: false, type: NotificationType.skype),
            NotificationData(name: "Instagram", isSelected: false, type: NotificationType.instagram),
            NotificationData(name: "KakaoTalk", isSelected: false, type: NotificationType.kakaoTalk),
            NotificationData(name: "Line", isSelected: false, type: NotificationType.line),
            NotificationData(name: "WeChat", isSelected: false, type: NotificationType.wechat)
        ]
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
extension WatchNotificationsViewController:CRPManagerDelegate,CBCentralManagerDelegate {
    
    

    func bluetoothStatus() {
        managerBLE = CBCentralManager(delegate: self, queue: nil, options: nil)
    }

    

    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch central.state {
        case .poweredOn:
            break
        case .poweredOff:
            print("Bluetooth is Off.")
        //    self.showAlertWithMessage("ALERT", "Please turn On your Bluetooth")
        //    startScanTapped = false
       //     btnStartStop.setTitle("START SCAN", for: .normal)
            
            break
        case .resetting:
            break
        case .unauthorized:
            break
        case .unsupported:
            break
        case .unknown:
            break
        default:
            break
        }
    }
    
    
    
    
    
    func didState(_ state: CRPState) {
        print("Connect state: \(state.rawValue)")
        if state == .connected{
            UserDetails.sharedInstance.connectedDevice = CRPSmartBandSDK.sharedInstance.currentCRPDiscovery
            UserDetails.sharedInstance.isDeviceConnected = true
           
            let alert = UIAlertController(title: "", message: "Connected to \(UserDetails.sharedInstance.connectedDevice.remotePeripheral.name!)", preferredStyle: UIAlertController.Style.alert)
            let cancel = UIAlertAction(title: "OK", style: UIAlertAction.Style.default,handler: {
            action in
                self.dismiss(animated: true, completion: nil)
               
            })
            
                    alert.addAction(cancel)
                    self.present(alert, animated: true, completion: nil)
            self.getData()
            CRPSmartBandSDK.sharedInstance.checkDFUState { (dfu, err) in
                print("dfu =\(dfu)")
            }
        }
    }
    
    func didBluetoothState(_ state: CRPBluetoothState) {
        print(state)
    }
    
    func receiveSteps(_ model: StepModel) {
        print(model)
    
        UserDetails.sharedInstance.steps = "\(model.steps)"
        UserDetails.sharedInstance.stepsTime = "\(model.time)"
        UserDetails.sharedInstance.stepsCalorie = "\(model.calory)"
        UserDetails.sharedInstance.stepsDistance = "\(model.distance)"
        
        let param = [ "currentStatus": "0",
                      "stepCount": UserDetails.sharedInstance.steps,
                      "caloriesBurned": UserDetails.sharedInstance.stepsCalorie,
                      "distance": UserDetails.sharedInstance.stepsDistance,
                      "duration": UserDetails.sharedInstance.stepsTime]
        
       
        NotificationCenter.default.post(name: .postNotification, object: "myObject", userInfo: ["dataType": "steps","walkSteps":"\(model.steps)","walkDuration":"\(model.time)","walkDistance":"\(model.distance)","walkCalories":"\(model.calory)"])
       
        
        let manager = CRPSmartBandSDK.sharedInstance
        
       // manager.setDial(1)
        manager.getSportData({ (model, error) in
            var sportsData = [SportsModel]()
            
            for i in model{
                var sportsString = ""
                switch(i.type.rawValue){
                case  0:
                sportsString = "walking"

                case 1:
                sportsString = "run"

                case 2:
                sportsString = "cycling"

                case  3:
                sportsString = "skipping"

                case 4:
                sportsString = "badminton"

                case  5:
                sportsString = "basketball"

                case 6:
                sportsString = "football"

                case  7:
                sportsString = "swimming"

                case  8:
                sportsString = "hiking"

                case 9:
                sportsString = "tennis"

                case 10:
                sportsString = "rugby"

                case 11:
                sportsString = "golf"

                case 12:
                sportsString = "yoga"

                case 13:
                sportsString = "workout"

                case 14:
                sportsString = "dance"

                case 15:
                sportsString = "baseball"

                case  16:
                sportsString = "elliptical"

                case 17:
                sportsString = "indoorCycling"

                case  18:
                sportsString = "training"

                case  19:
                sportsString = "rowingMachine"

                case  20:
                sportsString = "trailRun"

                case  21:
                sportsString = "ski"

                case  22:
                sportsString = "bowling"

                case  23:
                sportsString = "dumbbells"

                case  24:
                sportsString = "sit_ups"

                case  25:
                sportsString = "onfoot"

                case  26:
                sportsString = "indoorWalk"

                case  27:
                sportsString = "indoorRun"
                default:
                    sportsString = ""
                }
                
                
                sportsData.append(SportsModel(date: "\(i.date)", startTime: "\(i.startTime)", endTime: "\(i.endTime)", vaildTime: "\(i.vaildTime)", type: "\(sportsString)", step: "\(i.step)", distance: "\(i.distance)", kcal: "\(i.kcal)"))
            }
            AppHelper.sharedInstance.sportsData = sportsData
            
            NotificationCenter.default.post(name: .postNotification, object: "myObject", userInfo: ["dataType": "sports"])
           
            print(model)
        })
        
        manager.getSleepData({ (model, error) in
            print(model)
            var sleepModel = [SleepModel]()
            /*
            for i in model.detail{
                sleepModel.append(SleepModel(total: i.t, start: , end: , type: ))
            }
            */
            
            let sleepArray = model.detail
            for i in model.detail{
                let sleepData = i as? NSDictionary
                sleepModel.append(SleepModel(total: i["total"] as! String, start: i["start"] as! String, end: i["end"] as! String, type: i["type"] as! String))
            }
            AppHelper.sharedInstance.sleepData = sleepModel
            
            print("Deep sleep\(model.deep)Minute Light sleep\(model.light)Minute")
            NotificationCenter.default.post(name: .postNotification, object: "myObject", userInfo: ["dataType": "sleep","Light":"\(model.light)","Deep":"\(model.deep)"/*,"drillDown":sleepModel*/])
        })

        
    }
    
    func receiveHeartRate(_ heartRate: Int) {
        print(heartRate)
        UserDetails.sharedInstance.heartRateData = "\(heartRate)"
        NotificationCenter.default.post(name: .postNotification, object: "myObject", userInfo: ["dataType": "heartRate","heartRate":"\(heartRate)"])
    }
    
    func receiveHeartRateAll(_ model: HeartModel) {
        print(model)
        
    }
    
    func receiveBloodPressure(_ heartRate: Int, _ sbp: Int, _ dbp: Int) {
        print(heartRate)
        UserDetails.sharedInstance.sbp = "\(sbp)"
        UserDetails.sharedInstance.dbp = "\(dbp)"
        NotificationCenter.default.post(name: .postNotification, object: "myObject", userInfo: ["dataType": "bloodPressure","heartRate":"\(heartRate)","sbp":"\(sbp)","dbp":"\(dbp)"])
    }
    
    func receiveSpO2(_ o2: Int) {
        UserDetails.sharedInstance.oxygenData = "\(o2)"
        print(o2)
        NotificationCenter.default.post(name: .postNotification, object: "myObject", userInfo: ["dataType": "o2","o2":"\(o2)"])
        
    }
    
    
    
    

    func setInitialsWatchData(){
        CRPSmartBandSDK.sharedInstance.delegate = self
        
        
    }
    
    
    func recevieTakePhoto() {
        print("recevieTakePhoto")
    }
    
    
    func receiveUpgradeScreen(_ state: CRPUpgradeState, _ progress: Int) {
        print("state = \(state.description()), progress = \(progress)")
        
    }
    
    func receiveRealTimeHeartRate(_ heartRate: Int, _ rri: Int) {
        print("heart rate is \(heartRate)")
        
    }
    func receiveUpgrede(_ state: CRPUpgradeState, _ progress: Int) {
        print("state = \(state.description()), progress = \(progress)")
    }
    func recevieWeather() {
        print("recevieWeather")
    }
    
    
    func getData(){
        
            manager.getSteps({ (model, error) in
                print(model)
//                let cal
//                let text = "Step:\(model)"
                print("\(model.steps)step \(model.calory)kcal \(model.distance)m , \(model.time)s")
            })
      
            manager.getSleepData({ (model, error) in
                print(model)
                var sleepModel = [SleepModel]()
                /*
                for i in model.detail{
                    sleepModel.append(SleepModel(total: i.t, start: , end: , type: ))
                }
                */
                
                let sleepArray = model.detail
                for i in model.detail{
                    let sleepData = i as? NSDictionary
                    sleepModel.append(SleepModel(total: i["total"] as! String, start: i["start"] as! String, end: i["end"] as! String, type: i["type"] as! String))
                }
                AppHelper.sharedInstance.sleepData = sleepModel
                
                print("Deep sleep\(model.deep)Minute Light sleep\(model.light)Minute")
                NotificationCenter.default.post(name: .postNotification, object: "myObject", userInfo: ["dataType": "sleep","Light":"\(model.light)","Deep":"\(model.deep)"/*,"drillDown":sleepModel*/])
            })
    
            manager.getFeatures({ (features, error) in
                print("Support：\(features)")
            })
        
//            if let zippath = Bundle.main.path(forResource: "MOY-SFP4-1.7.9", ofType: "zip"){
//                manager.getOTAMac { (otaMac, error) in
//                    manager.startOTAFromFile(mac: otaMac, zipFilePath: zippath, isUser: false,false)
//                }
//            }
       
            manager.getSoftver({ (ver, error) in
                print(error)
              //  (self.view.viewWithTag(201) as! UILabel).text = ver
                print("varsion：\(ver)")
            })
      
            manager.getBattery({ (battery, error) in
              //  (self.view.viewWithTag(202) as! UILabel).text = String(battery)
                print("Battery：\(battery)")
            })
       
            manager.getGoal({ (value, error) in
              //  (self.view.viewWithTag(203) as! UILabel).text = String(value)
                print("Goal：\(value)")
            })
        
            manager.getDominantHand({ (value, error) in
               // (self.view.viewWithTag(204) as! UILabel).text = String(value)
                print("Hand：\(value)")
            })
       
            manager.getAlarms({ (alarms, error) in
                print(alarms)
            })
     
            manager.getProfile({ (profile, error) in
              print("\(profile)")
            })
       
            manager.getLanguage { (value, CRPErrorerror) in
                print("\(value)")
            } _: { (indexs, error) in
                print("index = \(indexs)")
            }

       
            manager.getDial({ (value, error) in
                print("\(value)")
            })
       
            manager.getRemindersToMove({ (value, error) in
                print("\(value)")
            })
       
            manager.getQuickView({ (value, error) in
                print("\(value)")
            })

            manager.getUnit({ (value, error) in
                print("\(value)")
            })
      
            manager.getTimeformat({ (value, error) in
                print("\(value)")
            })
     
            manager.getMac({ (value, error) in
                print("\(value)")
            })
        
            manager.getNotifications({ (value, error) in
                print(value)
            })
        
            manager.getNotifications({ (value, error) in
                print(value)
                print("\(value.description)")
            })
           
  
          //  manager.setStartSpO2()
          
      
         //   manager.setStopSpO2()
//            manager.getHeartData()
            manager.getMac { (mac, err) in
                self.manager.getSoftver { (ver, err) in
                    self.manager.checkLatest(mac, ver) { (newInfo, tpNewInfo, err) in
                        print("new = \(newInfo), tpNewInfo = \(tpNewInfo)")
                    }
                }
            }
           
        
    }
        
}
extension WatchNotificationsViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
      
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = itemList.dequeueReusableCell(withIdentifier: "WatchNotificationTableViewCell") as? WatchNotificationTableViewCell
        cell?.notificationTitle.text = dataArray[indexPath.row].name
        cell?.notificationImage.image = UIImage(named: dataArray[indexPath.row].name)
        if dataArray[indexPath.row].isSelected == false{
            cell?.btnSwitch.isOn = false
        }else{
            cell?.btnSwitch.isOn = true
        }
        /*
        if selectedIndexsArray.contains(indexPath.row){
            cell?.btnSwitch.isOn = true
        }else{
            cell?.btnSwitch.isOn = false
        }
        */
       
        cell?.selectionStyle = .none
        return cell!
        
       
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 65
     
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if dataArray[indexPath.row].isSelected == false{
            dataArray[indexPath.row].isSelected = true
            
        }else{
            dataArray[indexPath.row].isSelected = false
        }
        
        for element in dataArray{
            if element.isSelected{
                swis.append(element.type)
            }
            manager.setNotification(swis)
            
        }
        swis.removeAll()
        /*
        if selectedIndexsArray.contains(indexPath.row){
        
        }else{
            selectedIndexsArray.append(indexPath.row)
        }
        */
        itemList.reloadData()
    }
    
    /*
    func tableView(_ tableView: UITableView, shouldBeginMultipleSelectionInteractionAt indexPath: IndexPath) -> Bool {
        true
    }
    
    func tableView(_ tableView: UITableView, didBeginMultipleSelectionInteractionAt indexPath: IndexPath) {
     
    }
    */
}
