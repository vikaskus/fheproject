//
//  ReminderListViewController.swift
//  FHE
//
//  Created by Macintosh on 29/10/21.
//  Copyright © 2021 sylar. All rights reserved.
//

import UIKit

class ReminderListViewController: BaseViewControllerClass {
    
    static var viewControllerId = "ReminderListViewController"
    static var storyBoard = "Main"
    
    var reminderList = [ReminderModel]()

    @IBOutlet weak var itemList: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setInitials()
    }
    
    func setInitials(){
        
       // CRPSmartBandSDK.sharedInstance.delegate = self
        itemList.delegate = self
        itemList.dataSource = self
        
        itemList.register(UINib(nibName: "ReminderListTableViewCell", bundle: nil), forCellReuseIdentifier: "ReminderListTableViewCell")
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let params = ["":""]
        fetchReminderApi(params: params)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnAddReminderAction(_ sender: Any) {
        
        let controller = AddReminderViewController.instantiateFromStoryBoard()
        self.push(controller)
        
    }
    
}
extension ReminderListViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.reminderList.count
      
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = itemList.dequeueReusableCell(withIdentifier: "ReminderListTableViewCell") as? ReminderListTableViewCell
        if self.reminderList.count > 0{
            cell?.lblReminderTitle.text = self.reminderList[indexPath.row].remainderBody
            cell?.lblDate.text =  self.reminderList[indexPath.row].atThisTime
        }
        cell?.selectionStyle = .none
        return cell!
        
       
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 70
     
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let controller = ReminderDetailsViewController.instantiateFromStoryBoard()
        self.push(controller)
       
    }
    
}
extension ReminderListViewController{
    
    func fetchReminderApi(params:[String:String]){
        
        /*
        if !AppHelper.isInterNetConnectionAvailable(){
            showAlertWithMessage(ConstantStrings.ALERT, ConstantStrings.pleaseCheckYourInternetConnection)
            return
        }
        */
      //  ERProgressHud.sharedInstance.showBlurView(withTitle: "Loading...")
        WebServiceHandler.performPOSTRequest(withURL: fetchReminder, andParameters: params) {(result,error) in
            if result != nil{
                print(result!)
                
                let statusCode = result!["statusCode"]?.int
                if statusCode == 200
                {
                    self.reminderList = ReminderModel.getAllJSONList(dataArray: (result!["data"]?.array)!)
                    print(self.reminderList)
                    self.itemList.reloadData()
                }
                else{
                    if let message = result!["message"]?.string{
                       // AppHelper.showAlertView(message: errorMsg)
                        self.showAlertWithMessage(ConstantStrings.ALERT, message)
                    }
                }
              //  ERProgressHud.sharedInstance.hide()
            }else{
                self.showAlertWithMessage("ALERT",  "Something Went Wrong")
              //  ERProgressHud.sharedInstance.hide()
            }
        }
    }
}
