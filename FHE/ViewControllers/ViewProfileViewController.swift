//
//  ViewProfileViewController.swift
//  FHE
//
//  Created by Macintosh on 29/10/21.
//  Copyright © 2021 sylar. All rights reserved.
//

import UIKit

class ViewProfileViewController: BaseViewControllerClass {
    
    static var viewControllerId = "ViewProfileViewController"
    static var storyBoard = "Main"

    @IBOutlet weak var lblFirstName: UILabel!
    @IBOutlet weak var lblLastName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblMobile: UILabel!
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var lblHeight: UILabel!
    @IBOutlet weak var lblWeight: UILabel!
    @IBOutlet weak var btnEditProfile: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setData()
    }
    
    func setData(){
        
        lblFirstName.text = UserDetails.sharedInstance.firstName
        lblLastName.text = UserDetails.sharedInstance.lastName
        lblMobile.text = UserDetails.sharedInstance.phone
        
        lblEmail.text = UserDetails.sharedInstance.email
        lblGender.text = UserDetails.sharedInstance.gender
        lblHeight.text = UserDetails.sharedInstance.height + " " + UserDetails.sharedInstance.heightMeaseureIn
        lblWeight.text =  UserDetails.sharedInstance.weight + " " + UserDetails.sharedInstance.weightMeasureIn
                
    }
    


    @IBAction func btnEditProfileAction(_ sender: Any) {
        let controller = EditProfileViewController.instantiateFromStoryBoard()
        self.push(controller)
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
