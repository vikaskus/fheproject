//
//  PurchasePlansViewController.swift
//  FHE
//
//  Created by Macintosh on 02/11/21.
//  Copyright © 2021 sylar. All rights reserved.
//

import UIKit

class PurchasePlansViewController: BaseViewControllerClass {
    @IBOutlet weak var itemList: UITableView!
    
    @IBOutlet weak var sliderLabel1: UILabel!
    @IBOutlet weak var sliderLabel2: UILabel!
    let titleArray = ["Weight Loss Package","Cross Fit"]
    var planArray = ["-Get a curated meal plan\n-Free assement\n-Realistic indian meals","-Custom workouts\n-No equipments\n30 mins everyday"]
    
    
    static var viewControllerId = "PurchasePlansViewController"
    static var storyBoard = "Main"
    
    var isNutrition = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setInitials()

        // Do any additional setup after loading the view.
    }
    
    func setInitials(){
        sliderLabel2.isHidden = true
        
       // CRPSmartBandSDK.sharedInstance.delegate = self
        itemList.delegate = self
        itemList.dataSource = self
        
        itemList.register(UINib(nibName: "PlansTableViewCell", bundle: nil), forCellReuseIdentifier: "PlansTableViewCell")
    
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func btnNutritionAction(_ sender: Any) {
        sliderLabel1.isHidden = false
        sliderLabel2.isHidden = true
        isNutrition = true
        itemList.reloadData()
    }
    @IBAction func btnFitnessPlan(_ sender: Any) {
        sliderLabel1.isHidden = true
        sliderLabel2.isHidden = false
        isNutrition = false
        itemList.reloadData()
    }
    
}
extension PurchasePlansViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
      return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = itemList.dequeueReusableCell(withIdentifier: "PlansTableViewCell") as? PlansTableViewCell
        if isNutrition{
            cell?.lblTitle.text = titleArray[0]
            cell?.txtFieldView.text = planArray[0]
          
        }else{
            cell?.lblTitle.text = titleArray[1]
            cell?.txtFieldView.text = planArray[1]
            
        }
        cell?.selectionStyle = .none
        return cell!
        
       
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 217
     
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let controller = ReminderDetailsViewController.instantiateFromStoryBoard()
        self.push(controller)
       
    }
    
}
