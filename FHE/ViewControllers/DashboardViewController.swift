//
//  DashboardViewController.swift
//  FHE
//
//  Created by Macintosh on 23/10/21.
//  Copyright © 2021 sylar. All rights reserved.
//

import UIKit
import CRPSmartBand
import CoreBluetooth
enum SelectedSlider{
    case activity
    case nutrition
    case sleep
}

class DashboardViewController: BaseViewControllerClass {
    let manager = CRPSmartBandSDK.sharedInstance
    
    @IBOutlet weak var sliderLabel1: UILabel!
    @IBOutlet weak var sliderLabel2: UILabel!
    @IBOutlet weak var sliderLabel3: UILabel!
    @IBOutlet weak var itemList: UITableView!
    
    @IBOutlet weak var rewardTitle: UILabel!
    @IBOutlet weak var lblTodaydate: UILabel!
    @IBOutlet weak var rewardImageView: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    var dataTypeImageArray = ["steps","bloodPressure","pulse","oxygen"]
    var dataTypeTitleArray = ["Steps","BP","Pulse","Oxygen"]
    
    var dashboardData:DashboardDataModel?
    
    var totalExerciseData:TotalExerciseModel?
    var totalExerciseList = [AddedExerciseModel]()
    var totalSportsList = [SportsExerciseModel]()
    
    
    @IBOutlet weak var watchDataCollectionView: UICollectionView!
    static var viewControllerId = "UserDataViewController"
    static var storyBoard = "Main"
    
    
    var selectedSlider = SelectedSlider.activity
    @IBOutlet weak var addDeviceTitle: UILabel!
    @IBOutlet weak var addDeviceImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setInitials()
       
        self.navigationController?.isNavigationBarHidden = true
       
        print(UserDetails.sharedInstance.accessToken)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setAddDeviceButtonState()
        setProfileAndReward()
        updateWatchData()
        getDashboardDataApi()
        getExerciseListApi()
        getUserProfileApi()
        let params = [  "timeFrame":"weekly",
                        "dateString":""]
        getSleepDataApi(params: params)
        getSportsDataApi()
    }
    
    
    func setInitials(){
       // CRPSmartBandSDK.sharedInstance.delegate = self
        itemList.delegate = self
        itemList.dataSource = self
        
        itemList.register(UINib(nibName: "StepTableViewCell", bundle: nil), forCellReuseIdentifier: "StepTableViewCell")
        itemList.register(UINib(nibName: "ExerciseTableViewCell", bundle: nil), forCellReuseIdentifier: "ExerciseTableViewCell")
        itemList.register(UINib(nibName: "SleepTableViewCell", bundle: nil), forCellReuseIdentifier: "SleepTableViewCell")
        
        
        
        
        watchDataCollectionView.delegate = self
        watchDataCollectionView.dataSource = self
        
        watchDataCollectionView.register(UINib(nibName: "WatchDataCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "WatchDataCollectionViewCell")
        sliderLabel1.isHidden = false
        sliderLabel2.isHidden = true
        sliderLabel3.isHidden = true
        
    }
    
    
    func setProfileAndReward(){
        lblName.text = "Hi \(UserDetails.sharedInstance.firstName)"
    }
    
    func setAddDeviceButtonState(){
        if UserDetails.sharedInstance.connectedDevice != nil{
            addDeviceTitle.text = UserDetails.sharedInstance.connectedDevice.remotePeripheral.name! + " Connected"
            addDeviceImage.image = UIImage(named: "minusSmall")
            
        }else{
            addDeviceTitle.text = "ADD DEVICE"
            addDeviceImage.image = UIImage(named: "plus-circle")

        }
    }
    
    
    @IBAction func btnActivityAction(_ sender: Any) {
        selectedSlider = SelectedSlider.activity
        sliderLabel1.isHidden = false
        sliderLabel2.isHidden = true
        sliderLabel3.isHidden = true
        itemList.reloadData()
    }
    
    @IBAction func btnNutritionAction(_ sender: Any) {
        selectedSlider = SelectedSlider.nutrition
        sliderLabel1.isHidden = true
        sliderLabel2.isHidden = false
        sliderLabel3.isHidden = true
        itemList.reloadData()
    }
    
    @IBAction func btnSleepAction(_ sender: Any) {
        selectedSlider = SelectedSlider.sleep
        sliderLabel1.isHidden = true
        sliderLabel2.isHidden = true
        sliderLabel3.isHidden = false
        itemList.reloadData()
    }
    
    
    @IBAction func btnAddDeviceAction(_ sender: Any) {
        if UserDetails.sharedInstance.isDeviceConnected{
            let alert = UIAlertController(title: "ALERT", message: "Are you sure you want to disconnect \(UserDetails.sharedInstance.connectedDevice.remotePeripheral.name!)", preferredStyle: UIAlertController.Style.alert)
            let cancel = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil)
            let confirm = UIAlertAction(title: "Confirm", style: UIAlertAction.Style.default,handler: {
            action in
                CRPSmartBandSDK.sharedInstance.disConnet()
               
                /*
                CRPSmartBandSDK.sharedInstance.remove { (state, err) in
                    print(state)
                }
                */
                CRPSmartBandSDK.sharedInstance
                var name = UserDetails.sharedInstance.connectedDevice.remotePeripheral.name!
                UserDetails.sharedInstance.connectedDevice = nil
                UserDetails.sharedInstance.isDeviceConnected = false
                self.setAddDeviceButtonState()
                self.showAlertWithMessage("ALERT", "Please forget \(name) from settings")
            })
            
            alert.addAction(cancel)
            alert.addAction(confirm)
                    self.present(alert, animated: true, completion: nil)
         
            
        }else{
            let controller = AddDeviceViewController.instantiateFromStoryBoard()
            controller.modalPresentationStyle = .fullScreen
            self.present(controller, animated: true, completion: nil)
            
        }
    }
    
    @IBAction func btnProfileAction(_ sender: Any) {
        let controller = YourProfileViewController.instantiateFromStoryBoard()
        self.push(controller)
    }
    
    
 
}
extension DashboardViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch selectedSlider {
        case .activity:
            return 3
            
        case .nutrition:
            return 1
            
        case .sleep:
            return 1
            
        default:
            return 0
            
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch selectedSlider {
        case .activity:
            if indexPath.row == 0{
                let cell = itemList.dequeueReusableCell(withIdentifier: "StepTableViewCell") as? StepTableViewCell
                cell?.lblDistance.text = UserDetails.sharedInstance.stepsDistance + " m"
                cell?.lblStepCount.text = UserDetails.sharedInstance.steps
                cell?.lblCalorieBurned.text = UserDetails.sharedInstance.stepsCalorie + " KCals"
                cell?.selectionStyle = .none
                return cell!
            }else if indexPath.row == 1{
                let cell = itemList.dequeueReusableCell(withIdentifier: "ExerciseTableViewCell") as? ExerciseTableViewCell
                cell?.lblTitle.text = "EXERCISE"
                cell?.exerciseStackView.isHidden = false
                cell?.btnAddExercise.isHidden = false
                cell?.exerciseStackViewHeightConstraint.constant = 35
                if totalExerciseList.count > 0{
                    cell?.exerciseList = totalExerciseList
                    cell?.isSports = false
                    cell?.itemList.reloadData()
                }
                if totalExerciseData != nil{
                    cell?.lblTotalDuration.text = totalExerciseData?.totalExerciseDuration
                    cell?.lblNumberOfExercise.text = totalExerciseData?.totalExerciseDone
                    cell?.lblTotalCaloriesBurned.text = totalExerciseData?.totalCaloriesBurned
                }
                cell?.delegate = self
                cell?.selectionStyle = .none
                return cell!
            }else{
                let cell = itemList.dequeueReusableCell(withIdentifier: "ExerciseTableViewCell") as? ExerciseTableViewCell
                cell?.lblTitle.text = "SPORTS INFO"
                cell?.btnAddExercise.isHidden = true
                cell?.exerciseStackView.isHidden = true
                cell?.exerciseStackViewHeightConstraint.constant = 0
                if totalSportsList.count > 0{
                    cell?.totalSportsList = totalSportsList
                    cell?.isSports = true
                    cell?.itemList.reloadData()
                }
                /*
                if totalExerciseList.count > 0{
                    cell?.exerciseList = totalExerciseList
                    cell?.itemList.reloadData()
                }
                if totalExerciseData != nil{
                    cell?.lblTotalDuration.text = totalExerciseData?.totalExerciseDuration
                    cell?.lblNumberOfExercise.text = totalExerciseData?.totalExerciseDone
                    cell?.lblTotalCaloriesBurned.text = totalExerciseData?.totalCaloriesBurned
                }
                 */
                cell?.delegate = self
                cell?.selectionStyle = .none
                return cell!
            }
            
            
        case .nutrition:
            let cell = itemList.dequeueReusableCell(withIdentifier: "StepTableViewCell")
            cell?.selectionStyle = .none
            return cell!
            
        case .sleep:
            let cell = itemList.dequeueReusableCell(withIdentifier: "SleepTableViewCell")
            cell?.selectionStyle = .none
            return cell!
            
        default:
            let cell = itemList.dequeueReusableCell(withIdentifier: "StepTableViewCell")
            cell?.selectionStyle = .none
            return cell!
            
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch selectedSlider {
        case .activity:
            
            if indexPath.row == 0{
                return 180
            }else{
                return 200
            }
        case .nutrition:
            return 180
            
        case .sleep:
            return 350
            
        default:
            return 0
            
        }
    }
}
extension DashboardViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataTypeImageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = watchDataCollectionView.dequeueReusableCell(withReuseIdentifier: "WatchDataCollectionViewCell", for: indexPath) as? WatchDataCollectionViewCell
        cell?.dataTypeImage.image = UIImage(named: dataTypeImageArray[indexPath.row])
        cell?.delegate = self
        cell?.index = indexPath.row
        cell?.lblKeyTitle.text = dataTypeTitleArray[indexPath.row]
        if indexPath.row == 0{
            cell?.lblValueTitle.text = UserDetails.sharedInstance.steps
            
        }else if indexPath.row == 1{
            cell?.lblValueTitle.text = "\(UserDetails.sharedInstance.sbp)/\(UserDetails.sharedInstance.dbp)"
            
        }else if indexPath.row == 2{
            cell?.lblValueTitle.text = "\(UserDetails.sharedInstance.heartRateData)"
            
        }else if indexPath.row == 3{
            cell?.lblValueTitle.text = "\(UserDetails.sharedInstance.oxygenData)"
            
        }
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 75, height: 131)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0{
           // manager.setStartSingleHR()
            
        }else if indexPath.row == 1{
            manager.setStartBlood()
            
        }else if indexPath.row == 2{
            manager.setStartSingleHR()
            
        }else if indexPath.row == 3{
            manager.setStartSpO2()
            
        }
    }
    
    
}
extension DashboardViewController:CRPManagerDelegate{
    func didState(_ state: CRPState) {
        
        if state == .connected{
            UserDetails.sharedInstance.connectedDevice = CRPSmartBandSDK.sharedInstance.currentCRPDiscovery
            UserDetails.sharedInstance.isDeviceConnected = true
            setAddDeviceButtonState()
           
            let alert = UIAlertController(title: "", message: "Connected to \(UserDetails.sharedInstance.connectedDevice.remotePeripheral.name!)", preferredStyle: UIAlertController.Style.alert)
            let cancel = UIAlertAction(title: "OK", style: UIAlertAction.Style.default,handler: {
            action in
            })
            
                    alert.addAction(cancel)
                    self.present(alert, animated: true, completion: nil)
          
            CRPSmartBandSDK.sharedInstance.checkDFUState { (dfu, err) in
                print("dfu =\(dfu)")
            }
        }
        print(state)
    }
    
    func didBluetoothState(_ state: CRPBluetoothState) {
        print(state)
    }
    
    func receiveSteps(_ model: StepModel) {
        UserDetails.sharedInstance.steps = "\(model.steps)"
        watchDataCollectionView.reloadData()
        print(model)
    }
    
    func receiveHeartRate(_ heartRate: Int) {
        UserDetails.sharedInstance.heartRateData = "\(heartRate)"
        watchDataCollectionView.reloadData()
        print(heartRate)
    }
    
    func receiveRealTimeHeartRate(_ heartRate: Int, _ rri: Int) {
        print(heartRate)
        UserDetails.sharedInstance.heartRateData = "\(heartRate)"
        watchDataCollectionView.reloadData()
    }
    
    func receiveHeartRateAll(_ model: HeartModel) {
        print(model)
    }
    
    func receiveBloodPressure(_ heartRate: Int, _ sbp: Int, _ dbp: Int) {
        UserDetails.sharedInstance.sbp = "\(sbp)"
        UserDetails.sharedInstance.dbp = "\(dbp)"
        watchDataCollectionView.reloadData()
        print(sbp)
        print(dbp)
    }
    
    func receiveSpO2(_ o2: Int) {
        UserDetails.sharedInstance.oxygenData = "\(o2)"
        watchDataCollectionView.reloadData()
        print(o2)
    }
    
    func receiveUpgrede(_ state: CRPUpgradeState, _ progress: Int) {
        print(state)
    }
    
    func receiveUpgradeScreen(_ state: CRPUpgradeState, _ progress: Int) {
        
    }
    
    func recevieTakePhoto() {
        
    }
    
    
}
extension DashboardViewController{
    
    
    func updateWatchData(){
        
        
        NotificationCenter.default.addObserver(forName: .postNotification, object: nil, queue: nil) { [weak self] (notification) in
            guard let strongSelf = self else {
                return
            }

            if let data = notification.userInfo as? [String:String]{
                print(notification.userInfo)
                if data["dataType"] == "heartRate"{
                    let watchData = ["dataType":"heartRate","heartRate":data["heartRate"] as! String]
                    print(data["heartRate"] as! String)
                    
                    AppHelper.sharedInstance.watchData = watchData
                    let param = ["beatsPerMinute": UserDetails.sharedInstance.heartRateData]
                    
                    self!.uploadHeartRateDataaApi(params: param)
                    
                    
                }else if data["dataType"] == "bloodPressure"{
                    print(data["heartRate"] as! String)
                   // AppHelper.sharedInstance.watchData["sbp"] = data["sbp"] as! String
                  //  AppHelper.sharedInstance.watchData["dbp"] = data["dbp"] as! String
                    print(data["dbp"] as! String)
                    let watchData = ["dataType":"bloodPressure","sbp":data["sbp"] as! String,"dbp":data["dbp"] as! String]
                    
                    AppHelper.sharedInstance.watchData = watchData
                    let param = [  "systolicPressure": UserDetails.sharedInstance.sbp,
                                   "diastolicPressure":  UserDetails.sharedInstance.dbp,
                                   "detailAboutCurrentActivity": "BP SUMMARY AND INFO 2"]
                    
                    self!.uploadBPDataaApi(params: param)
                   
                    
                }else if data["dataType"] == "o2"{
                    print(data["o2"] as! String)
                    AppHelper.sharedInstance.watchData["O2"] = data["o2"] as! String
                    
                    let watchData = ["dataType":"O2","O2":data["o2"] as! String]
                    
                    
                    AppHelper.sharedInstance.watchData = watchData
                    let param = [ "oxygenPercentage": UserDetails.sharedInstance.oxygenData]
                    
                    self!.uploadOxygenDataApi(params: param)
                    
                    
                }else if data["dataType"] == "steps"{
                    var walkSteps = data["walkSteps"] as! String
                    var walkDuration = data["walkDuration"] as! String
                    var walkDistance = data["walkDistance"] as! String
                    var walkCalories = data["walkCalories"] as! String
                    
                    let watchData = ["dataType":"steps","walkSteps":"\(walkSteps)","walkDuration":"\(walkDuration)","walkDistance":"\(walkDistance)","walkCalories":"\(walkCalories)"]
                    let param = [ "currentStatus": "0",
                                  "stepCount": UserDetails.sharedInstance.steps,
                                  "caloriesBurned": UserDetails.sharedInstance.stepsCalorie,
                                  "distance": UserDetails.sharedInstance.stepsDistance,
                                  "duration": UserDetails.sharedInstance.stepsTime]
                    
                    self!.uploadStepsDataApi(params: param)
                    AppHelper.sharedInstance.watchData = watchData
                   // heartRateData["heartRate"] = "\(heartData["heartNum"]!)"
                    
                    AppHelper.sharedInstance.watchData["walkSteps"] = "\(walkSteps)"
                    AppHelper.sharedInstance.watchData["walkDuration"] = "\(walkDuration)"
                    AppHelper.sharedInstance.watchData["walkDistance"] = "\(walkDistance)"
                    AppHelper.sharedInstance.watchData["walkCalories"] = "\(walkCalories)"
                    
                }else if data["dataType"] == "sleep"{
                    var sleepModel = [SleepModel]()
                    var dict = [String:String]()
                    var dictArray = [dict]
                    for element in AppHelper.sharedInstance.sleepData{
                        dictArray.append(["type":element.type,"total":element.total,"start":element.start,"end":element.end,])
                    }
                    let watchData = ["dataType":"sleep","Light":data["Light"] as! String,"Deep":data["Deep"] as! String,"drillDown":dictArray] as [String : Any]
                    
                    AppHelper.sharedInstance.watchDataSleep = watchData
                    
                    let params = ["light": data["Light"] as! String,
                                   "deep": data["Deep"] as! String,
                                   "drillDown": dictArray] as [String : Any]
                    self!.uploadSleepDataApi(params: params)
                   
                }else if data["dataType"] == "sports"{
                    var sportsData = [SportsModel]()
                    var dict = [String:String]()
                    var dictArray = [dict]
                    for element in AppHelper.sharedInstance.sportsData{
                        dictArray.append(["date":element.date,"startTime":element.startTime,"endTime":element.endTime,"vaildTime":element.vaildTime,"type":element.type,"step":element.step,"distance":element.distance,"kcal":element.kcal])
                    }
                    let watchData = ["dataType":"sports","sportsData":dictArray] as [String : Any]
                    let params = ["sportsData": dictArray] as [String : Any]
                    self!.uploadSportsDataApi(params: params)
                    AppHelper.sharedInstance.watchDataSports = watchData
                }
                self!.watchDataCollectionView.reloadData()
                self!.itemList.reloadData()
            }
        }
    }
}
extension DashboardViewController:WatchDataCollectionViewCellDelegate{
    func detailsTapped(index: Int) {
        let controller = SummaryViewController.instantiateFromStoryBoard()
        controller.index = index
        self.push(controller)
    }
    
    
}
extension DashboardViewController{
    
    
    
    func getSleepDataApi(params:[String:String]){
        
        /*
        if !AppHelper.isInterNetConnectionAvailable(){
            showAlertWithMessage(ConstantStrings.ALERT, ConstantStrings.pleaseCheckYourInternetConnection)
            return
        }
        */
        
        WebServiceHandler.performPOSTRequest(withURL: getSleepData, andParameters: params) {(result,error) in
            if result != nil{
                print(result!)
                
                let statusCode = result!["statusCode"]?.int
                if statusCode == 200
                {
                    
                  print(statusCode)
                    if let data = result?["data"]?.dictionary{
                        
                        if let exerciseList = data["allExerciseList"]?.array{
                            self.totalExerciseList = AddedExerciseModel.getAllJSONList(dataArray: exerciseList)
                        }
                        
                        if let exerciseList = data["total"]?.dictionary{
                            self.totalExerciseData = TotalExerciseModel.parseJSONData(details: data["total"]!)
                        }
                        
                       
                        self.itemList.reloadData()
                        print(data)
                    }
                }
                else{
                    if let message = result!["message"]?.string{
                       // AppHelper.showAlertView(message: errorMsg)
                        self.showAlertWithMessage(ConstantStrings.ALERT, message)
                    }
                }

            }else{
                self.showAlertWithMessage("ALERT",  "Something Went Wrong")
                
            }
        }
    }
    
    
    
    func getSportsDataApi(){
        
        /*
        if !AppHelper.isInterNetConnectionAvailable(){
            showAlertWithMessage(ConstantStrings.ALERT, ConstantStrings.pleaseCheckYourInternetConnection)
            return
        }
        */
        
        WebServiceHandler.performGETRequest(withURL: fetchSports) {(result,error) in
            if result != nil{
                print(result!)
                
                let statusCode = result!["statusCode"]?.int
                if statusCode == 200
                {
                    
                  print(statusCode)
                    if let data = result?["data"]?.array{
                        
                        
                            self.totalSportsList = SportsExerciseModel.getAllJSONList(dataArray: data)
                        print(self.totalSportsList)
                        
                        
                        
                        
                       
                        self.itemList.reloadData()
                        print(data)
                    }
                }
                else{
                    if let message = result!["message"]?.string{
                       // AppHelper.showAlertView(message: errorMsg)
                        self.showAlertWithMessage(ConstantStrings.ALERT, message)
                    }
                }

            }else{
                self.showAlertWithMessage("ALERT",  "Something Went Wrong")
                
            }
        }
    }
    
    func uploadSleepDataApi(params:[String:Any]){
        
        /*
        if !AppHelper.isInterNetConnectionAvailable(){
            showAlertWithMessage(ConstantStrings.ALERT, ConstantStrings.pleaseCheckYourInternetConnection)
            return
        }
        */
      //  ERProgressHud.sharedInstance.showBlurView(withTitle: "Loading...")
        WebServiceHandler.performPOSTRequest(withURL: addSleepData, andParameters: params) {(result,error) in
            if result != nil{
                print(result!)
                print("Sleep Updated")
                
                let statusCode = result!["statusCode"]?.int
                if statusCode == 200
                {
                   
                    if let data = result?["data"]?.dictionary{
                        print(data)
                       
                        
                    }
                }
                else{
                    if let message = result!["message"]?.string{
                       // AppHelper.showAlertView(message: errorMsg)
                        self.showAlertWithMessage(ConstantStrings.ALERT, message)
                    }
                }
              //  ERProgressHud.sharedInstance.hide()
            }else{
                self.showAlertWithMessage("ALERT",  "Something Went Wrong")
              //  ERProgressHud.sharedInstance.hide()
            }
        }
    }
    
    
    func uploadSportsDataApi(params:[String:Any]){
        
        /*
        if !AppHelper.isInterNetConnectionAvailable(){
            showAlertWithMessage(ConstantStrings.ALERT, ConstantStrings.pleaseCheckYourInternetConnection)
            return
        }
        */
      //  ERProgressHud.sharedInstance.showBlurView(withTitle: "Loading...")
        WebServiceHandler.performPOSTRequest(withURL: addSports, andParameters: params) {(result,error) in
            if result != nil{
                print(result!)
                print("Sports Updated")
                
                let statusCode = result!["statusCode"]?.int
                if statusCode == 200
                {
                   
                    if let data = result?["data"]?.dictionary{
                        print(data)
                       
                        
                    }
                }
                else{
                    if let message = result!["message"]?.string{
                       // AppHelper.showAlertView(message: errorMsg)
                        self.showAlertWithMessage(ConstantStrings.ALERT, message)
                    }
                }
              //  ERProgressHud.sharedInstance.hide()
            }else{
                self.showAlertWithMessage("ALERT",  "Something Went Wrong")
              //  ERProgressHud.sharedInstance.hide()
            }
        }
    }
    
    
    func getUserProfileApi(){
        /*
        if !AppHelper.isInterNetConnectionAvailable(){
            showAlertWithMessage(ConstantStrings.ALERT, ConstantStrings.pleaseCheckYourInternetConnection)
            return
        }
        */
        
        WebServiceHandler.performGETRequest(withURL: fetchProfileData) {(result,error) in
            if result != nil{
                print(result!)
                
                let statusCode = result!["statusCode"]?.int
                if statusCode == 200
                {
                    
                  print(statusCode)
                    if let data = result?["data"]?.dictionary{
                        
                        if let userData = data["userExists"]?.dictionary{
                            let age = userData["age"]?.string
                            let heightMeasureIn = userData["heightMeasureIn"]?.string
                            let preferedFoodCategory = userData["preferedFoodCategory"]?.string
                            let gender = userData["gender"]?.string
                            let email = userData["email"]?.string
                            let lastName = userData["lastName"]?.string
                            let firstName = userData["firstName"]?.string
                            let currentActivityLevel = userData["currentActivityLevel"]?.string
                            let weightMeasureIn = userData["weightMeasureIn"]?.string
                            let goals = userData["goals"]?.string
                            let height = userData["height"]?.string
                            let weight = userData["weight"]?.string
                            
                           
                            UserDetails.sharedInstance.firstName = firstName != nil ? firstName! : ""
                            UserDetails.sharedInstance.lastName = lastName != nil ? lastName! : ""
                            
                            UserDetails.sharedInstance.email = email != nil ? email! : ""
                            UserDetails.sharedInstance.gender = gender != nil ? gender! : ""
                            UserDetails.sharedInstance.height = height != nil ? height! : ""
                            UserDetails.sharedInstance.weight = weight != nil ? weight! : ""
                            UserDetails.sharedInstance.heightMeaseureIn = heightMeasureIn != nil ? heightMeasureIn! : ""
                            UserDetails.sharedInstance.weightMeasureIn = weightMeasureIn != nil ? weightMeasureIn! : ""
                            UserDetails.sharedInstance.goals = goals != nil ? goals! : ""
                            UserDetails.sharedInstance.currentActivityLevel = currentActivityLevel != nil ? currentActivityLevel! : ""
                            UserDetails.sharedInstance.preferedFoodCategory = preferedFoodCategory != nil ? preferedFoodCategory! : ""
                            
                            self.lblName.text = "Hi \(UserDetails.sharedInstance.firstName)"
                            
                            print(age)
                            
                        }
                        
                        if let userPhone = data["userPhoneNo"]?.dictionary{
                            let phoneNo = userPhone["phoneNo"]?.string
                            UserDetails.sharedInstance.phone = phoneNo!
                            
                        }
                        
                        if let reward = data["reward"]?.dictionary{
                            let rewardType = reward["rewardType"]?.string
                            self.rewardTitle.text = rewardType
                            self.rewardImageView.image = UIImage(named: rewardType!)
                            
                        }
                       
                        self.itemList.reloadData()
                        print(data)
                    }
                }
                else{
                    if let message = result!["message"]?.string{
                       // AppHelper.showAlertView(message: errorMsg)
                          self.showAlertWithMessage(ConstantStrings.ALERT, message)
                    }
                }

            }else{
                self.showAlertWithMessage("ALERT",  "Something Went Wrong")
                
            }
        }
    }
    
    
    func getExerciseListApi(){
        
        /*
        if !AppHelper.isInterNetConnectionAvailable(){
            showAlertWithMessage(ConstantStrings.ALERT, ConstantStrings.pleaseCheckYourInternetConnection)
            return
        }
        */
        
        WebServiceHandler.performGETRequest(withURL: getUserExerciseList) {(result,error) in
            if result != nil{
                print(result!)
                
                let statusCode = result!["statusCode"]?.int
                if statusCode == 200
                {
                    
                  print(statusCode)
                    if let data = result?["data"]?.dictionary{
                        
                        if let exerciseList = data["allExerciseList"]?.array{
                            self.totalExerciseList = AddedExerciseModel.getAllJSONList(dataArray: exerciseList)
                        }
                        
                        if let exerciseList = data["total"]?.dictionary{
                            self.totalExerciseData = TotalExerciseModel.parseJSONData(details: data["total"]!)
                        }
                        
                       
                        self.itemList.reloadData()
                        print(data)
                    }
                }
                else{
                    if let message = result!["message"]?.string{
                       // AppHelper.showAlertView(message: errorMsg)
                        self.showAlertWithMessage(ConstantStrings.ALERT, message)
                    }
                }

            }else{
                self.showAlertWithMessage("ALERT",  "Something Went Wrong")
                
            }
        }
    }
    
    func fetchSportsDataApi(){
        /*
        if !AppHelper.isInterNetConnectionAvailable(){
            showAlertWithMessage(ConstantStrings.ALERT, ConstantStrings.pleaseCheckYourInternetConnection)
            return
        }
        */
        
        WebServiceHandler.performGETRequest(withURL: fetchSports) {(result,error) in
            if result != nil{
                print(result!)
                
                let statusCode = result!["statusCode"]?.int
                if statusCode == 200
                {
                    
                  print(statusCode)
                    if let data = result?["data"]?.dictionary{
                       
                        self.itemList.reloadData()
                        print(data)
                    }
                }
                else{
                    if let message = result!["message"]?.string{
                       // AppHelper.showAlertView(message: errorMsg)
                        self.showAlertWithMessage(ConstantStrings.ALERT, message)
                    }
                }

            }else{
                self.showAlertWithMessage("ALERT",  "Something Went Wrong")
                
            }
        }
    }
    
    func getDashboardDataApi(){
        
        /*
        if !AppHelper.isInterNetConnectionAvailable(){
            showAlertWithMessage(ConstantStrings.ALERT, ConstantStrings.pleaseCheckYourInternetConnection)
            return
        }
        */
        
        WebServiceHandler.performGETRequest(withURL: fetchWatchDataFromServer) {(result,error) in
            if result != nil{
                print(result!)
                
                let statusCode = result!["statusCode"]?.int
                if statusCode == 200
                {
                    
                    self.dashboardData = DashboardDataModel.parseJSONData(details: result!["data"]!)
                    print(self.dashboardData)
                    UserDetails.sharedInstance.steps = (self.dashboardData?.stepsData!.stepCount)!
                    UserDetails.sharedInstance.stepsCalorie = (self.dashboardData?.stepsData!.caloriesBurned)!
                    UserDetails.sharedInstance.stepsDistance = (self.dashboardData?.stepsData!.distance)!
                    UserDetails.sharedInstance.oxygenData = (self.dashboardData?.oxygenData!.oxygenPercentage)!
                    UserDetails.sharedInstance.sbp = (self.dashboardData?.bpData!.systolicPressure)!
                    UserDetails.sharedInstance.dbp = (self.dashboardData?.bpData!.diastolicPressure)!
                    UserDetails.sharedInstance.heartRateData = (self.dashboardData?.heartData!.beatsPerMinute)!
                    self.watchDataCollectionView.reloadData()
                    self.itemList.reloadData()
                    if let data = result?["data"]?.dictionary{
                       
                        self.itemList.reloadData()
                        print(data)
                    }
                }
                else{
                    if let message = result!["message"]?.string{
                       // AppHelper.showAlertView(message: errorMsg)
                        self.showAlertWithMessage(ConstantStrings.ALERT, message)
                    }
                }

            }else{
                self.showAlertWithMessage("ALERT",  "Something Went Wrong")
                
            }
        }
    }
    
    
    
    
    
    func uploadStepsDataApi(params:[String:String]){
        
        /*
        if !AppHelper.isInterNetConnectionAvailable(){
            showAlertWithMessage(ConstantStrings.ALERT, ConstantStrings.pleaseCheckYourInternetConnection)
            return
        }
        */
      //  ERProgressHud.sharedInstance.showBlurView(withTitle: "Loading...")
        WebServiceHandler.performPOSTRequest(withURL: uploadStepsData, andParameters: params) {(result,error) in
            if result != nil{
                print(result!)
                print("Steps Updated")
                
                let statusCode = result!["statusCode"]?.int
                if statusCode == 200
                {
                   
                    if let data = result?["data"]?.dictionary{
                        print(data)
                       
                        
                    }
                }
                else{
                    if let message = result!["message"]?.string{
                       // AppHelper.showAlertView(message: errorMsg)
                        self.showAlertWithMessage(ConstantStrings.ALERT, message)
                    }
                }
              //  ERProgressHud.sharedInstance.hide()
            }else{
                self.showAlertWithMessage("ALERT",  "Something Went Wrong")
              //  ERProgressHud.sharedInstance.hide()
            }
        }
    }
    
    
    func uploadBPDataaApi(params:[String:String]){
        
        /*
        if !AppHelper.isInterNetConnectionAvailable(){
            showAlertWithMessage(ConstantStrings.ALERT, ConstantStrings.pleaseCheckYourInternetConnection)
            return
        }
        */
      //  ERProgressHud.sharedInstance.showBlurView(withTitle: "Loading...")
        WebServiceHandler.performPOSTRequest(withURL: uploadbpData, andParameters: params) {(result,error) in
            if result != nil{
                print(result!)
                print("BP Updated")
                
                let statusCode = result!["statusCode"]?.int
                if statusCode == 200
                {
                   
                    if let data = result?["data"]?.dictionary{
                        print(data)
                       
                        
                    }
                }
                else{
                    if let message = result!["message"]?.string{
                       // AppHelper.showAlertView(message: errorMsg)
                        self.showAlertWithMessage(ConstantStrings.ALERT, message)
                    }
                }
              //  ERProgressHud.sharedInstance.hide()
            }else{
                self.showAlertWithMessage("ALERT",  "Something Went Wrong")
              //  ERProgressHud.sharedInstance.hide()
            }
        }
    }
    
    func uploadHeartRateDataaApi(params:[String:String]){
        
        /*
        if !AppHelper.isInterNetConnectionAvailable(){
            showAlertWithMessage(ConstantStrings.ALERT, ConstantStrings.pleaseCheckYourInternetConnection)
            return
        }
        */
      //  ERProgressHud.sharedInstance.showBlurView(withTitle: "Loading...")
        WebServiceHandler.performPOSTRequest(withURL: uploadHeartRateData, andParameters: params) {(result,error) in
            if result != nil{
                print("Heart Rate Updated")
                print(result!)
                
                let statusCode = result!["statusCode"]?.int
                if statusCode == 200
                {
                   
                    if let data = result?["data"]?.dictionary{
                        print(data)
                       
                        
                    }
                }
                else{
                    if let message = result!["message"]?.string{
                       // AppHelper.showAlertView(message: errorMsg)
                        self.showAlertWithMessage(ConstantStrings.ALERT, message)
                    }
                }
              //  ERProgressHud.sharedInstance.hide()
            }else{
                self.showAlertWithMessage("ALERT",  "Something Went Wrong")
              //  ERProgressHud.sharedInstance.hide()
            }
        }
    }
    
    
    func uploadOxygenDataApi(params:[String:String]){
        
        /*
        if !AppHelper.isInterNetConnectionAvailable(){
            showAlertWithMessage(ConstantStrings.ALERT, ConstantStrings.pleaseCheckYourInternetConnection)
            return
        }
        */
      //  ERProgressHud.sharedInstance.showBlurView(withTitle: "Loading...")
        WebServiceHandler.performPOSTRequest(withURL: uploadOxygenData, andParameters: params) {(result,error) in
            if result != nil{
                print(result!)
                print("Oxygen Updated")
                
                let statusCode = result!["statusCode"]?.int
                if statusCode == 200
                {
                   
                    if let data = result?["data"]?.dictionary{
                        print(data)
                       
                        
                    }
                }
                else{
                    if let message = result!["message"]?.string{
                       // AppHelper.showAlertView(message: errorMsg)
                        self.showAlertWithMessage(ConstantStrings.ALERT, message)
                    }
                }
              //  ERProgressHud.sharedInstance.hide()
            }else{
                self.showAlertWithMessage("ALERT",  "Something Went Wrong")
              //  ERProgressHud.sharedInstance.hide()
            }
        }
    }
    
    
}
extension DashboardViewController:ExerciseTableViewCellDelegate{
    func addExerciseTapped() {
        let controller = AddExerciseViewController.instantiateFromStoryBoard()
        self.push(controller)
    }
    
    
    
}
