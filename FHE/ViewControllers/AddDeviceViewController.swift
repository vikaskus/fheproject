//
//  AddDeviceViewController.swift
//  FHE
//
//  Created by Macintosh on 23/10/21.
//  Copyright © 2021 sylar. All rights reserved.
//

import UIKit
import CRPSmartBand
import CoreBluetooth
import ExternalAccessory




class AddDeviceViewController: BaseViewControllerClass {
    
    
    
    static var viewControllerId = "AddDeviceViewController"
    static var storyBoard = "Main"
    var startScanTapped = false
    
    
    //CRPSDK
    var myStepHandler:stepHandler!
    var discoverys = [CRPDiscovery]()
    var myDiscovery:CRPDiscovery!
    var mac = "";
    var managerBLE: CBCentralManager?

    @IBOutlet weak var btnStartStop: UIButton!
    
    @IBOutlet weak var itemList: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setInitials()
        setInitialsWatchData()
    }
    
    
    func setInitials(){
        itemList.delegate = self
        itemList.dataSource = self
        itemList.register(UINib(nibName: "DeviceTableViewCell", bundle: nil), forCellReuseIdentifier: "DeviceTableViewCell")
    }
    

    @IBAction func btnStartStopScan(_ sender: Any) {
        bluetoothStatus()
        if startScanTapped == false{
            self.discoverys.removeAll()
            startScanTapped = true
            btnStartStop.setTitle("STOP SCAN", for: .normal)
            startScanCRP()
            startScanCRP()
        }else{
            CRPSmartBandSDK.sharedInstance.interruptScan()
            
            btnStartStop.setTitle("START SCAN", for: .normal)
            startScanTapped = false
            
        }
        
    }
    
    
    @IBAction func btnCancelAction(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
}
extension AddDeviceViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return discoverys.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = itemList.dequeueReusableCell(withIdentifier: "DeviceTableViewCell") as? DeviceTableViewCell
        cell?.selectionStyle = .none
        let device = discoverys[indexPath.row]
        cell?.lblDeviceTitle.text = device.remotePeripheral.name
        return cell!
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     //   CEProductK6.shareInstance().connect(deviceArray[indexPath.row].peripheral)
        self.myDiscovery = self.discoverys[indexPath.row]
        CRPSmartBandSDK.sharedInstance.connet(self.myDiscovery)
        
      //  coordinatorDelegate?.navigateToFlutter(data:AppHelper.sharedInstance.watchData)
      //  self.navigationController?.popViewController(animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
       
    }
}
extension AddDeviceViewController:CRPManagerDelegate,CBCentralManagerDelegate {
    
    

    func bluetoothStatus() {
        managerBLE = CBCentralManager(delegate: self, queue: nil, options: nil)
    }

    

    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch central.state {
        case .poweredOn:
            break
        case .poweredOff:
            print("Bluetooth is Off.")
            self.showAlertWithMessage("ALERT", "Please turn On your Bluetooth")
            startScanTapped = false
            btnStartStop.setTitle("START SCAN", for: .normal)
            
            break
        case .resetting:
            break
        case .unauthorized:
            break
        case .unsupported:
            break
        case .unknown:
            break
        default:
            break
        }
    }
    
    
    func startScanCRP(){
       let devices =  EAAccessoryManager.shared().connectedAccessories
        print(devices)
    
        
        CRPSmartBandSDK.sharedInstance.scan(10, progressHandler: { (newDiscoverys) in
           // let p = newDiscoverys[0]
            print(newDiscoverys[0].kCABAdvidataLocalName)
            print(newDiscoverys[0].mac)
            if let deviceName = newDiscoverys[0].kCABAdvidataLocalName as? String{
                self.discoverys.append(newDiscoverys[0])
            }
            print(newDiscoverys[0])
           
            if self.discoverys.count > 0{
                self.itemList.reloadData()
            }
            /*
            print(p)
          //  if (mac == "\(p.mac)") {
              
            self.mac = p.mac!
                    self.myDiscovery = p
            let alert = UIAlertController(title: "", message: "Scanned to:\(self.mac)，please Bind", preferredStyle: UIAlertController.Style.alert)
            let cancel = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil)
                    alert.addAction(cancel)
                    self.present(alert, animated: true, completion: nil)
            */
                
        }){ (newDiscoverys, err) in
            print("error = \(err)")
            print("ok")
        }
    }
    
    func getStep(_ handler: @escaping stepHandler) {
        myStepHandler = handler
    }
    
    
    func didState(_ state: CRPState) {
        print("Connect state: \(state.rawValue)")
        if state == .connected{
            UserDetails.sharedInstance.connectedDevice = CRPSmartBandSDK.sharedInstance.currentCRPDiscovery
            UserDetails.sharedInstance.isDeviceConnected = true
           
            let alert = UIAlertController(title: "", message: "Connected to \(UserDetails.sharedInstance.connectedDevice.remotePeripheral.name!)", preferredStyle: UIAlertController.Style.alert)
            let cancel = UIAlertAction(title: "OK", style: UIAlertAction.Style.default,handler: {
            action in
                self.dismiss(animated: true, completion: nil)
               
            })
            
                    alert.addAction(cancel)
                    self.present(alert, animated: true, completion: nil)
            self.getData()
            CRPSmartBandSDK.sharedInstance.checkDFUState { (dfu, err) in
                print("dfu =\(dfu)")
            }
        }
    }
    
    func didBluetoothState(_ state: CRPBluetoothState) {
        print(state)
    }
    
    func receiveSteps(_ model: StepModel) {
        print(model)
    
        UserDetails.sharedInstance.steps = "\(model.steps)"
        UserDetails.sharedInstance.stepsTime = "\(model.time)"
        UserDetails.sharedInstance.stepsCalorie = "\(model.calory)"
        UserDetails.sharedInstance.stepsDistance = "\(model.distance)"
        
        let param = [ "currentStatus": "0",
                      "stepCount": UserDetails.sharedInstance.steps,
                      "caloriesBurned": UserDetails.sharedInstance.stepsCalorie,
                      "distance": UserDetails.sharedInstance.stepsDistance,
                      "duration": UserDetails.sharedInstance.stepsTime]
        
        uploadStepsDataApi(params: param)
        NotificationCenter.default.post(name: .postNotification, object: "myObject", userInfo: ["dataType": "steps","walkSteps":"\(model.steps)","walkDuration":"\(model.time)","walkDistance":"\(model.distance)","walkCalories":"\(model.calory)"])
       
        
        let manager = CRPSmartBandSDK.sharedInstance
        
       // manager.setDial(1)
        manager.getSportData({ (model, error) in
            var sportsData = [SportsModel]()
            
            for i in model{
                var sportsString = ""
                switch(i.type.rawValue){
                case  0:
                sportsString = "walking"

                case 1:
                sportsString = "run"

                case 2:
                sportsString = "cycling"

                case  3:
                sportsString = "skipping"

                case 4:
                sportsString = "badminton"

                case  5:
                sportsString = "basketball"

                case 6:
                sportsString = "football"

                case  7:
                sportsString = "swimming"

                case  8:
                sportsString = "hiking"

                case 9:
                sportsString = "tennis"

                case 10:
                sportsString = "rugby"

                case 11:
                sportsString = "golf"

                case 12:
                sportsString = "yoga"

                case 13:
                sportsString = "workout"

                case 14:
                sportsString = "dance"

                case 15:
                sportsString = "baseball"

                case  16:
                sportsString = "elliptical"

                case 17:
                sportsString = "indoorCycling"

                case  18:
                sportsString = "training"

                case  19:
                sportsString = "rowingMachine"

                case  20:
                sportsString = "trailRun"

                case  21:
                sportsString = "ski"

                case  22:
                sportsString = "bowling"

                case  23:
                sportsString = "dumbbells"

                case  24:
                sportsString = "sit_ups"

                case  25:
                sportsString = "onfoot"

                case  26:
                sportsString = "indoorWalk"

                case  27:
                sportsString = "indoorRun"
                default:
                    sportsString = ""
                }
                
                
                sportsData.append(SportsModel(date: "\(i.date)", startTime: "\(i.startTime)", endTime: "\(i.endTime)", vaildTime: "\(i.vaildTime)", type: "\(sportsString)", step: "\(i.step)", distance: "\(i.distance)", kcal: "\(i.kcal)"))
            }
            AppHelper.sharedInstance.sportsData = sportsData
            
            NotificationCenter.default.post(name: .postNotification, object: "myObject", userInfo: ["dataType": "sports"])
           
            print(model)
        })
        
        manager.getSleepData({ (model, error) in
            print(model)
            var sleepModel = [SleepModel]()
            /*
            for i in model.detail{
                sleepModel.append(SleepModel(total: i.t, start: , end: , type: ))
            }
            */
            
            let sleepArray = model.detail
            for i in model.detail{
                let sleepData = i as? NSDictionary
                sleepModel.append(SleepModel(total: i["total"] as! String, start: i["start"] as! String, end: i["end"] as! String, type: i["type"] as! String))
            }
            AppHelper.sharedInstance.sleepData = sleepModel
            
            print("Deep sleep\(model.deep)Minute Light sleep\(model.light)Minute")
            NotificationCenter.default.post(name: .postNotification, object: "myObject", userInfo: ["dataType": "sleep","Light":"\(model.light)","Deep":"\(model.deep)"/*,"drillDown":sleepModel*/])
        })

        
    }
    
    func receiveHeartRate(_ heartRate: Int) {
        print(heartRate)
        UserDetails.sharedInstance.heartRateData = "\(heartRate)"
        NotificationCenter.default.post(name: .postNotification, object: "myObject", userInfo: ["dataType": "heartRate","heartRate":"\(heartRate)"])
    }
    
    func receiveHeartRateAll(_ model: HeartModel) {
        print(model)
        
    }
    
    func receiveBloodPressure(_ heartRate: Int, _ sbp: Int, _ dbp: Int) {
        print(heartRate)
        UserDetails.sharedInstance.sbp = "\(sbp)"
        UserDetails.sharedInstance.dbp = "\(dbp)"
        NotificationCenter.default.post(name: .postNotification, object: "myObject", userInfo: ["dataType": "bloodPressure","heartRate":"\(heartRate)","sbp":"\(sbp)","dbp":"\(dbp)"])
    }
    
    func receiveSpO2(_ o2: Int) {
        UserDetails.sharedInstance.oxygenData = "\(o2)"
        print(o2)
        NotificationCenter.default.post(name: .postNotification, object: "myObject", userInfo: ["dataType": "o2","o2":"\(o2)"])
        
    }
    
    
    
    

    func setInitialsWatchData(){
        CRPSmartBandSDK.sharedInstance.delegate = self
        
        self.getStep { (step,error) in
            print(step)
        }
        
    }
    
    
    func recevieTakePhoto() {
        print("recevieTakePhoto")
    }
    
    
    func receiveUpgradeScreen(_ state: CRPUpgradeState, _ progress: Int) {
        print("state = \(state.description()), progress = \(progress)")
        
    }
    
    func receiveRealTimeHeartRate(_ heartRate: Int, _ rri: Int) {
        print("heart rate is \(heartRate)")
        
    }
    func receiveUpgrede(_ state: CRPUpgradeState, _ progress: Int) {
        print("state = \(state.description()), progress = \(progress)")
    }
    func recevieWeather() {
        print("recevieWeather")
    }
    
    
    func getData(){
        let manager = CRPSmartBandSDK.sharedInstance
            manager.getSteps({ (model, error) in
                print(model)
//                let cal
//                let text = "Step:\(model)"
                print("\(model.steps)step \(model.calory)kcal \(model.distance)m , \(model.time)s")
            })
      
            manager.getSleepData({ (model, error) in
                print(model)
                var sleepModel = [SleepModel]()
                /*
                for i in model.detail{
                    sleepModel.append(SleepModel(total: i.t, start: , end: , type: ))
                }
                */
                
                let sleepArray = model.detail
                for i in model.detail{
                    let sleepData = i as? NSDictionary
                    sleepModel.append(SleepModel(total: i["total"] as! String, start: i["start"] as! String, end: i["end"] as! String, type: i["type"] as! String))
                }
                AppHelper.sharedInstance.sleepData = sleepModel
                
                print("Deep sleep\(model.deep)Minute Light sleep\(model.light)Minute")
                NotificationCenter.default.post(name: .postNotification, object: "myObject", userInfo: ["dataType": "sleep","Light":"\(model.light)","Deep":"\(model.deep)"/*,"drillDown":sleepModel*/])
            })
    
            manager.getFeatures({ (features, error) in
                print("Support：\(features)")
            })
        
//            if let zippath = Bundle.main.path(forResource: "MOY-SFP4-1.7.9", ofType: "zip"){
//                manager.getOTAMac { (otaMac, error) in
//                    manager.startOTAFromFile(mac: otaMac, zipFilePath: zippath, isUser: false,false)
//                }
//            }
       
            manager.getSoftver({ (ver, error) in
                print(error)
              //  (self.view.viewWithTag(201) as! UILabel).text = ver
                print("varsion：\(ver)")
            })
      
            manager.getBattery({ (battery, error) in
              //  (self.view.viewWithTag(202) as! UILabel).text = String(battery)
                print("Battery：\(battery)")
            })
       
            manager.getGoal({ (value, error) in
              //  (self.view.viewWithTag(203) as! UILabel).text = String(value)
                print("Goal：\(value)")
            })
        
            manager.getDominantHand({ (value, error) in
               // (self.view.viewWithTag(204) as! UILabel).text = String(value)
                print("Hand：\(value)")
            })
       
            manager.getAlarms({ (alarms, error) in
                print(alarms)
            })
     
            manager.getProfile({ (profile, error) in
              print("\(profile)")
            })
       
            manager.getLanguage { (value, CRPErrorerror) in
                print("\(value)")
            } _: { (indexs, error) in
                print("index = \(indexs)")
            }

       
            manager.getDial({ (value, error) in
                print("\(value)")
            })
       
            manager.getRemindersToMove({ (value, error) in
                print("\(value)")
            })
       
            manager.getQuickView({ (value, error) in
                print("\(value)")
            })

            manager.getUnit({ (value, error) in
                print("\(value)")
            })
      
            manager.getTimeformat({ (value, error) in
                print("\(value)")
            })
     
            manager.getMac({ (value, error) in
                print("\(value)")
            })
        
            manager.getNotifications({ (value, error) in
                print(value)
            })
        
            manager.getNotifications({ (value, error) in
                print(value)
                print("\(value.description)")
            })
           
  
          //  manager.setStartSpO2()
          
      
         //   manager.setStopSpO2()
//            manager.getHeartData()
            manager.getMac { (mac, err) in
                manager.getSoftver { (ver, err) in
                    manager.checkLatest(mac, ver) { (newInfo, tpNewInfo, err) in
                        print("new = \(newInfo), tpNewInfo = \(tpNewInfo)")
                    }
                }
            }
           
        
    }
        
}
extension Notification.Name {
    static let postNotification = Notification.Name("postNotification")
}

struct SleepModel {
    var total = ""
    var start = ""
    var end = ""
    var type = ""
}

struct SportsModel{
    
    var date: String

    var startTime: String

    var endTime: String

    var vaildTime: String

    var type: String

    var step: String

    var distance: String

    var kcal: String
}
extension AddDeviceViewController{
    
    func uploadStepsDataApi(params:[String:String]){
        
        /*
        if !AppHelper.isInterNetConnectionAvailable(){
            showAlertWithMessage(ConstantStrings.ALERT, ConstantStrings.pleaseCheckYourInternetConnection)
            return
        }
        */
      //  ERProgressHud.sharedInstance.showBlurView(withTitle: "Loading...")
        WebServiceHandler.performPOSTRequest(withURL: uploadStepsData, andParameters: params) {(result,error) in
            if result != nil{
                print(result!)
                
                let statusCode = result!["statusCode"]?.int
                if statusCode == 200
                {
                   
                    if let data = result?["data"]?.dictionary{
                        print(data)
                       
                        
                    }
                }
                else{
                    if let message = result!["message"]?.string{
                       // AppHelper.showAlertView(message: errorMsg)
                        self.showAlertWithMessage(ConstantStrings.ALERT, message)
                    }
                }
              //  ERProgressHud.sharedInstance.hide()
            }else{
                self.showAlertWithMessage("ALERT",  "Something Went Wrong")
              //  ERProgressHud.sharedInstance.hide()
            }
        }
    }
    
    
}
