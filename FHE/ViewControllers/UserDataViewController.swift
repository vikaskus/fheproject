//
//  UserDataViewController.swift
//  FHE
//
//  Created by Macintosh on 22/10/21.
//  Copyright © 2021 sylar. All rights reserved.
//

import UIKit

class UserDataViewController: BaseViewControllerClass {
    static var viewControllerId = "UserDataViewController"
    static var storyBoard = "Main"
    
    
    
    @IBOutlet weak var questionTitle: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var pickerTitle: UILabel!
    @IBOutlet weak var customPickerView: UIPickerView!
    @IBOutlet weak var pickerContainerView: UIView!
    let feetArray = ["1","2","3","4","5","6","7","8"]
    let inchArray = ["0","1","2","3","4","5","6","7","8","9","10","11"]
    var cmArray = [String]()
    let weightDecimal = ["0","1","2","3","4","5","6","7","8","9"]
    
    @IBOutlet weak var btnFloating: UIButton!
    
    var testDataList = [TestDataModel]()
    var dataCompleted = false;
    
    
    var isHeight = false
    var inCm = false
    var inKg = false
    
    var firstName = ""
    var lastName = ""
    var gender = ""
    var height = ""
    var heightUnit = ""
    var weight = ""
    var weightUnit = ""
    var foodCategory = ""
    var goal = ""
    var selectedActivityLevel = ""
    
    var weightTitle = "0 Kg"
    var heightTitle = "0 Cm"
    
    
    var feet = "0"
    var inch = "0"
    
    var mainKG = "30"
    var kgDecimalValue = "0"
    
    var mainLbs = "30"
    var LbsDecimalValue = "0"
    
    
    var selectedIndex = 0;
    
    
   
    var questionArray = [
        "WE SHALL CALL YOU?",
        "CHOOSE YOUR GENDER",
        "WHAT'S YOUR HEIGHT?",
        "WHAT'S YOUR WEIGHT?",
        "PREFERRED FOOD CATEGORY?",
        "WHAT'S YOUR GOAL?",
        "CURRENT ACTIVITY LEVEL?"
      ]

    @IBOutlet weak var dataCollectionView: UICollectionView!
    @IBOutlet weak var progressViewWidthConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var screenWidth = UIScreen.main.bounds.width
         updateProgressBar()
        setInitials()
       // callGetProfileDataApi()
        // Do any additional setup after loading the view.
    }
    
    func updateProgressBar(){
        progressViewWidthConstraint.constant = (screenWidth - 50)/7 * CGFloat((selectedIndex + 1))
        
    }
    
    
    func setInitials(){
        
        for i in 30...300{
            cmArray.append("\(i)")
        }
        pickerContainerView.isHidden = true
        dataCollectionView.register(UINib(nibName: "UserNameCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "UserNameCollectionViewCell")
        dataCollectionView.register(UINib(nibName: "GenderCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "GenderCollectionViewCell")
        dataCollectionView.register(UINib(nibName: "UserHeightCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "UserHeightCollectionViewCell")
        dataCollectionView.register(UINib(nibName: "UserWeightCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "UserWeightCollectionViewCell")
        dataCollectionView.register(UINib(nibName: "FoodCategoryCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "FoodCategoryCollectionViewCell")
        dataCollectionView.register(UINib(nibName: "GoalCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "GoalCollectionViewCell")
        dataCollectionView.register(UINib(nibName: "CurrentActivityLevelCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CurrentActivityLevelCollectionViewCell")
        
        
        customPickerView.delegate = self
        customPickerView.dataSource = self
        
        
        dataCollectionView.delegate = self
        dataCollectionView.dataSource = self
        updateQuestions()
    }
    
    @IBAction func btnCancelAction(_ sender: Any) {
        pickerContainerView.isHidden = true
    }
    
    @IBAction func btnOkAction(_ sender: Any) {
        pickerContainerView.isHidden = true
    }
    @IBAction func btnFloatingAction(_ sender: Any) {
        
        
        if selectedIndex == 0{
            let index = IndexPath(row: selectedIndex, section: 0)
            let cell: UserNameCollectionViewCell = (self.dataCollectionView.cellForItem(at: index) as? UserNameCollectionViewCell)!
                firstName = cell.txtFieldFirstName.text!
                self.lastName = cell.txtFieldLastName.text!
            if firstName == "" {
                self.showAlertWithMessage("ALERT", "Please select First Name")
                return
            }else if lastName == ""{
                self.showAlertWithMessage("ALERT", "Please select Last Name")
                return
                
            }
            print(firstName)
            print(lastName)
            dataCompleted = false
              
        }else if selectedIndex == 1{
            let index = IndexPath(row: selectedIndex, section: 0)
            let cell: GenderCollectionViewCell = (self.dataCollectionView.cellForItem(at: index) as? GenderCollectionViewCell)!
                gender = cell.gender
            print(gender)
            dataCompleted = false
            
        }else if selectedIndex == 2{
            if heightTitle == "0 Cm"{
                self.showAlertWithMessage("ALERT", "Please select Height")
                return
            }
            
                
            
        }else if selectedIndex == 3{
            if weightTitle == "0 Kg"{
                self.showAlertWithMessage("ALERT", "Please select Weight")
                return
                
            }
            
                
            
        }else if selectedIndex == 4{
            let index = IndexPath(row: selectedIndex, section: 0)
            let cell: FoodCategoryCollectionViewCell = (self.dataCollectionView.cellForItem(at: index) as? FoodCategoryCollectionViewCell)!
                foodCategory = cell.foodCategory
            print(foodCategory)
            dataCompleted = false
                
        }else if selectedIndex == 5{
            let index = IndexPath(row: selectedIndex, section: 0)
            let cell: GoalCollectionViewCell = (self.dataCollectionView.cellForItem(at: index) as? GoalCollectionViewCell)!
                goal = cell.goal
            print(goal)
            dataCompleted = false
                
        }else if selectedIndex == 6{
            let index = IndexPath(row: selectedIndex, section: 0)
            let cell: CurrentActivityLevelCollectionViewCell = (self.dataCollectionView.cellForItem(at: index) as? CurrentActivityLevelCollectionViewCell)!
                selectedActivityLevel = cell.currentActivityLevel
                print(selectedActivityLevel)
            dataCompleted = true
            if dataCompleted{
                let params = [ "firstName":firstName,
                               "lastName":lastName,
                               "gender":gender,
                               "height":height,
                               "heightMeasureIn":heightUnit,
                               "weight":weight,
                               "weightMeasureIn":weightUnit,
                               "preferedFoodCategory":foodCategory,
                               "goals":goal,
                               "currentActivityLevel":selectedActivityLevel]
                uploadUserDataApi(params: params)
                
            }
                
        }else if dataCompleted{
            let params = [ "firstName":firstName,
                           "lastName":lastName,
                           "gender":gender,
                           "height":height,
                           "heightMeasureIn":heightUnit,
                           "weight":weight,
                           "weightMeasureIn":weightUnit,
                           "preferedFoodCategory":foodCategory,
                           "goals":goal,
                           "currentActivityLevel":selectedActivityLevel]
            uploadUserDataApi(params: params)
        }
        dataCollectionView.isPagingEnabled = false
        
        let visibleItems: NSArray = self.dataCollectionView.indexPathsForVisibleItems as NSArray
           let currentItem: IndexPath = visibleItems.object(at: 0) as! IndexPath
           let nextItem: IndexPath = IndexPath(item: currentItem.item + 1, section: 0)
                  if nextItem.row < questionArray.count {
                    
               self.dataCollectionView.scrollToItem(at: nextItem, at: .left, animated: true)
                    self.selectedIndex = nextItem.row
                    updateQuestions()
                    updateProgressBar()
                    print(self.selectedIndex)
           }
        
        dataCollectionView.isPagingEnabled = true
        dataCollectionView.reloadData()
        
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        dataCollectionView.isPagingEnabled = false
        
        let visibleItems: NSArray = self.dataCollectionView.indexPathsForVisibleItems as NSArray
           let currentItem: IndexPath = visibleItems.object(at: 0) as! IndexPath
           let nextItem: IndexPath = IndexPath(item: currentItem.item - 1, section: 0)
           if nextItem.row < questionArray.count && nextItem.row >= 0{
               self.dataCollectionView.scrollToItem(at: nextItem, at: .right, animated: true)
            self.selectedIndex = nextItem.row
            updateQuestions()
            updateProgressBar()
            print(self.selectedIndex)

           }
        
        dataCollectionView.isPagingEnabled = true
        dataCollectionView.reloadData()
        
    }
    
    func updateQuestions(){
        self.questionTitle.text = questionArray[selectedIndex]
        
    }
    
}
extension UserDataViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 7
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        

        if (indexPath.row == 0){
            let cell = dataCollectionView.dequeueReusableCell(withReuseIdentifier: "UserNameCollectionViewCell", for: indexPath)
            return cell
        }else if (indexPath.row == 1){
            let cell = dataCollectionView.dequeueReusableCell(withReuseIdentifier: "GenderCollectionViewCell", for: indexPath)
            return cell
        }else if (indexPath.row == 2){
            let cell = dataCollectionView.dequeueReusableCell(withReuseIdentifier: "UserHeightCollectionViewCell", for: indexPath) as? UserHeightCollectionViewCell
            cell?.lblHeightTitle.text = heightTitle
            cell?.delegate = self
            return cell!
        }else if (indexPath.row == 3){
            let cell = dataCollectionView.dequeueReusableCell(withReuseIdentifier: "UserWeightCollectionViewCell", for: indexPath) as? UserWeightCollectionViewCell
            cell?.lblWeightTitle.text = weightTitle
            cell?.delegate = self
            return cell!
        }else if (indexPath.row == 4){
            let cell = dataCollectionView.dequeueReusableCell(withReuseIdentifier: "FoodCategoryCollectionViewCell", for: indexPath)
            return cell
        }else if (indexPath.row == 5){
            let cell = dataCollectionView.dequeueReusableCell(withReuseIdentifier: "GoalCollectionViewCell", for: indexPath)
            return cell
        }else{
            let cell = dataCollectionView.dequeueReusableCell(withReuseIdentifier: "CurrentActivityLevelCollectionViewCell", for: indexPath)
            return cell
        }
        
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: dataCollectionView.frame.size.width, height: dataCollectionView.frame.size.height)
    }
    
    
}
extension UserDataViewController:UserHeightCollectionViewCellDelegate,UserWeightCollectionViewCellDelegate{
    
    
    func btnKgTapped() {
        isHeight = false
        inKg = true
        customPickerView.reloadAllComponents()
        pickerContainerView.isHidden = false
        pickerTitle.text = "Select Weight"
    }
    
    func btnLbsTapped() {
        isHeight = false
        inKg = false
        customPickerView.reloadAllComponents()
        pickerContainerView.isHidden = false
        pickerTitle.text = "Select Weight"
    }
    
   
    
    
    func btnFeetTapped() {
        isHeight = true
        inCm = false
        customPickerView.reloadAllComponents()
        pickerContainerView.isHidden = false
        pickerTitle.text = "Select Height"
      
    }
    
    func btnCmsTapped() {
        isHeight = true
        inCm = true
        customPickerView.reloadAllComponents()
        pickerContainerView.isHidden = false
        pickerTitle.text = "Select Height"
        
    }
    
    
}
extension UserDataViewController:UIPickerViewDelegate,UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        if isHeight{
            if inCm{
                return 1
            }else{
                return 2
            }
        }else{
                return 2
        }
    }
    
   
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if isHeight{
            if inCm{
                return cmArray.count
            }else{
                if component == 0{
                    return feetArray.count
                }else{
                    return inchArray.count
                }
               
            }
        }else{
            if component == 0{
                return cmArray.count
            }else{
                return weightDecimal.count
            }
                
        }
       
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if isHeight{
            if inCm{
                return cmArray[row]
            }else{
                if component == 0{
                    return feetArray[row]
                }else{
                    return inchArray[row]
                }
            }
        }else{
            if inKg{
                if component == 0{
                    return cmArray[row]
                }else{
                    return weightDecimal[row]
                }
                
            }else{
                if component == 0{
                    return cmArray[row]
                }else{
                    return weightDecimal[row]
                }
                
            }
        }
        
    }
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if isHeight{
            if inCm{
                height = cmArray[row]
                heightTitle = height + " Cm"
                heightUnit = "cms"
                
                print(heightTitle)
                print(heightUnit)
                
            }else{
                if component == 0{
                    feet = feetArray[row]
                }else{
                    inch = inchArray[row]
                }
                heightTitle = feet + " Feet " + inch + " Inch"
                heightUnit = "feet"
                height = feet + "." + inch
                print(height)
                print(heightTitle)
                print(heightUnit)
            }
        }else{
            if inKg{
                if component == 0{
                    mainKG = cmArray[row]
                }else{
                    kgDecimalValue = weightDecimal[row]
                }
                weight = mainKG + "." + kgDecimalValue
                weightTitle = weight + " Kg"
                weightUnit = "kgs"
                print(weightTitle)
                print(weightUnit)
                
            }else{
                if component == 0{
                    mainKG  = cmArray[row]
                }else{
                    kgDecimalValue = weightDecimal[row]
                }
                weight = mainKG + "." + kgDecimalValue
                weightTitle = weight + " Lbs"
                weightUnit = "lbs"
                print(weightTitle)
                print(weightUnit)
                
            }
        }
        dataCollectionView.reloadData()
    }
    
}
extension UserDataViewController{
    
    
    func uploadUserDataApi(params:[String:String]){
        
        /*
        if !AppHelper.isInterNetConnectionAvailable(){
            showAlertWithMessage(ConstantStrings.ALERT, ConstantStrings.pleaseCheckYourInternetConnection)
            return
        }
        */
      //  ERProgressHud.sharedInstance.showBlurView(withTitle: "Loading...")
        WebServiceHandler.performPOSTRequest(withURL: uploadUserData, andParameters: params) {(result,error) in
            if result != nil{
                print(result!)
                
                let statusCode = result!["statusCode"]?.int
                if statusCode == 200
                {
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                   let controller = storyboard.instantiateViewController(withIdentifier: "TabbarController")
                    
                  //  self.tabBarController?.selectedIndex = 1
                    self.navigationController?.pushViewController(controller, animated: true)
                     
                    print(statusCode)
                    
                }
                else{
                    if let message = result!["message"]?.string{
                       // AppHelper.showAlertView(message: errorMsg)
                        self.showAlertWithMessage(ConstantStrings.ALERT, message)
                    }
                }
              //  ERProgressHud.sharedInstance.hide()
            }else{
                self.showAlertWithMessage("ALERT",  "Something Went Wrong")
              //  ERProgressHud.sharedInstance.hide()
            }
        }
    }
    
   
}
