//
//  EditProfileViewController.swift
//  FHE
//
//  Created by Macintosh on 29/10/21.
//  Copyright © 2021 sylar. All rights reserved.
//

import UIKit

class EditProfileViewController: BaseViewControllerClass {

    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var txtFieldFirstName: UITextField!
    @IBOutlet weak var txtFieldLastName: UITextField!
    @IBOutlet weak var txtFieldEmail: UITextField!
    @IBOutlet weak var txtFieldMobile: UITextField!
    @IBOutlet weak var txtFieldGender: UITextField!
    @IBOutlet weak var txtFieldHeight: UITextField!
    @IBOutlet weak var txtFieldWeight: UITextField!
    
    static var viewControllerId = "EditProfileViewController"
    static var storyBoard = "Main"
    var heightUnit = ""
    var weightUnit = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtFieldMobile.isEnabled = false

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setData()
    }
    
    func setData(){
        heightUnit = UserDetails.sharedInstance.heightMeaseureIn
        weightUnit = UserDetails.sharedInstance.weightMeasureIn
        
        txtFieldFirstName.text = UserDetails.sharedInstance.firstName
        txtFieldLastName.text = UserDetails.sharedInstance.lastName
        txtFieldMobile.text = UserDetails.sharedInstance.phone
        
        txtFieldEmail.text = UserDetails.sharedInstance.email
        txtFieldGender.text = UserDetails.sharedInstance.gender
        txtFieldHeight.text = UserDetails.sharedInstance.height + " " + UserDetails.sharedInstance.heightMeaseureIn
        txtFieldWeight.text =  UserDetails.sharedInstance.weight + " " + UserDetails.sharedInstance.weightMeasureIn
                
    }
    
    @IBAction func btnSaveAction(_ sender: Any) {
        let params = [ "firstName":txtFieldFirstName.text!,
                       "lastName":txtFieldLastName.text!,
                       "gender":txtFieldGender.text!,
                       "height":txtFieldHeight.text!,
                       "heightMeasureIn":heightUnit,
                       "weight":txtFieldWeight.text!,
                       "weightMeasureIn":weightUnit
                       ]
        updateUserProfileApi(params: params)
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
extension EditProfileViewController{
    
    func updateUserProfileApi(params:[String:String]){
        
        /*
        if !AppHelper.isInterNetConnectionAvailable(){
            showAlertWithMessage(ConstantStrings.ALERT, ConstantStrings.pleaseCheckYourInternetConnection)
            return
        }
        */
      //  ERProgressHud.sharedInstance.showBlurView(withTitle: "Loading...")
        WebServiceHandler.performPOSTRequest(withURL: uploadUserData, andParameters: params) {(result,error) in
            if result != nil{
                print(result!)
                
                let statusCode = result!["statusCode"]?.int
                if statusCode == 200
                {
                    
                    UserDetails.sharedInstance.heightMeaseureIn = self.heightUnit
                    UserDetails.sharedInstance.weightMeasureIn = self.weightUnit
                    
                    UserDetails.sharedInstance.firstName = self.txtFieldFirstName.text!
                    UserDetails.sharedInstance.lastName = self.txtFieldLastName.text!
                    
                    
                    UserDetails.sharedInstance.email = self.txtFieldEmail.text!
                    UserDetails.sharedInstance.gender = self.txtFieldGender.text!
                    UserDetails.sharedInstance.height = self.txtFieldHeight.text!
                    UserDetails.sharedInstance.weight = self.txtFieldWeight.text!
                   
                    
                  //  self.tabBarController?.selectedIndex = 1
                    self.navigationController?.popViewController(animated: true)
                     
                    print(statusCode)
                    
                }
                else{
                    if let message = result!["message"]?.string{
                       // AppHelper.showAlertView(message: errorMsg)
                        self.showAlertWithMessage(ConstantStrings.ALERT, message)
                    }
                }
              //  ERProgressHud.sharedInstance.hide()
            }else{
                self.showAlertWithMessage("ALERT",  "Something Went Wrong")
              //  ERProgressHud.sharedInstance.hide()
            }
        }
    }
}
