//
//  AddReminderViewController.swift
//  FHE
//
//  Created by Macintosh on 29/10/21.
//  Copyright © 2021 sylar. All rights reserved.
//

import UIKit

class AddReminderViewController: BaseViewControllerClass {
    
    static var viewControllerId = "AddReminderViewController"
    static var storyBoard = "Main"
    
    
    @IBOutlet weak var txtViewReminder: UITextView!
    @IBOutlet weak var datePickerView: UIDatePicker!
    
    @IBOutlet weak var lblReminderType: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    var selectedDate = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblDate.text = ""

        // Do any additional setup after loading the view.
    }
    

    
    
    @IBAction func btnReminderTypeAction(_ sender: Any) {
        let controller = ReminderTypeViewController.instantiateFromStoryBoard()
        controller.delegate = self
        self.push(controller)
    }
    
    @IBAction func btnSaveReminderAction(_ sender: Any) {
        getDate()
        let params = [ "interval": lblReminderType.text!,
                       "remainderBody": txtViewReminder.text!,
                       "atThisTime": selectedDate]
        addReminderApi(params: params)
        
    }
    
    func getDate(){
        let formatter = DateFormatter()
          formatter.dateFormat = "dd-MM-yyyy hh:mm"
        selectedDate = formatter.string(from: datePickerView.date)
        print(selectedDate)
          self.view.endEditing(true)
    }
    
    @IBAction func btnCancelAction(_ sender: Any) {
        
    }
    @IBAction func btnBackAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
}
extension AddReminderViewController:ReminderTypeViewControllerDelegate{
    func typeSelect(type: String) {
        lblReminderType.text = type
    }
    
    
}
extension AddReminderViewController{
    
    
    
}
extension AddReminderViewController{
    
    func addReminderApi(params:[String:String]){
        
        /*
        if !AppHelper.isInterNetConnectionAvailable(){
            showAlertWithMessage(ConstantStrings.ALERT, ConstantStrings.pleaseCheckYourInternetConnection)
            return
        }
        */
      //  ERProgressHud.sharedInstance.showBlurView(withTitle: "Loading...")
        WebServiceHandler.performPOSTRequest(withURL: addReminder, andParameters: params) {(result,error) in
            if result != nil{
                print(result!)
                
                let statusCode = result!["statusCode"]?.int
                if statusCode == 200
                {
                    self.navigationController?.popViewController(animated: true)
                }
                else{
                    if let message = result!["message"]?.string{
                       // AppHelper.showAlertView(message: errorMsg)
                        self.showAlertWithMessage(ConstantStrings.ALERT, message)
                    }
                }
              //  ERProgressHud.sharedInstance.hide()
            }else{
                self.showAlertWithMessage("ALERT",  "Something Went Wrong")
              //  ERProgressHud.sharedInstance.hide()
            }
        }
    }
}

