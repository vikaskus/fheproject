//
//  ReminderTypeViewController.swift
//  FHE
//
//  Created by Macintosh on 29/10/21.
//  Copyright © 2021 sylar. All rights reserved.
//

import UIKit
protocol ReminderTypeViewControllerDelegate {
    func typeSelect(type:String)
}
class ReminderTypeViewController: BaseViewControllerClass {
    
    var selectedType = ""
    var delegate:ReminderTypeViewControllerDelegate?
    
    let arraySelect = ["Do not Repeat","Repeat once every hour","Repeat once every day"]
    
    static var viewControllerId = "ReminderTypeViewController"
    static var storyBoard = "Main"

    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view1: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        selectedType = arraySelect[0]
        view1.backgroundColor = UIColor.purple
        view2.backgroundColor = UIColor.clear
        view3.backgroundColor = UIColor.clear

        // Do any additional setup after loading the view.
    }
    

    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnDontRepeatAction(_ sender: Any) {
        selectedType = arraySelect[0]
        view1.backgroundColor = UIColor.purple
        view2.backgroundColor = UIColor.clear
        view3.backgroundColor = UIColor.clear
        self.delegate?.typeSelect(type: selectedType)
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func btnOnceEveryHourAction(_ sender: Any) {
        selectedType = arraySelect[1]
        view2.backgroundColor = UIColor.purple
        view1.backgroundColor = UIColor.clear
        view3.backgroundColor = UIColor.clear
        self.delegate?.typeSelect(type: selectedType)
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnOnceEveryDayAction(_ sender: Any) {
        selectedType = arraySelect[2]
        view3.backgroundColor = UIColor.purple
        view2.backgroundColor = UIColor.clear
        view1.backgroundColor = UIColor.clear
        self.delegate?.typeSelect(type: selectedType)
        self.navigationController?.popViewController(animated: true)
    }
    
    
}
