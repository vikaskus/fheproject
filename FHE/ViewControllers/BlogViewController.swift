//
//  BlogViewController.swift
//  FHE
//
//  Created by Macintosh on 30/10/21.
//  Copyright © 2021 sylar. All rights reserved.
//

import UIKit

class BlogViewController: BaseViewControllerClass {
    
    static var viewControllerId = "BlogViewController"
    static var storyBoard = "Main"
    
    //  @IBOutlet weak var sliderCollectionView: UICollectionView!
    @IBOutlet weak var segmentController: UISegmentedControl!
    
    @IBOutlet weak var itemList: UITableView!
    
    var blogData = [BlogDataModel]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setInitials()
        fetchBlogDataApi(urlString: getBlogUrl)
    }
    
    
    func setInitials(){
        // sliderCollectionView.delegate = self
        //  sliderCollectionView.dataSource = self
        //  sliderCollectionView.register(UINib(nibName: "TabSectionCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "TabSectionCollectionViewCell")
        
        itemList.delegate = self
        itemList.dataSource = self
        
        itemList.register(UINib(nibName: "BlogTableViewCell", bundle: nil), forCellReuseIdentifier: "BlogTableViewCell")
    }
    
    
    @IBAction func segementControlChanged(_ sender: Any) {
        switch segmentController.selectedSegmentIndex
        {
        case 0:
            
            //show popular view
            fetchBlogDataApi(urlString: getBlogUrl)
        case 1:
            NSLog("History selected")
            fetchBlogDataApi(urlString: getPlanUrl)
        //show history view
        case 2:
            fetchBlogDataApi(urlString: getTipsUrl)
        case 3:
            blogData.removeAll()
            itemList.reloadData()
            print("Teen")
        case 4:
            blogData.removeAll()
            itemList.reloadData()
            print("Covid")
        //show history view
        default:
            break;
        }
    }
    
    
}
/*
 extension BlogViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
 
 func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
 return array.count
 }
 
 func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
 let cell = sliderCollectionView.dequeueReusableCell(withReuseIdentifier: "TabSectionCollectionViewCell", for: indexPath) as? TabSectionCollectionViewCell
 cell?.lblTitle.text = array[indexPath.row]
 
 return cell!
 }
 
 func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
 return CGSize(width: 100, height: 50)
 }
 
 func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
 
 }
 
 
 }
 */
extension BlogViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch segmentController.selectedSegmentIndex {
        case 0,1,2:
        
           return self.blogData.count
        case 3:
            return 0
        case 4:
            return 0
        default:
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = itemList.dequeueReusableCell(withIdentifier: "BlogTableViewCell") as? BlogTableViewCell
        if self.blogData.count > 0{
            cell?.lblTitle.text = self.blogData[indexPath.row].postData
        }
        cell?.selectionStyle = .none
        return cell!
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
    }
    
}
extension BlogViewController{
    
    func fetchBlogDataApi(urlString:String){
        
        /*
         if !AppHelper.isInterNetConnectionAvailable(){
         showAlertWithMessage(ConstantStrings.ALERT, ConstantStrings.pleaseCheckYourInternetConnection)
         return
         }
         */
        
        WebServiceHandler.performGETRequest(withURL: urlString) {(result,error) in
            if result != nil{
                print(result!)
                
                let statusCode = result!["statusCode"]?.int
                if statusCode == 200
                {
                    
                    print(statusCode)
                    if let data = result?["data"]?.array{
                        self.blogData = BlogDataModel.getAllJSONList(dataArray: data)
                        
                        
                        
                        
                        self.itemList.reloadData()
                        print(data)
                    }
                }
                else{
                    if let message = result!["message"]?.string{
                        // AppHelper.showAlertView(message: errorMsg)
                        self.showAlertWithMessage(ConstantStrings.ALERT, message)
                    }
                }
                
            }else{
                self.showAlertWithMessage("ALERT",  "Something Went Wrong")
                
            }
        }
    }
}
