//
//  ReminderDetailsViewController.swift
//  FHE
//
//  Created by Macintosh on 29/10/21.
//  Copyright © 2021 sylar. All rights reserved.
//

import UIKit

class ReminderDetailsViewController: BaseViewControllerClass {
    
    static var viewControllerId = "AddReminderViewController"
    static var storyBoard = "Main"

    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblReminderText: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
