//
//  AddExerciseViewController.swift
//  FHE
//
//  Created by Macintosh on 29/10/21.
//  Copyright © 2021 sylar. All rights reserved.
//

import UIKit

class AddExerciseViewController: BaseViewControllerClass {
    @IBOutlet weak var itemList: UITableView!
    @IBOutlet weak var txtFieldSearchExercise: UITextField!
    
    @IBOutlet weak var tableContainer: UIView!
    static var viewControllerId = "AddExerciseViewController"
    static var storyBoard = "Main"
    
    var exerciseList = [ExerciseModel]()
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        setInitials()

        // Do any additional setup after loading the view.
    }
    
    func setInitials(){
        
       // CRPSmartBandSDK.sharedInstance.delegate = self
        itemList.delegate = self
        itemList.dataSource = self
        
        itemList.register(UINib(nibName: "AddExerciseTableViewCell", bundle: nil), forCellReuseIdentifier: "AddExerciseTableViewCell")
        txtFieldSearchExercise.delegate = self
        txtFieldSearchExercise.attributedPlaceholder = NSAttributedString(string: "Search Exercise",
                                                                          attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
    
    }
    

    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

}
extension AddExerciseViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return exerciseList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = itemList.dequeueReusableCell(withIdentifier: "AddExerciseTableViewCell") as? AddExerciseTableViewCell
        if exerciseList.count > 0{
            cell?.lblExerciseTitle.text = exerciseList[indexPath.row].exerciseName
        }
        cell?.selectionStyle = .none
        return cell!
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller = ExerciseDetailsViewController.instantiateFromStoryBoard()
        controller.exerciseData = exerciseList[indexPath.row]
        self.push(controller)
       
    }
    
}
extension AddExerciseViewController:UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.05){
            self.fetchExerciseListApi()
        }
        return true
    }
    
}
extension AddExerciseViewController{
    
    func fetchExerciseListApi(){
        /*
        if !AppHelper.isInterNetConnectionAvailable(){
            showAlertWithMessage(ConstantStrings.ALERT, ConstantStrings.pleaseCheckYourInternetConnection)
            return
        }
        */
        self.exerciseList.removeAll()
        
        WebServiceHandler.performGETRequest(withURL: getExerciseList + "exerciseName=\(txtFieldSearchExercise.text!)") {(result,error) in
            if result != nil{
                print(result!)
                
                let statusCode = result!["statusCode"]?.int
                if statusCode == 200
                {
                  print(statusCode)
                    if let data = result?["data"]?.array{
                        self.exerciseList = ExerciseModel.getAllJSONList(dataArray: data)
                        print(self.exerciseList)
                        self.itemList.reloadData()
                        print(data)
                    }
                }
                else{
                    if let message = result!["message"]?.string{
                       // AppHelper.showAlertView(message: errorMsg)
                        self.showAlertWithMessage(ConstantStrings.ALERT, message)
                    }
                }
            }else{
                self.showAlertWithMessage("ALERT",  "Something Went Wrong")
                
            }
        }
    }
    
}
