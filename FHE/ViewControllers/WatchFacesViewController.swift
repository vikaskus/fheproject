//
//  WatchFacesViewController.swift
//  FHE
//
//  Created by Macintosh on 08/11/21.
//  Copyright © 2021 sylar. All rights reserved.
//

import UIKit
import CRPSmartBand
import CoreBluetooth

class WatchFacesViewController: BaseViewControllerClass {
    let manager = CRPSmartBandSDK.sharedInstance
    
    static var viewControllerId = "WatchFacesViewController"
    static var storyBoard = "Main"
    var selectedIndex = 0
    var watchIndex = 1
    
    var managerBLE: CBCentralManager?
    
    @IBOutlet weak var itemList: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setInitialsWatchData()
        setInitials()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        manager.getDial({ (value, error) in
            print(value)
            self.watchIndex = value
            self.selectedIndex = self.watchIndex - 1
            self.itemList.reloadData()
        })
    }
    
    
    func setInitials(){
       // CRPSmartBandSDK.sharedInstance.delegate = self
        itemList.delegate = self
        itemList.dataSource = self
        
        itemList.register(UINib(nibName: "WatchFaceTableViewCell", bundle: nil), forCellReuseIdentifier: "WatchFaceTableViewCell")
    }
    

    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
extension WatchFacesViewController:CRPManagerDelegate,CBCentralManagerDelegate {
    
    

    func bluetoothStatus() {
        managerBLE = CBCentralManager(delegate: self, queue: nil, options: nil)
    }

    

    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch central.state {
        case .poweredOn:
            break
        case .poweredOff:
            print("Bluetooth is Off.")
        //    self.showAlertWithMessage("ALERT", "Please turn On your Bluetooth")
        //    startScanTapped = false
       //     btnStartStop.setTitle("START SCAN", for: .normal)
            
            break
        case .resetting:
            break
        case .unauthorized:
            break
        case .unsupported:
            break
        case .unknown:
            break
        default:
            break
        }
    }
    
    
  
    
  
    
    
    func didState(_ state: CRPState) {
        print("Connect state: \(state.rawValue)")
        if state == .connected{
            UserDetails.sharedInstance.connectedDevice = CRPSmartBandSDK.sharedInstance.currentCRPDiscovery
            UserDetails.sharedInstance.isDeviceConnected = true
           
            let alert = UIAlertController(title: "", message: "Connected to \(UserDetails.sharedInstance.connectedDevice.remotePeripheral.name!)", preferredStyle: UIAlertController.Style.alert)
            let cancel = UIAlertAction(title: "OK", style: UIAlertAction.Style.default,handler: {
            action in
                self.dismiss(animated: true, completion: nil)
               
            })
            
                    alert.addAction(cancel)
                    self.present(alert, animated: true, completion: nil)
            self.getData()
            CRPSmartBandSDK.sharedInstance.checkDFUState { (dfu, err) in
                print("dfu =\(dfu)")
            }
        }
    }
    
    func didBluetoothState(_ state: CRPBluetoothState) {
        print(state)
    }
    
    func receiveSteps(_ model: StepModel) {
        print(model)
    
        UserDetails.sharedInstance.steps = "\(model.steps)"
        UserDetails.sharedInstance.stepsTime = "\(model.time)"
        UserDetails.sharedInstance.stepsCalorie = "\(model.calory)"
        UserDetails.sharedInstance.stepsDistance = "\(model.distance)"
        
        let param = [ "currentStatus": "0",
                      "stepCount": UserDetails.sharedInstance.steps,
                      "caloriesBurned": UserDetails.sharedInstance.stepsCalorie,
                      "distance": UserDetails.sharedInstance.stepsDistance,
                      "duration": UserDetails.sharedInstance.stepsTime]
        
    
        NotificationCenter.default.post(name: .postNotification, object: "myObject", userInfo: ["dataType": "steps","walkSteps":"\(model.steps)","walkDuration":"\(model.time)","walkDistance":"\(model.distance)","walkCalories":"\(model.calory)"])
       
        
        let manager = CRPSmartBandSDK.sharedInstance
        
       // manager.setDial(1)
        manager.getSportData({ (model, error) in
            var sportsData = [SportsModel]()
            
            for i in model{
                var sportsString = ""
                switch(i.type.rawValue){
                case  0:
                sportsString = "walking"

                case 1:
                sportsString = "run"

                case 2:
                sportsString = "cycling"

                case  3:
                sportsString = "skipping"

                case 4:
                sportsString = "badminton"

                case  5:
                sportsString = "basketball"

                case 6:
                sportsString = "football"

                case  7:
                sportsString = "swimming"

                case  8:
                sportsString = "hiking"

                case 9:
                sportsString = "tennis"

                case 10:
                sportsString = "rugby"

                case 11:
                sportsString = "golf"

                case 12:
                sportsString = "yoga"

                case 13:
                sportsString = "workout"

                case 14:
                sportsString = "dance"

                case 15:
                sportsString = "baseball"

                case  16:
                sportsString = "elliptical"

                case 17:
                sportsString = "indoorCycling"

                case  18:
                sportsString = "training"

                case  19:
                sportsString = "rowingMachine"

                case  20:
                sportsString = "trailRun"

                case  21:
                sportsString = "ski"

                case  22:
                sportsString = "bowling"

                case  23:
                sportsString = "dumbbells"

                case  24:
                sportsString = "sit_ups"

                case  25:
                sportsString = "onfoot"

                case  26:
                sportsString = "indoorWalk"

                case  27:
                sportsString = "indoorRun"
                default:
                    sportsString = ""
                }
                
                
                sportsData.append(SportsModel(date: "\(i.date)", startTime: "\(i.startTime)", endTime: "\(i.endTime)", vaildTime: "\(i.vaildTime)", type: "\(sportsString)", step: "\(i.step)", distance: "\(i.distance)", kcal: "\(i.kcal)"))
            }
            AppHelper.sharedInstance.sportsData = sportsData
            
            NotificationCenter.default.post(name: .postNotification, object: "myObject", userInfo: ["dataType": "sports"])
           
            print(model)
        })
        
        manager.getSleepData({ (model, error) in
            print(model)
            var sleepModel = [SleepModel]()
            /*
            for i in model.detail{
                sleepModel.append(SleepModel(total: i.t, start: , end: , type: ))
            }
            */
            
            let sleepArray = model.detail
            for i in model.detail{
                let sleepData = i as? NSDictionary
                sleepModel.append(SleepModel(total: i["total"] as! String, start: i["start"] as! String, end: i["end"] as! String, type: i["type"] as! String))
            }
            AppHelper.sharedInstance.sleepData = sleepModel
            
            print("Deep sleep\(model.deep)Minute Light sleep\(model.light)Minute")
            NotificationCenter.default.post(name: .postNotification, object: "myObject", userInfo: ["dataType": "sleep","Light":"\(model.light)","Deep":"\(model.deep)"/*,"drillDown":sleepModel*/])
        })

        
    }
    
    func receiveHeartRate(_ heartRate: Int) {
        print(heartRate)
        UserDetails.sharedInstance.heartRateData = "\(heartRate)"
        NotificationCenter.default.post(name: .postNotification, object: "myObject", userInfo: ["dataType": "heartRate","heartRate":"\(heartRate)"])
    }
    
    func receiveHeartRateAll(_ model: HeartModel) {
        print(model)
        
    }
    
    func receiveBloodPressure(_ heartRate: Int, _ sbp: Int, _ dbp: Int) {
        print(heartRate)
        UserDetails.sharedInstance.sbp = "\(sbp)"
        UserDetails.sharedInstance.dbp = "\(dbp)"
        NotificationCenter.default.post(name: .postNotification, object: "myObject", userInfo: ["dataType": "bloodPressure","heartRate":"\(heartRate)","sbp":"\(sbp)","dbp":"\(dbp)"])
    }
    
    func receiveSpO2(_ o2: Int) {
        UserDetails.sharedInstance.oxygenData = "\(o2)"
        print(o2)
        NotificationCenter.default.post(name: .postNotification, object: "myObject", userInfo: ["dataType": "o2","o2":"\(o2)"])
        
    }
    
    

    func setInitialsWatchData(){
        CRPSmartBandSDK.sharedInstance.delegate = self
        getData()
    }
    
    
    func recevieTakePhoto() {
        print("recevieTakePhoto")
    }
    
    
    func receiveUpgradeScreen(_ state: CRPUpgradeState, _ progress: Int) {
        print("state = \(state.description()), progress = \(progress)")
        
    }
    
    func receiveRealTimeHeartRate(_ heartRate: Int, _ rri: Int) {
        print("heart rate is \(heartRate)")
        
    }
    func receiveUpgrede(_ state: CRPUpgradeState, _ progress: Int) {
        print("state = \(state.description()), progress = \(progress)")
    }
    func recevieWeather() {
        print("recevieWeather")
    }
    
    
    func getData(){
     
        manager.getWatchFaceSupportModel { (model, error) in
            print("currentID = \(model.currentID)")
            print("supportModel = \(model.supportModel)")
        }
        
        manager.getDial({ (value, error) in
            print(value)
        })
        
        print("This is Supported Watch Faces")
        
        /*
            manager.getSteps({ (model, error) in
                print(model)
//                let cal
//                let text = "Step:\(model)"
                print("\(model.steps)step \(model.calory)kcal \(model.distance)m , \(model.time)s")
            })
      
            manager.getSleepData({ (model, error) in
                print(model)
                var sleepModel = [SleepModel]()
                /*
                for i in model.detail{
                    sleepModel.append(SleepModel(total: i.t, start: , end: , type: ))
                }
                */
                
                let sleepArray = model.detail
                for i in model.detail{
                    let sleepData = i as? NSDictionary
                    sleepModel.append(SleepModel(total: i["total"] as! String, start: i["start"] as! String, end: i["end"] as! String, type: i["type"] as! String))
                }
                AppHelper.sharedInstance.sleepData = sleepModel
                
                print("Deep sleep\(model.deep)Minute Light sleep\(model.light)Minute")
                NotificationCenter.default.post(name: .postNotification, object: "myObject", userInfo: ["dataType": "sleep","Light":"\(model.light)","Deep":"\(model.deep)"/*,"drillDown":sleepModel*/])
            })
    
            manager.getFeatures({ (features, error) in
                print("Support：\(features)")
            })
        
//            if let zippath = Bundle.main.path(forResource: "MOY-SFP4-1.7.9", ofType: "zip"){
//                manager.getOTAMac { (otaMac, error) in
//                    manager.startOTAFromFile(mac: otaMac, zipFilePath: zippath, isUser: false,false)
//                }
//            }
       
            manager.getSoftver({ (ver, error) in
                print(error)
              //  (self.view.viewWithTag(201) as! UILabel).text = ver
                print("varsion：\(ver)")
            })
      
            manager.getBattery({ (battery, error) in
              //  (self.view.viewWithTag(202) as! UILabel).text = String(battery)
                print("Battery：\(battery)")
            })
       
            manager.getGoal({ (value, error) in
              //  (self.view.viewWithTag(203) as! UILabel).text = String(value)
                print("Goal：\(value)")
            })
        
            manager.getDominantHand({ (value, error) in
               // (self.view.viewWithTag(204) as! UILabel).text = String(value)
                print("Hand：\(value)")
            })
       
            manager.getAlarms({ (alarms, error) in
                print(alarms)
            })
     
            manager.getProfile({ (profile, error) in
              print("\(profile)")
            })
       
            manager.getLanguage { (value, CRPErrorerror) in
                print("\(value)")
            } _: { (indexs, error) in
                print("index = \(indexs)")
            }

       
            manager.getDial({ (value, error) in
                print("\(value)")
            })
       
            manager.getRemindersToMove({ (value, error) in
                print("\(value)")
            })
       
            manager.getQuickView({ (value, error) in
                print("\(value)")
            })

            manager.getUnit({ (value, error) in
                print("\(value)")
            })
      
            manager.getTimeformat({ (value, error) in
                print("\(value)")
            })
     
            manager.getMac({ (value, error) in
                print("\(value)")
            })
        
            manager.getNotifications({ (value, error) in
                print(value)
            })
        
            manager.getNotifications({ (value, error) in
                print(value)
                print("\(value.description)")
            })
           
  
          //  manager.setStartSpO2()
          
      
         //   manager.setStopSpO2()
//            manager.getHeartData()
            manager.getMac { (mac, err) in
                manager.getSoftver { (ver, err) in
                    manager.checkLatest(mac, ver) { (newInfo, tpNewInfo, err) in
                        print("new = \(newInfo), tpNewInfo = \(tpNewInfo)")
                    }
                }
            }
           */
        
    }
        
}
extension WatchFacesViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
      
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = itemList.dequeueReusableCell(withIdentifier: "WatchFaceTableViewCell") as? WatchFaceTableViewCell
        if self.watchIndex <= 3{
            cell?.lblWatchFace.text = "Watch Face \(indexPath.row + 1)"
            if indexPath.row == selectedIndex{
                cell?.activeInactiveView.backgroundColor = Colors.themeGreen
            }else{
                cell?.activeInactiveView.backgroundColor = UIColor.clear
            }
        }
       
        cell?.selectionStyle = .none
        return cell!
        
       
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 120
     
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            manager.setDial(1)
            selectedIndex = 0
            watchIndex = 1
        }else if indexPath.row == 1{
            manager.setDial(2)
            selectedIndex = 1
            watchIndex = 2
        }else if indexPath.row == 2{
            watchIndex = 3
            selectedIndex = 2
            manager.setDial(3)
       
        }
        itemList.reloadData()
    }
}
