//
//  SummaryViewController.swift
//  FHE
//
//  Created by Macintosh on 25/10/21.
//  Copyright © 2021 sylar. All rights reserved.
//

import UIKit
import FSCalendar

class SummaryViewController: BaseViewControllerClass{
    
    
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    static var viewControllerId = "SummaryViewController"
    static var storyBoard = "Main"
    
    var index:Int?
    var stepsData = [StepsDataModel]()
    var bpData = [BPDataModel]()
    var heartRateData = [HeartRateModel]()
    var oxygenData = [OxygenModel]()
    
    
    @IBOutlet weak var itemList: UITableView!
    
    @IBOutlet weak var calendar: FSCalendar!
    
    var timeFrame = "daily"
    var dateString = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        setInitials()
      //  heightConstraint.constant = 0
        self.calendar.isHidden = true
        self.calendar.scope = .week
        self.calendar.delegate = self
        self.calendar.dataSource = self
        
        if index == 0{
            let params = ["timeFrame": timeFrame,
                          "dateString": dateString,]
            fetchStepsDataApi(params: params)
        }else if index == 1{
            let params = ["timeFrame": timeFrame,
                          "dateString": dateString,]
            fetchBPDataApi(params: params)
        }else if index == 2{
            let params = ["timeFrame": timeFrame,
                          "dateString": dateString,]
            fetchHeartRateDataApi(params: params)
        }else if index == 3{
            let params = ["timeFrame": timeFrame,
                          "dateString": dateString,]
            fetchOxygenDataApi(params: params)
        }
  
        // Do any additional setup after loading the view.
    }
    
    
    func setInitials(){
        itemList.delegate = self
        itemList.dataSource = self
        itemList.register(UINib(nibName: "StepsTableViewCell", bundle: nil), forCellReuseIdentifier: "StepsTableViewCell")
        itemList.register(UINib(nibName: "BPTableViewCell", bundle: nil), forCellReuseIdentifier: "BPTableViewCell")
        itemList.register(UINib(nibName: "PulseOxyTableViewCell", bundle: nil), forCellReuseIdentifier: "PulseOxyTableViewCell")
        itemList.register(UINib(nibName: "GraphTableViewCell", bundle: nil), forCellReuseIdentifier: "GraphTableViewCell")
        
        
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func btnDailyAction(_ sender: Any) {
      //  heightConstraint.constant = 0
        self.calendar.isHidden = true
      //  self.calendar.setScope(.week, animated: true)
    }
    
    @IBAction func btnWeeklyAction(_ sender: Any) {
      //  heightConstraint.constant = 300
        self.calendar.isHidden = false
        self.calendar.setScope(.week, animated: true)
    }
    
    @IBAction func btnMonthlyAction(_ sender: Any) {
       // heightConstraint.constant = 300
        self.calendar.isHidden = false
        self.calendar.setScope(.month, animated: true)
    }

    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
extension SummaryViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if index == 0{
            return 2
        }else{
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch index {
        case 0:
            if indexPath.row == 0{
                let cell = itemList.dequeueReusableCell(withIdentifier: "StepsTableViewCell") as? StepsTableViewCell
                if self.stepsData.count > 0{
                    cell?.lblDistance.text = self.stepsData[indexPath.row].distance
                    cell?.lblStepCount.text = self.stepsData[indexPath.row].stepCount
                    cell?.lblCalorieBurned.text = self.stepsData[indexPath.row].caloriesBurned
                }
               
                cell?.selectionStyle = .none
                return cell!
            }else{
                let cell = itemList.dequeueReusableCell(withIdentifier: "GraphTableViewCell")
                cell?.selectionStyle = .none
                return cell!
            }
            
        case 1:
            let cell = itemList.dequeueReusableCell(withIdentifier: "BPTableViewCell") as? BPTableViewCell
            if self.bpData.count > 0{
                cell?.lblSystolic.text = self.bpData[indexPath.row].systolicPressure
                cell?.lblDiastolic.text = self.bpData[indexPath.row].diastolicPressure
            }
           
            cell?.selectionStyle = .none
            return cell!
        
        case 2:
            let cell = itemList.dequeueReusableCell(withIdentifier: "PulseOxyTableViewCell") as? PulseOxyTableViewCell
            if self.heartRateData.count > 0{
                cell?.lblValue.text = self.heartRateData[indexPath.row].beatsPerMinute
            }
            cell?.lblTitle.text = "Beats Per Minute"
            cell?.selectionStyle = .none
            return cell!
            
        case 3:
            let cell = itemList.dequeueReusableCell(withIdentifier: "PulseOxyTableViewCell") as? PulseOxyTableViewCell
            if self.oxygenData.count > 0{
                cell?.lblValue.text = self.oxygenData[indexPath.row].oxygenPercentage
            }
            cell?.lblTitle.text = "Percent"
            cell?.featureImageView.image = UIImage(named: "oxygen")
            cell?.selectionStyle = .none
            return cell!
            
        default:
            let cell = itemList.dequeueReusableCell(withIdentifier: "PulseOxyTableViewCell")
            cell?.selectionStyle = .none
            return cell!
        }
       
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if index == 0{
            if indexPath.row == 0{
                return 120
            }else{
                return 150
            }
        }else{
            return 120
        }
        
    }
    
    
}
extension SummaryViewController:FSCalendarDataSource, FSCalendarDelegate  {
    
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        self.heightConstraint.constant = bounds.height
        self.view.layoutIfNeeded()
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        print(date)
       
        dateString = AppHelper.convertDateToFormattedDateString(date: date)
        if index == 0{
            let params = ["timeFrame": timeFrame,
                          "dateString": dateString,]
            fetchStepsDataApi(params: params)
        }else if index == 1{
            let params = ["timeFrame": timeFrame,
                          "dateString": dateString,]
            fetchBPDataApi(params: params)
        }else if index == 2{
            let params = ["timeFrame": timeFrame,
                          "dateString": dateString,]
            fetchHeartRateDataApi(params: params)
        }else if index == 3{
            let params = ["timeFrame": timeFrame,
                          "dateString": dateString,]
            fetchOxygenDataApi(params: params)
        }
    }
}
extension SummaryViewController{
    
    
    func fetchStepsDataApi(params:[String:String]){
        /*
        if !AppHelper.isInterNetConnectionAvailable(){
            showAlertWithMessage(ConstantStrings.ALERT, ConstantStrings.pleaseCheckYourInternetConnection)
            return
        }
        */
      //  ERProgressHud.sharedInstance.showBlurView(withTitle: "Loading...")
        WebServiceHandler.performPOSTRequest(withURL: getStepsSummary, andParameters: params) {(result,error) in
            if result != nil{
                print(result!)
                
                let statusCode = result!["statusCode"]?.int
                if statusCode == 200
                {
                    if let data = result?["data"]?.dictionary{
                        if let dataList = data["dataList"]?.array{
                            self.stepsData = StepsDataModel.getAllJSONList(dataArray: dataList)
                            print(self.stepsData)
                        }
                        print(data)
                        self.itemList.reloadData()
                    }
                }
                else{
                    if let message = result!["message"]?.string{
                       // AppHelper.showAlertView(message: errorMsg)
                        self.showAlertWithMessage(ConstantStrings.ALERT, message)
                    }
                }
              //  ERProgressHud.sharedInstance.hide()
            }else{
                self.showAlertWithMessage("ALERT",  "Something Went Wrong")
              //  ERProgressHud.sharedInstance.hide()
            }
        }
    }
    
    func fetchHeartRateDataApi(params:[String:String]){
        /*
        if !AppHelper.isInterNetConnectionAvailable(){
            showAlertWithMessage(ConstantStrings.ALERT, ConstantStrings.pleaseCheckYourInternetConnection)
            return
        }
        */
      //  ERProgressHud.sharedInstance.showBlurView(withTitle: "Loading...")
        WebServiceHandler.performPOSTRequest(withURL: getheartRateSummary, andParameters: params) {(result,error) in
            if result != nil{
                print(result!)
                
                let statusCode = result!["statusCode"]?.int
                if statusCode == 200
                {
                   
                    if let data = result?["data"]?.dictionary{
                        if let dataList = data["dataList"]?.array{
                            self.heartRateData = HeartRateModel.getAllJSONList(dataArray: dataList)
                            print(self.heartRateData)
                        }
                        print(data)
                        self.itemList.reloadData()
                        
                    }
                }
                else{
                    if let message = result!["message"]?.string{
                       // AppHelper.showAlertView(message: errorMsg)
                        self.showAlertWithMessage(ConstantStrings.ALERT, message)
                    }
                }
              //  ERProgressHud.sharedInstance.hide()
            }else{
                self.showAlertWithMessage("ALERT",  "Something Went Wrong")
              //  ERProgressHud.sharedInstance.hide()
            }
        }
    }
    
    func fetchOxygenDataApi(params:[String:String]){
        /*
        if !AppHelper.isInterNetConnectionAvailable(){
            showAlertWithMessage(ConstantStrings.ALERT, ConstantStrings.pleaseCheckYourInternetConnection)
            return
        }
        */
      //  ERProgressHud.sharedInstance.showBlurView(withTitle: "Loading...")
        WebServiceHandler.performPOSTRequest(withURL: getOxygenSummary, andParameters: params) {(result,error) in
            if result != nil{
                print(result!)
                
                let statusCode = result!["statusCode"]?.int
                if statusCode == 200
                {
                   
                    if let data = result?["data"]?.dictionary{
                        if let dataList = data["dataList"]?.array{
                            self.oxygenData = OxygenModel.getAllJSONList(dataArray: dataList)
                            print(self.oxygenData)
                        }
                        print(data)
                        self.itemList.reloadData()
                        
                    }
                }
                else{
                    if let message = result!["message"]?.string{
                       // AppHelper.showAlertView(message: errorMsg)
                        self.showAlertWithMessage(ConstantStrings.ALERT, message)
                    }
                }
              //  ERProgressHud.sharedInstance.hide()
            }else{
                self.showAlertWithMessage("ALERT",  "Something Went Wrong")
              //  ERProgressHud.sharedInstance.hide()
            }
        }
    }
    
    func fetchBPDataApi(params:[String:String]){
        /*
        if !AppHelper.isInterNetConnectionAvailable(){
            showAlertWithMessage(ConstantStrings.ALERT, ConstantStrings.pleaseCheckYourInternetConnection)
            return
        }
        */
      //  ERProgressHud.sharedInstance.showBlurView(withTitle: "Loading...")
        WebServiceHandler.performPOSTRequest(withURL: getBloodPressureSummary, andParameters: params) {(result,error) in
            if result != nil{
                print(result!)
                
                let statusCode = result!["statusCode"]?.int
                if statusCode == 200
                {
                   
                    if let data = result?["data"]?.dictionary{
                        if let dataList = data["dataList"]?.array{
                            self.bpData = BPDataModel.getAllJSONList(dataArray: dataList)
                            print(self.bpData)
                        }
                        print(data)
                        self.itemList.reloadData()
                        
                    }
                }
                else{
                    if let message = result!["message"]?.string{
                       // AppHelper.showAlertView(message: errorMsg)
                        self.showAlertWithMessage(ConstantStrings.ALERT, message)
                    }
                }
              //  ERProgressHud.sharedInstance.hide()
            }else{
                self.showAlertWithMessage("ALERT",  "Something Went Wrong")
              //  ERProgressHud.sharedInstance.hide()
            }
        }
    }
}
