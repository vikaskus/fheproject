//
//  WebServiceHandler.swift
//
//  Created by Vikash Rajput on 3/8/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class WebServiceHandler: NSObject {
    
    class func performGETRequestForMapPath(withURL urlString: String, completion completionBlock: @escaping (_ result: [String: JSON]?, _ error: Error?) -> Void) {
        
        Alamofire.request(urlString).response { response in // method defaults to
            
            let statusCode = response.response?.statusCode
            
            if statusCode == 200{
                
                let responseJSON = JSON(response.data as Any);
                print("responseJSON::", responseJSON)
                completionBlock(responseJSON.dictionary, nil)
            }
            else{
                completionBlock(nil, response.error)
            }
        }
    }
    
    class func performGETRequest(withURL urlString: String, completion completionBlock: @escaping (_ result: [String: JSON]?, _ error: Error?) -> ()) {
        
        var token = UserDetails.sharedInstance.accessToken
      
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "accesstoken":token
        ]
        
        let encodedUrl = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            
        
        Alamofire.request(encodedUrl!, method : .get, parameters : nil, encoding : JSONEncoding.default , headers : headers).responseData { (response) in
           
            let statusCode = response.response?.statusCode
            print("Status Code \(statusCode)")
            if statusCode == 200{
                let responseJSON = JSON(response.data!)
                print(responseJSON)
                completionBlock(responseJSON.dictionary, response.error)
            }
            else{
                completionBlock(nil, response.error)
            }
        }
    }
    
    
    class func performPOSTRequest(withURL urlString: String?, andParameters params: Parameters?, completion completionBlock: @escaping (_ result: [String: JSON]?, _ error: Error?) -> Void) {
        
        var token = ""
        if urlString != "http://fheapi.fourbrick.in/user/verifyOTP"{
            token = UserDetails.sharedInstance.accessToken
        }
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "accesstoken":token
        ]
        
        
        
        
        /*
        print(params!)
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in params! {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
        
        }, usingThreshold: UInt64.init(), to: urlString!, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in

                    if response.error != nil{
                        completionBlock(nil, response.error)
                        return
                    }
                    
                    let statusCode = response.response?.statusCode
                    if statusCode == 200{
                        let responseJSON = JSON(response.data as AnyObject);
                        print(responseJSON)
                        completionBlock(responseJSON.dictionary, nil)
                    }else{
                        completionBlock(nil, response.error)
                    }
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                completionBlock(nil, error)
            }
        
        */
        
      
        print(params!)
        Alamofire.request(urlString!, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers)
        
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)) { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON { response in

                let statusCode = response.response?.statusCode
                print("Status Code \(statusCode)")
                if statusCode == 200{
                    let responseJSON = JSON(response.data as Any);
                    print("responseJSON::", responseJSON)
                    
                    completionBlock(responseJSON.dictionary, nil)
                }
                else{
                    completionBlock(nil, response.error)
                }
        }
    }
    
    
    class func performPUTRequest(withURL urlString: String?, andParameters params: Parameters?, completion completionBlock: @escaping (_ result: [String: JSON]?, _ error: Error?) -> Void) {
        
    
        let headers: HTTPHeaders = [
            "Content-Type": "application/json"
        ]
        
        
        /*
        print(params!)
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in params! {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
        
        }, usingThreshold: UInt64.init(), to: urlString!, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in

                    if response.error != nil{
                        completionBlock(nil, response.error)
                        return
                    }
                    
                    let statusCode = response.response?.statusCode
                    if statusCode == 200{
                        let responseJSON = JSON(response.data as AnyObject);
                        print(responseJSON)
                        completionBlock(responseJSON.dictionary, nil)
                    }else{
                        completionBlock(nil, response.error)
                    }
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                completionBlock(nil, error)
            }
        
        */
        
      
        print(params!)
        Alamofire.request(urlString!, method: .put, parameters: params, encoding: JSONEncoding.default, headers: headers)
        
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)) { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON { response in

                let statusCode = response.response?.statusCode
                print("Status Code \(statusCode)")
                if statusCode == 200{
                    let responseJSON = JSON(response.data as Any);
                    print("responseJSON::", responseJSON)
                    
                    completionBlock(responseJSON.dictionary, nil)
                }
                else{
                    completionBlock(nil, response.error)
                }
        }
    }
    
    
    class func performPATCHRequest(withURL urlString: String?, andParameters params: Parameters?, completion completionBlock: @escaping (_ result: [String: JSON]?, _ error: Error?) -> Void) {
        
    
        let headers: HTTPHeaders = [
            "Content-Type": "application/json"
        ]
        
        
        /*
        print(params!)
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in params! {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
        
        }, usingThreshold: UInt64.init(), to: urlString!, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in

                    if response.error != nil{
                        completionBlock(nil, response.error)
                        return
                    }
                    
                    let statusCode = response.response?.statusCode
                    if statusCode == 200{
                        let responseJSON = JSON(response.data as AnyObject);
                        print(responseJSON)
                        completionBlock(responseJSON.dictionary, nil)
                    }else{
                        completionBlock(nil, response.error)
                    }
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                completionBlock(nil, error)
            }
        
        */
        
      
        print(params!)
        Alamofire.request(urlString!, method: .patch, parameters: params, encoding: JSONEncoding.default, headers: headers)
        
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)) { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON { response in

                let statusCode = response.response?.statusCode
                print("Status Code \(statusCode)")
                if statusCode == 200{
                    let responseJSON = JSON(response.data as Any);
                    print("responseJSON::", responseJSON)
                    
                    completionBlock(responseJSON.dictionary, nil)
                }
                else{
                    completionBlock(nil, response.error)
                }
        }
    }
    
    
    
    class func performPOSTRequestWithArray(withURL urlString: String?, andParameters params: [String: Any]?, andAcessToken accessToken: String?, completion completionBlock: @escaping (_ result: [String: JSON]?, _ error: Error?) -> Void) {
        
        var apiToken = accessToken
        
        if apiToken == nil {
            apiToken = ""
        }
        else if apiToken?.count == 0{
            //apiToken = UserDetails.sharedInstance.accessToken
        }
        let headers: HTTPHeaders = [
            "Accept": "application/json",
            "accessToken": apiToken!,
          //  "loginId":"\(UserDetails.sharedInstance.id)",
            "timezone":"Asia/Calcutta"
        ]
        
        print(params!)
        Alamofire.request(urlString!, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers)
            
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)) { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON { response in
                
                let statusCode = response.response?.statusCode
                print("Status Code \(statusCode)")
                
                if statusCode == 200{
                    let responseJSON = JSON(response.data as Any);
                    print("responseJSON::", responseJSON)
                    
                    if let dict = responseJSON.dictionary{
                        /*
                        if let filePathDict = dict["file_path"]?.dictionary{
                            UserDetails.sharedInstance.productImagePath = filePathDict["product_image"]?.string ?? ""
                            UserDetails.sharedInstance.gallerybasePath = filePathDict["gallery_image"]?.string ?? ""
                            UserDetails.sharedInstance.userImagePath = filePathDict["user_image"]?.string ?? ""
                            UserDetails.sharedInstance.storeImagePath = filePathDict["store_image"]?.string ?? ""
                            
                        }
                        */
                    }
                    completionBlock(responseJSON.dictionary, nil)
                }
                else if (statusCode == 403) || (statusCode == 401){
                    /*
                    if UserDetails.sharedInstance.userID.count != 0{
                        
                        AppHelper.callGenrateUserAccessTokenAPIWithLoader(isLoader: true, completionBlock: {
                            WebServiceHandler.performPOSTRequest(withURL: urlString, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken, completion: { (result, error) in
                                
                                if (!(error != nil)) {
                                    completionBlock(result,nil);
                                }
                                else{
                                    completionBlock(nil,error);
                                }
                            })
                        })
                    }
                    else{
                        completionBlock(nil, response.error)
                    }
                    */
                }
                else{
                    completionBlock(nil, response.error)
                }
        }
    }
    
    class func performPOSTRequestWithData(withURL urlString: String?, andParameters params: Parameters?, andAcessToken accessToken: String?, completion completionBlock: @escaping (_ result: [String: JSON]?, _ error: Error?) -> Void) {
        
        var apiToken = accessToken
        
        if apiToken == nil {
            apiToken = ""
        }
        else if apiToken?.count == 0{
           // apiToken = UserDetails.sharedInstance.accessToken
        }
        let headers: HTTPHeaders = [
            "Accept": "application/json",
            "accessToken": apiToken!,
         //   "loginId":"\(UserDetails.sharedInstance.id)",
            "timezone":"Asia/Calcutta"
        ]
        
        print(params!)
        Alamofire.request(urlString!, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers)
            
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)) { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON { response in
                
                let statusCode = response.response?.statusCode
                if statusCode == 200{
                    let responseJSON = JSON(response.data as Any);
                    print("responseJSON::", responseJSON)
                    
                    if let dict = responseJSON.dictionary{
                        /*
                        if let filePathDict = dict["file_path"]?.dictionary{
                            UserDetails.sharedInstance.productImagePath = filePathDict["product_image"]?.string ?? ""
                            UserDetails.sharedInstance.gallerybasePath = filePathDict["gallery_image"]?.string ?? ""
                            UserDetails.sharedInstance.userImagePath = filePathDict["user_image"]?.string ?? ""
                            UserDetails.sharedInstance.storeImagePath = filePathDict["store_image"]?.string ?? ""
                            
                        }
                        */
                    }
                    completionBlock(responseJSON.dictionary, nil)
                }
                else if (statusCode == 403) || (statusCode == 401){
                    
                    /*
                    if UserDetails.sharedInstance.userID.count != 0{
                        AppHelper.callGenrateUserAccessTokenAPIWithLoader(isLoader: true, completionBlock: {
                            WebServiceHandler.performPOSTRequest(withURL: urlString, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken, completion: { (result, error) in
                                
                                if (!(error != nil)) {
                                    completionBlock(result,nil);
                                }
                                else{
                                    completionBlock(nil,error);
                                }
                            })
                        })
                    }
                     
                    else{
                        completionBlock(nil, response.error)
                    }
                    */
                }
                else{
                    completionBlock(nil, response.error)
                }
        }
    }

    class func performMultipartRequest(urlString: String?, fileName: String, params: Parameters?,imageDataArray: Array<Data>, completion completionBlock: @escaping (_ result: [String: JSON]?, _ error: Error?) -> Void) {
    
        let headers: HTTPHeaders = [
           "Accept": "application/json",
          //  "loginId":"\(UserDetails.sharedInstance.id)",
            "timezone":"Asia/Calcutta"
        ]

        print(params!)
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in params! {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            var count = 0
            
            for data in imageDataArray{
                var imageName = fileName
                if count != 0{
                    imageName = imageName + String(count) + ".jpeg"
                }
                else{
                    imageName = imageName + ".jpeg"
                }
                multipartFormData.append(data, withName: fileName, fileName: imageName, mimeType: "image/jpeg")
                count += 1
            }
            
        }, usingThreshold: UInt64.init(), to: urlString!, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in

                    if response.error != nil{
                        completionBlock(nil, response.error)
                        return
                    }
                    
                    let statusCode = response.response?.statusCode
                    if statusCode == 200{
                        let responseJSON = JSON(response.data as AnyObject);
                        print(responseJSON.dictionary!)
                        completionBlock(responseJSON.dictionary, nil)
                    }
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                completionBlock(nil, error)
            }
        }
    }
}


