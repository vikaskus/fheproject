//
//  BaseViewController.swift
//  
//
//  Created by Suraj on 6/15/20.
//  Copyright © 2020 MAC. All rights reserved.
//

import UIKit

protocol Instantiable{
    static var viewControllerId:String{get}
    static var storyBoard:String{get}
    static func instantiateFromStoryBoard()->Self
}

extension Instantiable{
    static func instantiateFromStoryBoard()->Self{
        let storyboard = UIStoryboard(name: Self.storyBoard, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: Self.viewControllerId)
        return controller as! Self
    }
}

typealias BaseViewControllerClass = BaseViewController & Instantiable

class BaseViewController: UIViewController {
    
    let screenWidth = UIScreen.main.bounds.size.width
    let screenHeight = UIScreen.main.bounds.size.height
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    func push(_ controller:UIViewController){
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    //    MARK:- Show Alert With Message
    func showAlertWithMessage(_ title : String, _ message : String) {
        let alertController = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        
        let defaultAction = UIAlertAction.init(title: "OK", style: .default, handler: nil)
        alertController.addAction(defaultAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    //    Setup Drawer Controller
    func setupDrawerController() -> Void {
        /*
        let dashboardVC = DashboardViewController.instantiateFromStoryBoard()
        let mainViewController = UINavigationController.init(rootViewController: dashboardVC)
        mainViewController.viewWillAppear(true)
      
        let drawerVC = DrawerViewController.init(nibName: "DrawerViewController", bundle: nil)
        let drawerViewController = UINavigationController.init(rootViewController: drawerVC)
        /*
        if self.screenWidth == 320 {
            
            self.appDelegate.drawerController.drawerWidth = 250
        }else {
            
            self.appDelegate.drawerController.drawerWidth = 300
        }
        */
        self.appDelegate.drawerController.screenEdgePanGestureEnabled = true
        self.appDelegate.drawerController.mainViewController = mainViewController
        self.appDelegate.drawerController.drawerViewController = drawerViewController
        self.appDelegate.window?.rootViewController = self.appDelegate.drawerController
        self.appDelegate.window?.makeKeyAndVisible()
        */
    }
    
    
}

