//
//  Colors.swift
//  Medbulance
//
//  Created by Apple on 23/02/21.
//

import UIKit

class Colors: NSObject {
    
    static let backgroundBG = UIColor(red: 217/255, green: 38/255, blue: 41/255, alpha: 1)
    static let themeColor = UIColor(red: 252/255, green: 49/255, blue: 52/255, alpha: 1)
    static let bgColor51 = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
    static let bgColor81 = UIColor(red: 81/255, green: 81/255, blue: 81/255, alpha: 1)
    static let themeGreen = UIColor(red: 0/255, green: 214/255, blue: 0/255, alpha: 1)

}
