//
//  StoryboardConstant.swift
//  Medbulance
//
//  Created by Apple on 09/03/21.
//

import Foundation

struct StoryboardConstant {
    static let main = "Main"
    static let driver = "Driver"
    static let user = "User"
}
