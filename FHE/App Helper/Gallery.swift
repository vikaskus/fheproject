//
//  Gallery.swift
//
//  Created by Dheeraj on 24/10/17.
//  Copyright © 2017 Dheeraj. All rights reserved.
//

import UIKit

class Gallery: NSObject {
    /*
    
    private let mCallback: (UIImage)->Void
    private weak var mRoot: UIViewController?
    
    private lazy var pickerController: UIImagePickerController = {
        let picker                    = UIImagePickerController()
        picker.delegate               = self
        picker.allowsEditing          = false
        picker.modalPresentationStyle = .fullScreen
        return picker
    }()
    
    
    init(root: UIViewController, callback: @escaping (UIImage)->Void) {
        mRoot = root
        mCallback = callback
    }
    
    func show() {
        
        guard let types = UIImagePickerController.availableMediaTypes(for: .photoLibrary) else { return }
        
        func gallery() -> Void {
            self.pickerController.sourceType = .photoLibrary
            self.pickerController.mediaTypes = types
            self.mRoot?.present(self.pickerController, animated: true)
        }
        if (UIImagePickerController.isCameraDeviceAvailable(.front) || UIImagePickerController.isCameraDeviceAvailable(.rear)) {
            let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Take Photo", style: .default, handler: { [weak self](_) in
                guard let self = self else { return }
                self.pickerController.sourceType        = .camera
                self.pickerController.cameraCaptureMode = .photo
                self.mRoot?.present(self.pickerController, animated: true)
            }))
            alert.addAction(UIAlertAction(title: "Choose Photo", style: .default, handler: { (_) in
                gallery()
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            mRoot?.present(alert, animated: true)
        } else {
            gallery()
        }
        
    }
}
extension Gallery: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        mRoot?.dismiss(animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = (info[UIImagePickerController.InfoKey.originalImage] as? UIImage)?.resize(size: CGSize(width: 400, height: 400)) {
            mCallback(image)
            mRoot?.dismiss(animated: true)
        }
    }
    
}
extension UIImage {
    convenience init?(base64: String) {
        guard let data = Data(base64Encoded: base64, options: .init(rawValue: 0)) else { return nil }
        self.init(data: data)
    }
    var base64: String {
        if let imageData = self.jpegData(compressionQuality: 0.5){
            return imageData.base64EncodedString(options:[])
        }
        return ""
    }
    func resize(size: CGSize) -> UIImage? {
        let s: CGFloat = min(size.width  / self.size.width, size.height / self.size.height)
        let d: CGSize  = CGSize(width: self.size.width * s, height: self.size.height * s)
        UIGraphicsBeginImageContextWithOptions(d, false, UIScreen.main.scale)
        self.draw(in: CGRect(origin: .zero, size: d))
        let img: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img
    }
}

//===========================================================
//MARK: - Almofire ImageView
//===========================================================
import AlamofireImage
extension UIImageView {
    func setImageWithUrl(_ urlString: String, placeHolderImage image: UIImage?) {
        if let url = URL(string: urlString), url.host != nil && url.scheme != nil {
            self.af_setImage(withURL: url, placeholderImage: image)
        }
    }
    func setRenderImageWithUrl(_ urlString: String, placeHolderImage image: UIImage?) {
        if let url = URL(string: urlString), url.host != nil && url.scheme != nil {
            let filterValue = DynamicImageFilter("TemplateImageFilter") { image in
                return image.withRenderingMode(.alwaysTemplate)
            }
            self.af_setImage(withURL: url, placeholderImage: image, filter:filterValue)
        }
    }
    func rotated(angle: Int) {
        self.transform = CGAffineTransform(rotationAngle: angle.degreesToRadians.cgFloat);
    }
}
extension Int{
    var degreesToRadians: Double {
           return Double.pi * Double(self) / 180.0
       }
}
extension Double{
    var degreesToRadians: Double {
           return Double.pi * Double(self) / 180.0
       }
    var cgFloat: CGFloat {
           return CGFloat(self)
       }
 */
}
