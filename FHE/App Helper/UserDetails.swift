//
//  UserDetails.swift
//  Medbulance
//
//  Created by Apple on 17/03/21.
//

import UIKit
import CRPSmartBand
import CoreBluetooth
class UserDetails: NSObject {
    
    var connectedDevice:CRPDiscovery!
    
    static let sharedInstance = UserDetails()
    
    var notificationData = [NotificationData(name: "Phone", isSelected: false, type: NotificationType.phone),
                             NotificationData(name: "Message", isSelected: false, type: NotificationType.messages),
                             NotificationData(name: "Facebook", isSelected: false, type: NotificationType.facebook),
                             NotificationData(name: "Twitter", isSelected: false, type: NotificationType.twitter),
                             NotificationData(name: "WhatsApp", isSelected: false, type: NotificationType.whatsApp),
                             NotificationData(name: "Skype", isSelected: false, type: NotificationType.skype),
                             NotificationData(name: "Instagram", isSelected: false, type: NotificationType.instagram),
                             NotificationData(name: "KakaoTalk", isSelected: false, type: NotificationType.kakaoTalk),
                             NotificationData(name: "Line", isSelected: false, type: NotificationType.line),
                             NotificationData(name: "WeChat", isSelected: false, type: NotificationType.wechat)]
    
    
    var firstName = ""
    var lastName = ""
    var phone = ""
    var email = ""
    var gender = ""
    var height = ""
    var weight = ""
    var heightMeaseureIn = ""
    var weightMeasureIn = ""
    var goals = ""
    var currentActivityLevel = ""
    var preferedFoodCategory = ""
    
    
    var oxygenData = "0"
    var heartRateData = "0"
    var sbp = "0"
    var dbp = "0"
    var steps = "0"
    var stepsTime = ""
    var stepsDistance = ""
    var stepsCalorie = ""
    
    
    var deviceToken = ""
    var accessToken = ""
    var isDeviceConnected = false
    
    var Id = ""
    var Name = ""
    var Phone = ""
    var profileImage = ""
    var isVerifiedUser = false
}
