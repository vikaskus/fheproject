//
//  AppHelper.swift
//  Medbulance
//
//  Created by Apple on 15/03/21.
//

import UIKit
let APPDELEGATE = UIApplication.shared.delegate as! AppDelegate
typealias AlertCompletion = (_ action: UIAlertAction)->(Void)
class AppHelper: NSObject {
    
    
    static let sharedInstance = AppHelper()
    var watchData = [String:String]()
    var sleepData = [SleepModel]()
    var sportsData = [SportsModel]()
    var watchDataSleep = [String:Any]()
    var watchDataSports = [String:Any]()
    
    class func convertDateToFormattedDateString(date: Date) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss +0000"
        //   dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "yyyy/MM/dd"
        return dateFormatter.string(from: date)
    
        
    }
    
    class func resetUserDefaults() {
       
           let defaults = UserDefaults.standard
           let dictionary = defaults.dictionaryRepresentation()
           dictionary.keys.forEach { key in
              
                   defaults.removeObject(forKey: key)
           }
           
        UserDetails.sharedInstance.Id = ""
        UserDetails.sharedInstance.Name = ""
        UserDetails.sharedInstance.Phone = ""
        UserDetails.sharedInstance.profileImage = ""
        
        DriverDetails.sharedInstance.Id = ""
        DriverDetails.sharedInstance.Name = ""
        DriverDetails.sharedInstance.Phone = ""
        DriverDetails.sharedInstance.profileImage = ""
        DriverDetails.sharedInstance.Email = ""
           
           AppHelper.saveUserDetails()
           
       }
    
    
    class func getUserDetails(){
        
        UserDetails.sharedInstance.Id = UserDefaultOperations.getStringObject("Id")
        UserDetails.sharedInstance.Name = UserDefaultOperations.getStringObject("Name")
        UserDetails.sharedInstance.Phone = UserDefaultOperations.getStringObject("Phone")
        UserDetails.sharedInstance.profileImage = UserDefaultOperations.getStringObject("profileImage")
        UserDetails.sharedInstance.isVerifiedUser = UserDefaultOperations.getBoolObject("isVerifiedUser")
            
    }
    
    class func getDriverDetails(){
                
        DriverDetails.sharedInstance.Id = UserDefaultOperations.getStringObject("DriverId")
        DriverDetails.sharedInstance.Name = UserDefaultOperations.getStringObject("DriverName")
        DriverDetails.sharedInstance.Phone = UserDefaultOperations.getStringObject("DriverPhone")
        DriverDetails.sharedInstance.profileImage = UserDefaultOperations.getStringObject("DriverProfileImage")
        DriverDetails.sharedInstance.Email = UserDefaultOperations.getStringObject("Email")
        
        DriverDetails.sharedInstance.isVerify = UserDefaultOperations.getStringObject("isVerify")
        DriverDetails.sharedInstance.isVerifyLicense = UserDefaultOperations.getStringObject("isVerifyLicense")
        DriverDetails.sharedInstance.isVerifyAmbulance = UserDefaultOperations.getStringObject("isVerifyAmbulance")
        DriverDetails.sharedInstance.isVerifyPersonalId = UserDefaultOperations.getStringObject("isVerifyPersonalId")

    }
    
    class func saveUserDetails(){
        UserDefaultOperations.setStringObject("Id", UserDetails.sharedInstance.Id)
        UserDefaultOperations.setStringObject("Name", UserDetails.sharedInstance.Name)
        UserDefaultOperations.setStringObject("Phone", UserDetails.sharedInstance.Phone)
        UserDefaultOperations.setStringObject("profileImage", UserDetails.sharedInstance.profileImage)
        UserDefaultOperations.setBoolObject("isVerifiedUser", UserDetails.sharedInstance.isVerifiedUser)
        
    }
    
    class func saveDriverDetails(){

        UserDefaultOperations.setStringObject("DriverId", DriverDetails.sharedInstance.Id)
        UserDefaultOperations.setStringObject("DriverName", DriverDetails.sharedInstance.Name)
        UserDefaultOperations.setStringObject("DriverPhone", DriverDetails.sharedInstance.Phone)
        UserDefaultOperations.setStringObject("DriverProfileImage", DriverDetails.sharedInstance.profileImage)
        UserDefaultOperations.setStringObject("Email", DriverDetails.sharedInstance.Email)
      
        UserDefaultOperations.setStringObject("isVerify", DriverDetails.sharedInstance.isVerify)
        UserDefaultOperations.setStringObject("isVerifyLicense", DriverDetails.sharedInstance.isVerifyLicense)
        UserDefaultOperations.setStringObject("isVerifyAmbulance", DriverDetails.sharedInstance.isVerifyAmbulance)
        UserDefaultOperations.setStringObject("isVerifyPersonalId", DriverDetails.sharedInstance.isVerifyPersonalId)

    }
    
    
    class func selectContainer(view:UIView){
        view.layer.borderWidth = 1
        view.layer.borderColor = Colors.themeGreen.cgColor
        
    }
    
    class func unSelectContainer(view:UIView){
        view.layer.borderWidth = 0
        view.layer.borderColor = UIColor.clear.cgColor
        
    }
}
func showAlertWithCompletion(title: String?, message: String?, buttonTitle: String?, vc: UIViewController, completion:@escaping AlertCompletion) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
    let action = UIAlertAction(title: buttonTitle, style: .default, handler: completion)
    alert.addAction(action)
    alert.view.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    vc.present(alert, animated: true, completion: nil)
}
