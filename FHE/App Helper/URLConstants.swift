//
//  Constants.swift
//  HyperCommute
//
//  Created by anurag singh on 04/03/20.
//  Copyright © 2020 MAC. All rights reserved.
//

import Foundation
import UIKit

let BASEURL = "https://fheapi.fourbrick.in/"
let loginUrl = BASEURL + "user/login";
let verifyOtp = BASEURL + "user/verifyOTP";
let uploadUserData = BASEURL + "user/userInfo";
let uploadStepsData =
    BASEURL + "profile/addStepSummary";
let uploadHeartRateData =
    BASEURL + "profile/addHeartSummary";

let uploadOxygenData =
    BASEURL + "profile/addOxygenSummary";

let uploadbpData =
    BASEURL + "profile/addBPSummary";

let fetchWatchDataFromServer =
    BASEURL + "goal/dailySummary";

let getExerciseList =
    BASEURL + "goal/getExerciseList?";

let addExerciseByUser =
    BASEURL + "goal/addExerciseByUser";

let getUserExerciseList =
    BASEURL + "goal/getUserExerciseList";

let getBlogUrl = BASEURL + "plan/getBlog";
let getPlanUrl = BASEURL + "plan/getPlan";
let getTipsUrl = BASEURL + "plan/getTodayTip";
let getStepsSummary =
    BASEURL + "time/stepsDataDWM";
let getheartRateSummary =
    BASEURL + "time/heartDataDWM";
let getBloodPressureSummary =
    BASEURL + "time/bloodPressureDataDWM";
let getOxygenSummary =
    BASEURL + "time/oxygenDataDWM";
let getSleepData = BASEURL + "time/sleepDWM";

let addSleepData = BASEURL + "goal/addSleepData";

let fetchProfileData =
    BASEURL + "user/getUserInfo";

let updateFcmToken =
    BASEURL + "user/saveUserDeviceInfo";

let addReminder = BASEURL + "notification/setting";

let fetchReminder =
    BASEURL + "notification/getReminder";

let addSports = BASEURL + "sports/addSports";

let fetchSports =
    BASEURL + "sports/getSportsListUser";



