//
//  DriverDetails.swift
//  driverapp
//
//  Created by Vikash Rajput on 07/12/18.
//  Copyright © 2018 HyperCommute. All rights reserved.
//

import UIKit

class DriverDetails: NSObject {

    static let sharedInstance = DriverDetails()
    
    var Id = ""
    var Name = ""
    var Phone = ""
    var profileImage = ""
    var Email = ""
    var address = ""
    var VehicleModel = ""
    var VehicalNumber = ""
    var accessToken = ""
    var deviceToken = ""
    var RegisterId = ""
    
    var isVerify = "false"
    var isVerifyPersonalId = "false"
    var isVerifyLicense = "false"
    var isVerifyAmbulance = "false"
    
    var DriverProfileImage = ""
}
