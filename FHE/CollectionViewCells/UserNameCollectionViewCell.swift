//
//  UserNameCollectionViewCell.swift
//  FHE
//
//  Created by Macintosh on 22/10/21.
//  Copyright © 2021 sylar. All rights reserved.
//

import UIKit

class UserNameCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var txtFieldFirstName: UITextField!
    @IBOutlet weak var txtFieldLastName: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        txtFieldFirstName.attributedPlaceholder = NSAttributedString(string: "First Name",
                                                                     attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        txtFieldLastName.attributedPlaceholder = NSAttributedString(string: "Last Name",
                                                                     attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
    }

}
