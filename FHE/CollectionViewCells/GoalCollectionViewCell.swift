//
//  GoalCollectionViewCell.swift
//  FHE
//
//  Created by Macintosh on 22/10/21.
//  Copyright © 2021 sylar. All rights reserved.
//

import UIKit

class GoalCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var weightLossContainer: UIView!
    @IBOutlet weak var weightMaintainContainer: UIView!
    @IBOutlet weak var strengthGainContainer: UIView!
    
    var goal = ""

    override func awakeFromNib() {
        super.awakeFromNib()
        goal = "Weight Loss"
        AppHelper.selectContainer(view: weightLossContainer)
        AppHelper.unSelectContainer(view: weightMaintainContainer)
        AppHelper.unSelectContainer(view: strengthGainContainer)
        // Initialization code
    }
    
    @IBAction func weightLossTapped(_ sender: Any) {
        goal = "Weight Loss"
        AppHelper.selectContainer(view: weightLossContainer)
        AppHelper.unSelectContainer(view: weightMaintainContainer)
        AppHelper.unSelectContainer(view: strengthGainContainer)
    }
    
    @IBAction func weightMaintenanceTapped(_ sender: Any) {
        goal = "Weight Maintenance"
        AppHelper.selectContainer(view: weightMaintainContainer)
        AppHelper.unSelectContainer(view: weightLossContainer)
        AppHelper.unSelectContainer(view: strengthGainContainer)
    }
    
    @IBAction func strengthGainTapped(_ sender: Any) {
        goal = "Strength Gain"
        AppHelper.selectContainer(view: strengthGainContainer)
        AppHelper.unSelectContainer(view: weightLossContainer)
        AppHelper.unSelectContainer(view: weightMaintainContainer)
    }

}
