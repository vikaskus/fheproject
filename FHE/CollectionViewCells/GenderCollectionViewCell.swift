//
//  GenderCollectionViewCell.swift
//  FHE
//
//  Created by Macintosh on 22/10/21.
//  Copyright © 2021 sylar. All rights reserved.
//

import UIKit

class GenderCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var maleContainer: UIView!
    @IBOutlet weak var femaleContainer: UIView!
    @IBOutlet weak var otherContainer: UIView!
    
    var gender = ""

    override func awakeFromNib() {
        super.awakeFromNib()
        gender = "male"
        AppHelper.selectContainer(view: maleContainer)
        AppHelper.unSelectContainer(view: femaleContainer)
        AppHelper.unSelectContainer(view: otherContainer)
        // Initialization code
    }
    
    

    @IBAction func btnMaleTapped(_ sender: Any) {
        gender = "male"
        AppHelper.selectContainer(view: maleContainer)
        AppHelper.unSelectContainer(view: femaleContainer)
        AppHelper.unSelectContainer(view: otherContainer)
        
    }
    
    @IBAction func btnFemaleTapped(_ sender: Any) {
        gender = "female"
        AppHelper.selectContainer(view: femaleContainer)
        AppHelper.unSelectContainer(view: maleContainer)
        AppHelper.unSelectContainer(view: otherContainer)
        
    }
    @IBAction func btnOthersTapped(_ sender: Any) {
        gender = "others"
        AppHelper.selectContainer(view: otherContainer)
        AppHelper.unSelectContainer(view: femaleContainer)
        AppHelper.unSelectContainer(view: maleContainer)
        
    }
}
