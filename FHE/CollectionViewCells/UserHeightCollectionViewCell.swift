//
//  UserHeightCollectionViewCell.swift
//  FHE
//
//  Created by Macintosh on 22/10/21.
//  Copyright © 2021 sylar. All rights reserved.
//

import UIKit
protocol UserHeightCollectionViewCellDelegate {
    func btnFeetTapped()
    func btnCmsTapped()
}
class UserHeightCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var lblHeightTitle: UILabel!
    
    var delegate:UserHeightCollectionViewCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    

    @IBAction func btnFeetAction(_ sender: Any) {
        self.delegate?.btnFeetTapped()
        
    }
    @IBAction func btnCmsAction(_ sender: Any) {
        self.delegate?.btnCmsTapped()
        
    }
}
