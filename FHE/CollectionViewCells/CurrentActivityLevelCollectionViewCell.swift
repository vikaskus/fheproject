//
//  CurrentActivityLevelCollectionViewCell.swift
//  FHE
//
//  Created by Macintosh on 22/10/21.
//  Copyright © 2021 sylar. All rights reserved.
//

import UIKit

class CurrentActivityLevelCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var noWorkoutContainer: UIView!
    @IBOutlet weak var view1dayContainer: UIView!
    @IBOutlet weak var view1to3dayContainer: UIView!
    @IBOutlet weak var view4to6dayContainer: UIView!
    
    
    var currentActivityLevel = ""

    override func awakeFromNib() {
        super.awakeFromNib()
        currentActivityLevel = "No Workouts"
        AppHelper.selectContainer(view: noWorkoutContainer)
        AppHelper.unSelectContainer(view: view1dayContainer)
        AppHelper.unSelectContainer(view: view1to3dayContainer)
        AppHelper.unSelectContainer(view: view4to6dayContainer)
        // Initialization code
    }
    
    @IBAction func btnNoWorkoutTapped(_ sender: Any) {
        currentActivityLevel = "No Workouts"
        AppHelper.selectContainer(view: noWorkoutContainer)
        AppHelper.unSelectContainer(view: view1dayContainer)
        AppHelper.unSelectContainer(view: view1to3dayContainer)
        AppHelper.unSelectContainer(view: view4to6dayContainer)
    }
    
    @IBAction func btn1DayWeekTapped(_ sender: Any) {
        currentActivityLevel = "1 day a week"
        AppHelper.selectContainer(view: view1dayContainer)
        AppHelper.unSelectContainer(view: noWorkoutContainer)
        AppHelper.unSelectContainer(view: view1to3dayContainer)
        AppHelper.unSelectContainer(view: view4to6dayContainer)
    }
    
    @IBAction func btn1to3daysTapped(_ sender: Any) {
        currentActivityLevel = "1-3 days a week"
        AppHelper.selectContainer(view: view1to3dayContainer)
        AppHelper.unSelectContainer(view: noWorkoutContainer)
        AppHelper.unSelectContainer(view: view1dayContainer)
        AppHelper.unSelectContainer(view: view4to6dayContainer)
    }
    
    @IBAction func btn4to6daysTapped(_ sender: Any) {
        currentActivityLevel = "4-6 days a week"
        AppHelper.selectContainer(view: view4to6dayContainer)
        AppHelper.unSelectContainer(view: noWorkoutContainer)
        AppHelper.unSelectContainer(view: view1dayContainer)
        AppHelper.unSelectContainer(view: view1to3dayContainer)
    }

}
