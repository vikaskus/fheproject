//
//  WatchDataCollectionViewCell.swift
//  FHE
//
//  Created by Macintosh on 23/10/21.
//  Copyright © 2021 sylar. All rights reserved.
//

import UIKit
protocol WatchDataCollectionViewCellDelegate {
    func detailsTapped(index:Int)
}

class WatchDataCollectionViewCell: UICollectionViewCell {
    var index = 0
    var delegate:WatchDataCollectionViewCellDelegate?
    @IBOutlet weak var lblValueTitle: UILabel!
    @IBOutlet weak var lblKeyTitle: UILabel!
    
    @IBOutlet weak var dataTypeImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBAction func btnDetailsAction(_ sender: Any) {
        self.delegate?.detailsTapped(index: index)
    }
    
}
