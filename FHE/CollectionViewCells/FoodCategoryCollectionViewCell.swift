//
//  FoodCategoryCollectionViewCell.swift
//  FHE
//
//  Created by Macintosh on 22/10/21.
//  Copyright © 2021 sylar. All rights reserved.
//

import UIKit

class FoodCategoryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var vegContainer: UIView!
    @IBOutlet weak var nonVegContainer: UIView!
    @IBOutlet weak var northIndianContainer: UIView!
    @IBOutlet weak var southIndianContainer: UIView!
    var foodCategory = ""

    override func awakeFromNib() {
        super.awakeFromNib()
        foodCategory = "Vegetarian"
        AppHelper.selectContainer(view: vegContainer)
        AppHelper.unSelectContainer(view: nonVegContainer)
        AppHelper.unSelectContainer(view: northIndianContainer)
        AppHelper.unSelectContainer(view: southIndianContainer)
        // Initialization code
    }
    
    @IBAction func btnVegTapped(_ sender: Any) {
       
        foodCategory = "Vegetarian"
        
        AppHelper.selectContainer(view: vegContainer)
        AppHelper.unSelectContainer(view: nonVegContainer)
        AppHelper.unSelectContainer(view: northIndianContainer)
        AppHelper.unSelectContainer(view: southIndianContainer)
    }
    
    @IBAction func btnNonVegTapped(_ sender: Any) {
        foodCategory = "Non Vegetarian"
        
        AppHelper.selectContainer(view: nonVegContainer)
        AppHelper.unSelectContainer(view: vegContainer)
        AppHelper.unSelectContainer(view: northIndianContainer)
        AppHelper.unSelectContainer(view: southIndianContainer)
    }
    
    @IBAction func btnNorthIndianTapped(_ sender: Any) {
        foodCategory = "North Indian"
        
        AppHelper.selectContainer(view: northIndianContainer)
        AppHelper.unSelectContainer(view: vegContainer)
        AppHelper.unSelectContainer(view: nonVegContainer)
        AppHelper.unSelectContainer(view: southIndianContainer)
    }
    
    @IBAction func btnSouthIndianTapped(_ sender: Any) {
        foodCategory = "South Indian"
        
        AppHelper.selectContainer(view: southIndianContainer)
        AppHelper.unSelectContainer(view: vegContainer)
        AppHelper.unSelectContainer(view: nonVegContainer)
        AppHelper.unSelectContainer(view: northIndianContainer)
    }

}
