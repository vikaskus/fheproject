//
//  UserWeightCollectionViewCell.swift
//  FHE
//
//  Created by Macintosh on 22/10/21.
//  Copyright © 2021 sylar. All rights reserved.
//

import UIKit
protocol UserWeightCollectionViewCellDelegate {
    func btnKgTapped()
    func btnLbsTapped()
}

class UserWeightCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var lblWeightTitle: UILabel!
    var delegate:UserWeightCollectionViewCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    

    @IBAction func btnKgAction(_ sender: Any) {
        self.delegate?.btnKgTapped()
        
    }
    @IBAction func btnLbsAction(_ sender: Any) {
        self.delegate?.btnLbsTapped()
        
    }

}
