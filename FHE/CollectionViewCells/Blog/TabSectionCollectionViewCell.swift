//
//  TabSectionCollectionViewCell.swift
//  FHE
//
//  Created by Macintosh on 30/10/21.
//  Copyright © 2021 sylar. All rights reserved.
//

import UIKit

class TabSectionCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSlider: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
