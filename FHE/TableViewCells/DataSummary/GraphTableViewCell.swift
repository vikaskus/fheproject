//
//  GraphTableViewCell.swift
//  FHE
//
//  Created by Macintosh on 28/10/21.
//  Copyright © 2021 sylar. All rights reserved.
//

import UIKit
import Charts

class GraphTableViewCell: UITableViewCell {
    @IBOutlet var chartView: LineChartView!
    var shouldHideData: Bool = false
    let week = ["Mon", "Tue", "Wed","Thu","Fri","Sat","Sun"]
    
    let values = [0,2000,4000,6000,8000,10000]
    var numbers : [Double] = [1000,3000,2000,5575,4000,7000,9500]
    let yValues: [ChartDataEntry] = [ChartDataEntry(x: 0.0, y: 5.0),ChartDataEntry(x: 2.0, y: 4.0),ChartDataEntry(x: 3.0, y: 3.0)]
    override func awakeFromNib() {
        super.awakeFromNib()
       // updateChartData()
        updateGraph()
        // Initialization code
    }
    
    func updateGraph(){
        var lineChartEntry  = [ChartDataEntry]() //this is the Array that will eventually be displayed on the graph.

        
        
        //here is the for loop
        for i in 0..<numbers.count {

            let value = ChartDataEntry(x: Double(i), y: numbers[i]) // here we set the X and Y status in a data chart entry

            lineChartEntry.append(value) // here we add it to the data set
        }

        let line1 = LineChartDataSet(entries: lineChartEntry, label: "Steps") //Here we convert lineChartEntry to a LineChartDataSet

        line1.colors = [NSUIColor.purple] //Sets the colour to blue


        let data = LineChartData() //This is the object that will be added to the chart

        data.addDataSet(line1) //Adds the line to the dataSet
        
        chartView.xAxis.valueFormatter = IndexAxisValueFormatter(values:week)
        
        chartView.xAxis.granularity = 1
        chartView.xAxis.labelPosition = .top

        // Set the x values date formatter
        let yAxis = chartView.leftAxis
        yAxis.labelTextColor = .white
        yAxis.axisLineColor = .white
        yAxis.labelPosition = .outsideChart
        line1.drawCircleHoleEnabled = false
        line1.drawCirclesEnabled = false
        line1.lineWidth = 3
        line1.drawHorizontalHighlightIndicatorEnabled = false
        line1.drawVerticalHighlightIndicatorEnabled = false
        
        let xAxis = chartView.xAxis
        xAxis.labelPosition = .bottom
        xAxis.labelTextColor = .white
        xAxis.axisLineColor = .white
        chartView.rightAxis.enabled = false
        chartView.data = data
        data.setDrawValues(false)//finally - it adds the chart data to the chart and causes an update

        chartView.chartDescription?.text = "" // Here we set the description for the graph

    }
    


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func updateChartData() {
        if self.shouldHideData {
            chartView.data = nil
            return
        }

        self.setDataCount(Int(100), range: UInt32(45))
    }
    
    func setDataCount(_ count: Int, range: UInt32) {
        /*
        let values = (0..<count).map { (i) -> ChartDataEntry in
            let val = Double(arc4random_uniform(range) + 3)
            return ChartDataEntry(x: Double(i), y: val, icon: nil)
        }
        */
        let set1 = LineChartDataSet(entries: yValues, label: "DataSet 1")
      
        set1.drawIconsEnabled = false
        

        let value = ChartDataEntry(x: Double(3), y: 3)
        set1.addEntryOrdered(value)
      

        set1.fillAlpha = 1
      //  set1.fill = LinearGradientFill(gradient: gradient, angle: 90)
        set1.drawFilledEnabled = false
        

        let data = LineChartData(dataSet: set1)
        setup(set1)
        data.setDrawValues(false)
        chartView.rightAxis.enabled = false
        
        
        let yAxis = chartView.leftAxis
        yAxis.labelTextColor = .white
        yAxis.axisLineColor = .white
        yAxis.labelPosition = .outsideChart
        
        let xAxis = chartView.xAxis
        xAxis.labelPosition = .bottom
        xAxis.labelTextColor = .white
        xAxis.axisLineColor = .white
    //    set1.mode = .cubicBezier
    //    set1.lineWidth = 3
        set1.drawHorizontalHighlightIndicatorEnabled = false
        set1.drawVerticalHighlightIndicatorEnabled = false
        set1.highlightColor = .clear

        chartView.data = data
    }

    private func setup(_ dataSet: LineChartDataSet) {
//        if dataSet.isDrawLineWithGradientEnabled {
//            dataSet.lineDashLengths = nil
//            dataSet.highlightLineDashLengths = nil
//            dataSet.setColors(.black, .red, .white)
//            dataSet.setCircleColor(.black)
//            dataSet.gradientPositions = [0, 40, 100]
//            dataSet.lineWidth = 1
//            dataSet.circleRadius = 3
//            dataSet.drawCircleHoleEnabled = false
//            dataSet.valueFont = .systemFont(ofSize: 9)
//            dataSet.formLineDashLengths = nil
//            dataSet.formLineWidth = 1
//            dataSet.formSize = 15
//        } else {
            dataSet.lineDashLengths = nil
            dataSet.highlightLineDashLengths = [5, 2.5]
            dataSet.setColor(.white)
            dataSet.setCircleColor(.clear)
          //  dataSet.gradientPositions = nil
            dataSet.lineWidth = 1
            dataSet.circleRadius = 3
            dataSet.drawCircleHoleEnabled = false
            dataSet.valueFont = .systemFont(ofSize: 9)
            dataSet.formLineDashLengths = [5, 2.5]
            dataSet.formLineWidth = 1
            dataSet.formSize = 15
     //   }
    }
    
}
