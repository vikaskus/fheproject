//
//  StepsTableViewCell.swift
//  FHE
//
//  Created by Macintosh on 25/10/21.
//  Copyright © 2021 sylar. All rights reserved.
//

import UIKit

class StepsTableViewCell: UITableViewCell {

    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblCalorieBurned: UILabel!
    @IBOutlet weak var lblStepCount: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
