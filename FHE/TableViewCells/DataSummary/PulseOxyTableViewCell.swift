//
//  PulseOxyTableViewCell.swift
//  FHE
//
//  Created by Macintosh on 25/10/21.
//  Copyright © 2021 sylar. All rights reserved.
//

import UIKit

class PulseOxyTableViewCell: UITableViewCell {
    @IBOutlet weak var featureImageView: UIImageView!
    @IBOutlet weak var lblValue: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
