//
//  WatchFaceTableViewCell.swift
//  FHE
//
//  Created by Macintosh on 08/11/21.
//  Copyright © 2021 sylar. All rights reserved.
//

import UIKit

class WatchFaceTableViewCell: UITableViewCell {
    @IBOutlet weak var activeInactiveView: UIView!
    @IBOutlet weak var lblWatchFace: UILabel!
    @IBOutlet weak var watchImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
