//
//  WatchNotificationTableViewCell.swift
//  FHE
//
//  Created by Macintosh on 08/11/21.
//  Copyright © 2021 sylar. All rights reserved.
//

import UIKit

class WatchNotificationTableViewCell: UITableViewCell {

    @IBOutlet weak var btnSwitch: UISwitch!
    @IBOutlet weak var notificationTitle: UILabel!
    @IBOutlet weak var notificationImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        /*
        if selected{
            btnSwitch.isOn = true
        }else{
            btnSwitch.isOn = false

        }
        */

        // Configure the view for the selected state
    }
    
   
    @IBAction func btnSwitchAction(_ sender: Any) {
    }
    
}
