//
//  ExerciseDurationTableViewCell.swift
//  FHE
//
//  Created by Macintosh on 29/10/21.
//  Copyright © 2021 sylar. All rights reserved.
//

import UIKit
protocol ExerciseDurationTableViewCellDelegate {
    func durationEntered(value:String)
}
class ExerciseDurationTableViewCell: UITableViewCell,UITextFieldDelegate {
    
    var delegate:ExerciseDurationTableViewCellDelegate?

    @IBOutlet weak var txtFieldDuration: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        txtFieldDuration.delegate = self
        txtFieldDuration.attributedPlaceholder = NSAttributedString(string: "Enter Duration",
                                                                    attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.05){
            
            self.delegate?.durationEntered(value: textField.text!)
        }
        return true
    }
    
}
