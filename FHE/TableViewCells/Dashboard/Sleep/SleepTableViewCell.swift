//
//  SleepTableViewCell.swift
//  FHE
//
//  Created by Macintosh on 29/10/21.
//  Copyright © 2021 sylar. All rights reserved.
//

import UIKit
import Charts

class SleepTableViewCell: UITableViewCell {
    
    @IBOutlet var chartView: LineChartView!
    
    let week = ["Mon", "Tue", "Wed","Thu","Fri","Sat","Sun"]
    
    let values = [3,5,7,4,9,6]
    var numbers : [Double] = [2,5,7,4,9,6]

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        updateGraph()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func updateGraph(){
        var lineChartEntry  = [ChartDataEntry]() //this is the Array that will eventually be displayed on the graph.

        
        
        //here is the for loop
        for i in 0..<numbers.count {

            let value = ChartDataEntry(x: Double(i), y: numbers[i]) // here we set the X and Y status in a data chart entry

            lineChartEntry.append(value) // here we add it to the data set
        }

        let line1 = LineChartDataSet(entries: lineChartEntry, label: "Sleep") //Here we convert lineChartEntry to a LineChartDataSet

        line1.colors = [NSUIColor.purple] //Sets the colour to blue


        let data = LineChartData() //This is the object that will be added to the chart

        data.addDataSet(line1) //Adds the line to the dataSet
        
        chartView.xAxis.valueFormatter = IndexAxisValueFormatter(values:week)
        
        chartView.xAxis.granularity = 1
        chartView.xAxis.labelPosition = .top

        // Set the x values date formatter
        let yAxis = chartView.leftAxis
        yAxis.labelTextColor = .white
        yAxis.axisLineColor = .white
        yAxis.labelPosition = .outsideChart
        line1.drawCircleHoleEnabled = false
        line1.drawCirclesEnabled = false
        line1.lineWidth = 3
        line1.drawHorizontalHighlightIndicatorEnabled = false
        line1.drawVerticalHighlightIndicatorEnabled = false
        
        let xAxis = chartView.xAxis
        xAxis.labelPosition = .bottom
        xAxis.labelTextColor = .white
        xAxis.axisLineColor = .white
        chartView.rightAxis.enabled = false
        chartView.data = data
        data.setDrawValues(false)//finally - it adds the chart data to the chart and causes an update

        chartView.chartDescription?.text = "" // Here we set the description for the graph
    }
    
    
}
