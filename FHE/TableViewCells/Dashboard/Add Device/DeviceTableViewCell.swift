//
//  DeviceTableViewCell.swift
//  FHE
//
//  Created by Macintosh on 23/10/21.
//  Copyright © 2021 sylar. All rights reserved.
//

import UIKit

class DeviceTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var lblDeviceTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
