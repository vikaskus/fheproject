//
//  ExerciseListTableViewCell.swift
//  FHE
//
//  Created by Macintosh on 23/10/21.
//  Copyright © 2021 sylar. All rights reserved.
//

import UIKit

class ExerciseListTableViewCell: UITableViewCell {
    @IBOutlet weak var lblExerciseName: UILabel!
    
    @IBOutlet weak var lblCalories: UILabel!
    @IBOutlet weak var lblDuration: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
