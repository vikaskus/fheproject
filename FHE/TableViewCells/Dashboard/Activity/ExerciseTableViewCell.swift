//
//  ExerciseTableViewCell.swift
//  FHE
//
//  Created by Macintosh on 23/10/21.
//  Copyright © 2021 sylar. All rights reserved.
//

import UIKit
protocol ExerciseTableViewCellDelegate {
    func addExerciseTapped()
}
class ExerciseTableViewCell: UITableViewCell {

    
    @IBOutlet weak var btnAddExercise: UIButton!
    @IBOutlet weak var exerciseStackViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var exerciseStackView: UIStackView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblNumberOfExercise: UILabel!
    @IBOutlet weak var lblTotalCaloriesBurned: UILabel!
    @IBOutlet weak var lblTotalDuration: UILabel!
    @IBOutlet weak var itemList: UITableView!
    
    var delegate:ExerciseTableViewCellDelegate?
    var exerciseList = [AddedExerciseModel]()
    var totalSportsList = [SportsExerciseModel]()
    var isSports = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        itemList.register(UINib(nibName: "ExerciseListTableViewCell", bundle: nil), forCellReuseIdentifier: "ExerciseListTableViewCell")
        itemList.delegate = self
        itemList.dataSource = self
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func btnAddExerciseAction(_ sender: Any) {
        self.delegate?.addExerciseTapped()
        
    }
}
extension ExerciseTableViewCell:UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSports{
            return totalSportsList.count
        }else{
            return exerciseList.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isSports{
            let cell = itemList.dequeueReusableCell(withIdentifier: "ExerciseListTableViewCell") as? ExerciseListTableViewCell
            cell?.lblCalories.text = "\(totalSportsList[indexPath.row].kcal)"
            cell?.lblDuration.text = "\(totalSportsList[indexPath.row].vaildTime)"
            cell?.lblExerciseName.text = "\(totalSportsList[indexPath.row].sportName)"
            cell?.selectionStyle = .none
            
            return cell!
        }else{
            let cell = itemList.dequeueReusableCell(withIdentifier: "ExerciseListTableViewCell") as? ExerciseListTableViewCell
            cell?.lblCalories.text = "\(exerciseList[indexPath.row].totalCaloriesBurned)"
            cell?.lblDuration.text = "\(exerciseList[indexPath.row].duration)"
            cell?.lblExerciseName.text = "\(exerciseList[indexPath.row].exerciseName)"
            cell?.selectionStyle = .none
            
            return cell!
        }
       
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    
}
